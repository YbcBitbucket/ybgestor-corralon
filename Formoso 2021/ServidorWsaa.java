package WebServices;

import Clases.ConexionMySQL;
import Clases.fnCargarFecha;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.rpc.ParameterMode;
import org.apache.axis.AxisFault;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.Base64;
import org.apache.axis.encoding.XMLType;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import java.io.FileInputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertStore;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class ServidorWsaa {

    public WSFE.FEAuthRequest ObtenerAutorizacion() {
        
        String LoginTicketRespuesta = null;
        WSFE.FEAuthRequest AutorizacionSolicitud = new WSFE.FEAuthRequest();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectWsaa = null;
        String token = "";
        String sign = "";
        String cuit = "";
        fnCargarFecha fecha = new fnCargarFecha();

        //Selecciono si Hay token y Sign
        try {
            SelectWsaa = cn.createStatement();
            ResultSet rs = null;
            try {
                rs = SelectWsaa.executeQuery("SELECT MAX(idWsaa), token, sign, cuit FROM afip_wsaa "
                        + "WHERE direccionhost = '" + InetAddress.getLocalHost().getHostAddress() + "' AND "
                        + "nombrehost = '" + InetAddress.getLocalHost().getHostName() + "' AND "
                        + "expiration >= '" + fecha.cargarfechayhoraSQL() + "' AND tipows = 'PRODUCCION'");
            } catch (UnknownHostException ex) {
                Logger.getLogger(ServidorWsaa.class.getName()).log(Level.SEVERE, null, ex);
            }
            rs.next();
            if (rs.getString("token") != null) {
                
                token = rs.getString("token");
                sign = rs.getString("sign");
                cuit = rs.getString("cuit");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);

        } finally {
            try {
                if (SelectWsaa != null) {
                    SelectWsaa.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        //SI HAY TOKEN Y SIGN
        if (!token.equals("") && !sign.equals("") && !cuit.equals("")) {
            AutorizacionSolicitud.setToken(token);
            AutorizacionSolicitud.setSign(sign);
            AutorizacionSolicitud.setCuit(Long.parseLong(cuit));
        } else {

            long ticketLong = new Long("36000");
            String p12file = "C:/Certificados_AFIP/Certificados_Produccion/YBGestor.p12";
            //Para producción
            byte[] LoginTicketSolicitud_xml_cms = create_cms(p12file, "bruluma31", "YBGestor", "CN=wsaa, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239", "wsfe", ticketLong);
            // Invoke AFIP WSAA and get LoginTicketResponse

            try {

                //Para homologación
                LoginTicketRespuesta = invoque_WSAA(LoginTicketSolicitud_xml_cms, "https://wsaa.afip.gov.ar/ws/services/LoginCms");
                
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }

            // Get token & sign from LoginTicketResponse
            try {

                Reader tokenReader = new StringReader(LoginTicketRespuesta);
                Document tokenDoc = new SAXReader(false).read(tokenReader);
                String expirationTime = tokenDoc.valueOf("/loginTicketResponse/header/expirationTime");

                //formateo la la fecha de expiración
                String customFormat = "yyyy-MM-dd HH:mm:ss";

                DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
                LocalDateTime parsedDate = dtf.parseLocalDateTime(expirationTime);

                String dateWithCustomFormat = parsedDate.toString(DateTimeFormat.forPattern(customFormat));
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date myDate = formatter.parse(dateWithCustomFormat);
                java.sql.Timestamp sqlexpiration = new java.sql.Timestamp(myDate.getTime());

                //Inserta en la base de DATOS
                PreparedStatement InsertWsaa = null;

                try {

                    cn.setAutoCommit(false); //transaction block start

                    String sSQL = "INSERT INTO afip_wsaa("
                            + " token, sign, cuit, expiration,"
                            + " direccionhost, nombrehost, tipows) VALUES(?,?,?,?,?,?,?)";
                    InsertWsaa = cn.prepareStatement(sSQL);
                    InsertWsaa.setString(1, tokenDoc.valueOf("/loginTicketResponse/credentials/token"));
                    InsertWsaa.setString(2, tokenDoc.valueOf("/loginTicketResponse/credentials/sign"));
                    InsertWsaa.setString(3, "20239314075");
                    InsertWsaa.setTimestamp(4, sqlexpiration);
                    InsertWsaa.setString(5, InetAddress.getLocalHost().getHostAddress());
                    InsertWsaa.setString(6, InetAddress.getLocalHost().getHostName());
                    InsertWsaa.setString(7, "PRODUCCION");

                    int n = InsertWsaa.executeUpdate();

                    cn.commit(); //transaction block end

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    JOptionPane.showMessageDialog(null, ex);
                    try {
                        cn.rollback();
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                } finally {
                    try {
                        if (InsertWsaa != null) {
                            InsertWsaa.close();
                        }
                        if (cn != null) {
                            cn.close();
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }

                AutorizacionSolicitud.setToken(tokenDoc.valueOf("/loginTicketResponse/credentials/token"));
                AutorizacionSolicitud.setSign(tokenDoc.valueOf("/loginTicketResponse/credentials/sign"));
                AutorizacionSolicitud.setCuit(Long.parseLong("20346059479"));

                System.out.println(LoginTicketRespuesta);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }

        }

        System.out.println("TOKEN: " + AutorizacionSolicitud.getToken());
        System.out.println("SIGN: " + AutorizacionSolicitud.getSign());
        System.out.println(fecha.cargarfechayhoraSQL());

        return AutorizacionSolicitud;
    }

    public WSFE_HOMOLOGACION.FEAuthRequest ObtenerAutorizacionHomologacion() {
        String LoginTicketRespuesta = null;
        WSFE_HOMOLOGACION.FEAuthRequest AutorizacionSolicitud = new WSFE_HOMOLOGACION.FEAuthRequest();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectWsaa = null;
        String token = "";
        String sign = "";
        String cuit = "";
        fnCargarFecha fecha = new fnCargarFecha();

        //Selecciono si Hay token y Sign
        try {
            SelectWsaa = cn.createStatement();
            ResultSet rs = null;
            try {
                rs = SelectWsaa.executeQuery("SELECT MAX(idWsaa), token, sign, cuit FROM afip_wsaa "
                        + "WHERE direccionhost = '" + InetAddress.getLocalHost().getHostAddress() + "' AND "
                        + "nombrehost = '" + InetAddress.getLocalHost().getHostName() + "' AND "
                        + "expiration >= '" + fecha.cargarfechayhoraSQL() + "' AND tipows = 'HOMOLOGACION'");
            } catch (UnknownHostException ex) {
                Logger.getLogger(ServidorWsaa.class.getName()).log(Level.SEVERE, null, ex);
            }
            rs.next();
            if (rs.getString("token") != null) {

                token = rs.getString("token");
                sign = rs.getString("sign");
                cuit = rs.getString("cuit");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);

        } finally {
            try {
                if (SelectWsaa != null) {
                    SelectWsaa.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        //SI HAY TOKEN Y SIGN
        if (!token.equals("") && !sign.equals("") && !cuit.equals("")) {
            AutorizacionSolicitud.setToken(token);
            AutorizacionSolicitud.setSign(sign);
            AutorizacionSolicitud.setCuit(Long.parseLong(cuit));
        } else {

            long ticketLong = new Long("36000");
            String p12file = "C:/Certificados_AFIP/Certificados_Homologacion/SOFT.p12";
            //Para homologación
            byte[] LoginTicketSolicitud_xml_cms = create_cms(p12file, "Cole978++", "SOFT", "CN=wsaahomo, O=AFIP, C=AR, serialNumber=CUIT 33693450239", "wsfe", ticketLong);
            // Invoke AFIP WSAA and get LoginTicketResponse

            try {

                //Para homologación
                LoginTicketRespuesta = invoque_WSAA(LoginTicketSolicitud_xml_cms, "https://wsaahomo.afip.gov.ar/ws/services/LoginCms");

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }

            // Get token & sign from LoginTicketResponse
            try {

                Reader tokenReader = new StringReader(LoginTicketRespuesta);
                Document tokenDoc = new SAXReader(false).read(tokenReader);
                String expirationTime = tokenDoc.valueOf("/loginTicketResponse/header/expirationTime");

                //formateo la la fecha de expiración
                String customFormat = "yyyy-MM-dd HH:mm:ss";

                DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
                LocalDateTime parsedDate = dtf.parseLocalDateTime(expirationTime);

                String dateWithCustomFormat = parsedDate.toString(DateTimeFormat.forPattern(customFormat));
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date myDate = formatter.parse(dateWithCustomFormat);
                java.sql.Timestamp sqlexpiration = new java.sql.Timestamp(myDate.getTime());

                //Inserta en la base de DATOS
                PreparedStatement InsertWsaa = null;

                try {

                    cn.setAutoCommit(false); //transaction block start

                    String sSQL = "INSERT INTO afip_wsaa("
                            + " token, sign, cuit, expiration,"
                            + " direccionhost, nombrehost, tipows) VALUES(?,?,?,?,?,?,?)";
                    InsertWsaa = cn.prepareStatement(sSQL);
                    InsertWsaa.setString(1, tokenDoc.valueOf("/loginTicketResponse/credentials/token"));
                    InsertWsaa.setString(2, tokenDoc.valueOf("/loginTicketResponse/credentials/sign"));
                    InsertWsaa.setString(3, "20346059479");
                    InsertWsaa.setTimestamp(4, sqlexpiration);
                    InsertWsaa.setString(5, InetAddress.getLocalHost().getHostAddress());
                    InsertWsaa.setString(6, InetAddress.getLocalHost().getHostName());
                    InsertWsaa.setString(7, "HOMOLOGACION");

                    int n = InsertWsaa.executeUpdate();

                    cn.commit(); //transaction block end

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    JOptionPane.showMessageDialog(null, ex);
                    try {
                        cn.rollback();
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                } finally {
                    try {
                        if (InsertWsaa != null) {
                            InsertWsaa.close();
                        }
                        if (cn != null) {
                            cn.close();
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }

                AutorizacionSolicitud.setToken(tokenDoc.valueOf("/loginTicketResponse/credentials/token"));
                AutorizacionSolicitud.setSign(tokenDoc.valueOf("/loginTicketResponse/credentials/sign"));
                AutorizacionSolicitud.setCuit(Long.parseLong("20346059479"));

                System.out.println(LoginTicketRespuesta);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }

        }

        System.out.println("TOKEN: " + AutorizacionSolicitud.getToken());
        System.out.println("SIGN: " + AutorizacionSolicitud.getSign());
        System.out.println(fecha.cargarfechayhoraSQL());

        return AutorizacionSolicitud;
    }

    public String invoque_WSAA(byte[] LoginTicketSolicitud_xml_cms, String endpoint)
            throws AxisFault, Exception {
        String LoginTicketRespuesta = null;
        try {
            Service service = new Service();
            org.apache.axis.client.Call call = (org.apache.axis.client.Call) service.createCall();
            // Prepare the call for the web service
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            call.setOperationName("loginCms");
            call.addParameter("request", XMLType.XSD_STRING, ParameterMode.IN);
            call.setReturnType(XMLType.XSD_STRING);
            // Make the actual call and assign the answer to a string
            LoginTicketRespuesta = (String) call.invoke(new Object[]{Base64
                .encode(LoginTicketSolicitud_xml_cms)});
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return LoginTicketRespuesta;
    }

    // Create the CMS Message
    // p12file = Keystore en formato PKCS#12 que posee el certificado del computador que solicita el acceso
    // p12pass = Clave de acceso al keystore 
    // signer = Signer del certificado en el keystore
    // dstDN = Destino del Servicio
    // service = Servicio para el cual se requiere el ticket de acceso
    // TicketTime = Tiempo de vida del ticket requerido
    public byte[] create_cms(String p12file, String p12pass, String signer,
            String dstDN, String service, Long TicketTime) {
        PrivateKey pKey = null;
        X509Certificate pCertificate = null;
        byte[] asn1_cms = null;
        CertStore cstore = null;
        String LoginTicketSolicitud_xml;
        String SignerDN = null;

        // Manage Keys & Certificates
        try {
            // Create a keystore using keys from the pkcs#12 p12file
            KeyStore ks = KeyStore.getInstance("pkcs12");
            FileInputStream p12stream = new FileInputStream(p12file);
            ks.load(p12stream, p12pass.toCharArray());
            p12stream.close();

            // Get Certificate & Private key from KeyStore
            pKey = (PrivateKey) ks.getKey(signer, p12pass.toCharArray());

            pCertificate = (X509Certificate) ks.getCertificate(signer);

            SignerDN = pCertificate.getSubjectDN().toString();

            // Create a list of Certificates to include in the final CMS
            ArrayList<X509Certificate> certList = new ArrayList<X509Certificate>();
            certList.add(pCertificate);
            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }
            cstore = CertStore.getInstance("Collection",
                    new CollectionCertStoreParameters(certList), "BC");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        // Create XML Message
        LoginTicketSolicitud_xml = crear_LoginTicketSolicitud(SignerDN, dstDN,
                service, TicketTime);
        // Create CMS Message
        try {
            // Create a new empty CMS Message
            CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
            // Add a Siggner to the Message
            gen.addSigner(pKey, pCertificate,
                    CMSSignedDataGenerator.DIGEST_SHA1);
            // Add the Certificate to the Message
            gen.addCertificatesAndCRLs(cstore);
            // Add the data (XML) to the Message
            CMSProcessable data = new CMSProcessableByteArray(
                    LoginTicketSolicitud_xml.getBytes());
            // Add a Sign of the Data to the Message
            CMSSignedData signed = gen.generate(data, true, "BC");
            asn1_cms = signed.getEncoded();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return (asn1_cms);
    }

    // Create XML Message fo0r AFIP wsaa
    public String crear_LoginTicketSolicitud(String SignerDN, String dstDN,
            String service, Long TicketTime) {
        String LoginTicketSolicitud_xml;
        Date GenTime = new Date();
        GregorianCalendar genTime = new GregorianCalendar();
        GregorianCalendar expTime = new GregorianCalendar();
        String UniqueId = new Long(GenTime.getTime() / 1000).toString();
        expTime.setTime(new Date(GenTime.getTime() + TicketTime));
        XMLGregorianCalendarImpl XMLGenTime = new XMLGregorianCalendarImpl(genTime);
        XMLGregorianCalendarImpl XMLExpTime = new XMLGregorianCalendarImpl(expTime);

        LoginTicketSolicitud_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<loginTicketRequest version=\"1.0\">"
                + "<header>"
                + "<source>"
                + SignerDN
                + "</source>"
                + "<destination>"
                + dstDN
                + "</destination>"
                + "<uniqueId>"
                + UniqueId
                + "</uniqueId>"
                + "<generationTime>"
                + XMLGenTime
                + "</generationTime>"
                + "<expirationTime>"
                + XMLExpTime
                + "</expirationTime>"
                + "</header>"
                + "<service>"
                + service
                + "</service>"
                + "</loginTicketRequest>";
        System.out.println("XML_TICKET: " + LoginTicketSolicitud_xml);
        return (LoginTicketSolicitud_xml);
    }

}
