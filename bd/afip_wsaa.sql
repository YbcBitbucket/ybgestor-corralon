﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:41:25
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "afip_wsaa"
#

CREATE TABLE `afip_wsaa` (
  `idWsaa` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token` text DEFAULT NULL,
  `sign` text DEFAULT NULL,
  `cuit` varchar(255) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `direccionhost` varchar(255) DEFAULT NULL,
  `nombrehost` varchar(255) DEFAULT NULL,
  `tipows` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idWsaa`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1759 DEFAULT CHARSET=utf8;
