﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:36:30
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# View "vista_facturacion_usuario"
#

CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion_usuario`
  AS
  SELECT
    `afip_datos`.`idTiporesponsable`,
    `afip_tiporesponsable`.`descripcion`,
    `afip_datos`.`nombre`,
    `afip_hasar_propiedades`.`direccionip`,
    `afip_hasar_propiedades`.`idImpresoraHasar`,
    `afip_hasar_propiedades`.`transporte`,
    `afip_hasar_propiedades`.`puerto`,
    `afip_hasar_propiedades`.`baudios`
  FROM
    ((`afip_datos`
      JOIN `afip_tiporesponsable` ON (`afip_datos`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
      JOIN `afip_hasar_propiedades`)
  ORDER BY `afip_hasar_propiedades`.`idImpresoraHasar` DESC;
