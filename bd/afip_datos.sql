﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:33:10
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "afip_datos"
#

CREATE TABLE `afip_datos` (
  `idDatos` int(11) NOT NULL AUTO_INCREMENT,
  `idTiporesponsable` int(11) unsigned NOT NULL DEFAULT 1,
  `idTipodoc` int(11) unsigned NOT NULL DEFAULT 1,
  `IvaDocumento` varchar(50) NOT NULL DEFAULT '',
  `nombre` varchar(255) DEFAULT NULL,
  `topeDiario` double(11,2) DEFAULT NULL,
  `minimo` int(11) DEFAULT NULL,
  `maximo` int(11) DEFAULT NULL,
  `automatizacion` int(1) NOT NULL DEFAULT 1,
  `horaZ` int(11) DEFAULT NULL,
  `minutoZ` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDatos`),
  KEY `fk_afipdatos_afiptipodoc` (`idTipodoc`),
  KEY `fk_afipdatos_afiptiporesponsable` (`idTiporesponsable`),
  CONSTRAINT `fk_afipdatos_afiptipodoc` FOREIGN KEY (`idTipodoc`) REFERENCES `afip_tipodoc` (`idTipodoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_afipdatos_afiptiporesponsable` FOREIGN KEY (`idTiporesponsable`) REFERENCES `afip_tiporesponsable` (`idTiporesponsable`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "afip_datos"
#

INSERT INTO `afip_datos` VALUES (1,1,25,'20243403694','Araoz Walter Gabriel',10000.00,10,30,1,23,0);
