﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:33:37
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "afip_tipo_facturacion"
#

CREATE TABLE `afip_tipo_facturacion` (
  `idAfipTipo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo` int(1) DEFAULT 0,
  `estado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idAfipTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipo_facturacion"
#

INSERT INTO `afip_tipo_facturacion` VALUES (1,'WSFE',1,'PRODUCCION'),(2,'HASAR',0,NULL),(3,'HASAR 2G',0,NULL);
