﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:35:37
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "facturacion_tributo"
#

CREATE TABLE `facturacion_tributo` (
  `idFacturacion_Tributo` int(11) NOT NULL AUTO_INCREMENT,
  `idFacturacion` int(11) unsigned NOT NULL DEFAULT 0,
  `idTipotributo` int(11) unsigned NOT NULL DEFAULT 0,
  `desc` varchar(255) DEFAULT NULL,
  `baseImp` double(13,2) unsigned DEFAULT 0.00,
  `alicuota` double(13,2) unsigned DEFAULT 0.00,
  `importe` double(13,2) unsigned DEFAULT 0.00,
  PRIMARY KEY (`idFacturacion_Tributo`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
