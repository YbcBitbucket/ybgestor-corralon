﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:36:58
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# View "vista_tipofacturacion"
#

CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_tipofacturacion`
  AS
  SELECT
    `clientes`.`idClientes`,
    `clientes`.`idTiporesponsable`,
    `afip_tipo_facturacion`.`descripcion` AS 'AfipTipo',
    `vista_clientes`.`nombre` AS 'RazonSocial',
    `vista_clientes`.`IvaDocumento` AS 'Documento',
    `afip_tipodoc`.`wsfe` AS 'tipodocWSFE',
    `afip_tipodoc`.`hasar` AS 'tipodocHASAR',
    `afip_tiporesponsable`.`wsfe` AS 'tiporesponsableWSFE',
    `afip_tiporesponsable`.`hasar` AS 'tiporesponsableHASAR',
    `vista_clientes`.`domicilio` AS 'Domicilio'
  FROM
    ((`afip_tipo_facturacion`
      JOIN ((`clientes`
        JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
        JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`)))
      JOIN `vista_clientes`)
  WHERE
    `afip_tipo_facturacion`.`tipo` = 1 AND `clientes`.`idClientes` = `vista_clientes`.`idClientes`;
