﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:33:27
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "afip_operacion"
#

CREATE TABLE `afip_operacion` (
  `idCodigoOperacion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(1) NOT NULL DEFAULT '',
  `descripcion` varchar(50) DEFAULT '',
  PRIMARY KEY (`idCodigoOperacion`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "afip_operacion"
#

INSERT INTO `afip_operacion` VALUES (1,'0','NO CORRESPONDE'),(2,'A','NO ALCANZADO'),(3,'C','OPERACIONES DE CANJE'),(4,'E','OPERACIONES EXENTAS'),(5,'N','NO GRAVADO'),(6,'X','IMPORTACION DEL EXTERIOR'),(7,'Z','IMPORTACION DE ZONA FRANCA');
