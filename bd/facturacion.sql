﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:32:39
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "facturacion"
#

DROP TABLE IF EXISTS `facturacion`;
CREATE TABLE `facturacion` (
  `idFacturacion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idPedidos` int(11) unsigned DEFAULT NULL,
  `cantReg` int(11) DEFAULT 0,
  `idTipoCbte` int(11) unsigned NOT NULL DEFAULT 0,
  `puntoVenta` int(11) unsigned NOT NULL DEFAULT 0,
  `idTiporesponsable` int(11) unsigned DEFAULT NULL,
  `documento` bigint(11) unsigned DEFAULT NULL,
  `cbteDesde` varchar(255) DEFAULT NULL,
  `numeroCbte` int(11) DEFAULT 0,
  `fechaCbte` varchar(25) DEFAULT NULL,
  `cbteHasta` varchar(255) DEFAULT NULL,
  `cbteFech` varchar(40) DEFAULT NULL,
  `impTotal` double(13,2) DEFAULT NULL,
  `impTotConc` double(13,2) DEFAULT NULL,
  `impNeto` double(13,2) DEFAULT NULL,
  `impOpEx` double(13,2) DEFAULT NULL,
  `impIVA` double(13,2) DEFAULT NULL,
  `impTrib` double(13,2) DEFAULT NULL,
  `AfipTipo` varchar(40) DEFAULT NULL,
  `CAE` varchar(255) DEFAULT '0',
  `fechaCAE` varchar(40) DEFAULT NULL,
  `cbteAsociado` varchar(40) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  `totalVenta` double(11,4) DEFAULT 0.0000,
  `totalIVA` double(11,4) DEFAULT 0.0000,
  `descripcion` varchar(255) DEFAULT NULL,
  `horaCbte` varchar(255) DEFAULT NULL,
  `totaldescuento` double(11,4) NOT NULL DEFAULT 0.0000,
  `subtotalNeto` double(11,4) NOT NULL DEFAULT 0.0000,
  PRIMARY KEY (`idFacturacion`),
  KEY `fk_facturacion_tipocbte` (`idTipoCbte`),
  KEY `fk_facturacion_pedidos` (`idPedidos`),
  CONSTRAINT `fk_facturacion_pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_facturacion_tipocbte` FOREIGN KEY (`idTipoCbte`) REFERENCES `afip_tipocbte` (`idTipoCbte`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
