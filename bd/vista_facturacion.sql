﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:36:06
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# View "vista_facturacion"
#

CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion`
  AS
  SELECT
    `facturacion`.`idFacturacion`,
    `afip_tipocbte`.`descripcion` AS 'tipocbte',
    `facturacion`.`numeroCbte`,
    `facturacion`.`fechaCbte`,
    `facturacion`.`puntoVenta`,
    `facturacion`.`totalVenta`,
    `facturacion`.`totalIVA`,
    `facturacion`.`CAE`,
    `facturacion`.`descripcion`,
    `facturacion`.`idPedidos`,
    `vista_pedidos`.`cliente`,
    `facturacion`.`cbteDesde`
  FROM
    ((`facturacion`
      JOIN `afip_tipocbte` ON (`facturacion`.`idTipoCbte` = `afip_tipocbte`.`idTipoCbte`))
      JOIN `vista_pedidos` ON (`facturacion`.`idPedidos` = `vista_pedidos`.`idPedidos`))
  ORDER BY `facturacion`.`idFacturacion`;
