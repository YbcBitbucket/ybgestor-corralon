﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:35:25
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "facturacion_iva"
#

CREATE TABLE `facturacion_iva` (
  `idFacturacion_IVA` int(11) NOT NULL AUTO_INCREMENT,
  `idFacturacion` int(11) unsigned NOT NULL DEFAULT 0,
  `idTipoiva` int(11) unsigned DEFAULT 0,
  `baseImp` double(13,2) unsigned DEFAULT 0.00,
  `importe` double(13,2) unsigned DEFAULT 0.00,
  PRIMARY KEY (`idFacturacion_IVA`) USING BTREE,
  KEY `fk_facturacioniva_facturacion` (`idFacturacion`) USING BTREE,
  KEY `fk_facturacioniva_tipoiva` (`idTipoiva`) USING BTREE,
  CONSTRAINT `fk_facturacioniva_facturacion` FOREIGN KEY (`idFacturacion`) REFERENCES `facturacion` (`idFacturacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_facturacioniva_tipoiva` FOREIGN KEY (`idTipoiva`) REFERENCES `afip_tipoiva` (`idTipoiva`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
