﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2021-10-22 00:44:55
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# View "vista_clientes"
#

CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_clientes`
  AS
  SELECT
    `clientes`.`idClientes`,
    `clientes`.`nombre`,
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    `clientes`.`margen`,
    `clientes`.`domicilio`
  FROM
    ((`clientes`
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`));
