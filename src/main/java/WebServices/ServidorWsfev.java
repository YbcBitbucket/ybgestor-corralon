
package WebServices;

import WSFE.CbteTipoResponse;
import WSFE.ConceptoTipoResponse;
import WSFE.DocTipoResponse;
import WSFE.DummyResponse;
import WSFE.FECAEAGetResponse;
import WSFE.FECAEAResponse;
import WSFE.FECAEASinMovConsResponse;
import WSFE.FECAEASinMovResponse;
import WSFE.FECAEResponse;
import WSFE.FECompConsultaResponse;
import WSFE.FECotizacionResponse;
import WSFE.FEPaisResponse;
import WSFE.FEPtoVentaResponse;
import WSFE.FERecuperaLastCbteResponse;
import WSFE.FERegXReqResponse;
import WSFE.FETributoResponse;
import WSFE.IvaTipoResponse;
import WSFE.MonedaResponse;
import WSFE.OpcionalTipoResponse;


public class ServidorWsfev {

    public static FECAEAGetResponse fecaeaConsultar(WSFE.FEAuthRequest auth, int periodo, short orden) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaConsultar(auth, periodo, orden);
    }

    public static FECAEAResponse fecaeaRegInformativo(WSFE.FEAuthRequest auth, WSFE.FECAEARequest feCAEARegInfReq) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaRegInformativo(auth, feCAEARegInfReq);
    }

    public static FECAEASinMovConsResponse fecaeaSinMovimientoConsultar(WSFE.FEAuthRequest auth, java.lang.String caea, int ptoVta) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSinMovimientoConsultar(auth, caea, ptoVta);
    }

    public static FECAEASinMovResponse fecaeaSinMovimientoInformar(WSFE.FEAuthRequest auth, int ptoVta, java.lang.String caea) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSinMovimientoInformar(auth, ptoVta, caea);
    }

    public static FECAEAGetResponse fecaeaSolicitar(WSFE.FEAuthRequest auth, int periodo, short orden) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSolicitar(auth, periodo, orden);
    }

    public static FECAEResponse fecaeSolicitar(WSFE.FEAuthRequest auth, WSFE.FECAERequest feCAEReq) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.fecaeSolicitar(auth, feCAEReq);
    }

    public static FECompConsultaResponse feCompConsultar(WSFE.FEAuthRequest auth, WSFE.FECompConsultaReq feCompConsReq) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feCompConsultar(auth, feCompConsReq);
    }

    public static FERegXReqResponse feCompTotXRequest(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feCompTotXRequest(auth);
    }

    public static FERecuperaLastCbteResponse feCompUltimoAutorizado(WSFE.FEAuthRequest auth, int ptoVta, int cbteTipo) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feCompUltimoAutorizado(auth, ptoVta, cbteTipo);
    }

    public static DummyResponse feDummy() {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feDummy();
    }

    public static FECotizacionResponse feParamGetCotizacion(WSFE.FEAuthRequest auth, java.lang.String monId) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetCotizacion(auth, monId);
    }

    public static FEPtoVentaResponse feParamGetPtosVenta(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetPtosVenta(auth);
    }

    public static CbteTipoResponse feParamGetTiposCbte(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposCbte(auth);
    }

    public static ConceptoTipoResponse feParamGetTiposConcepto(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposConcepto(auth);
    }

    public static DocTipoResponse feParamGetTiposDoc(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposDoc(auth);
    }

    public static IvaTipoResponse feParamGetTiposIva(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposIva(auth);
    }

    public static MonedaResponse feParamGetTiposMonedas(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposMonedas(auth);
    }

    public static OpcionalTipoResponse feParamGetTiposOpcional(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposOpcional(auth);
    }

    public static FEPaisResponse feParamGetTiposPaises(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposPaises(auth);
    }

    public static FETributoResponse feParamGetTiposTributos(WSFE.FEAuthRequest auth) {
        WSFE.Service service = new WSFE.Service();
        WSFE.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposTributos(auth);
    }
    
    
}
