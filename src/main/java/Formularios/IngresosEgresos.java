package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnEscape;
import Clases.fnReversa;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import static Formularios.PedidosDetalle.idPedidos;
import static Formularios.CuentasCorrientes.id_cliente;

public class IngresosEgresos extends javax.swing.JDialog {

    DefaultTableModel model1, model2, model3;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    String fecha, cod;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

    public IngresosEgresos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclickPedidos();
        dobleclickCC();
        fnEscape.funcionescape(this);
        this.setLocationRelativeTo(null);
        cargartablaPedidos();
        cargartablaEgresos();
        cargartablaCuentaCorriente();
        cargartotales();
        cargarfecha();
        this.setResizable(false);
    }

    void dobleclickPedidos() {
        TablaPedidos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    idPedidos = TablaPedidos.getValueAt(TablaPedidos.getSelectedRow(), 0).toString();
                    new PedidosDetalle(null, true).setVisible(true);
                }
            }
        });
    }

    void dobleclickCC() {
        TablaCC.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    id_cliente = Integer.valueOf(TablaCC.getValueAt(TablaCC.getSelectedRow(), 0).toString());
                    new PedidosDetalle(null, true).setVisible(true);
                }
            }
        });
    }

    void cargartablaPedidos() {
        /////Tabla Ingresos    
        String[] Titulo1 = {"N°", "Fecha", "FormadePago", "Cliente", "Total ($)"};
        String[] Registros1 = new String[5];

        String sql1 = "SELECT idPedidos,fecha,formadepago,cliente,total FROM vista_pedidos where estado != 'ANULADO' and (formadepago != 'CUENTA CORRIENTE')";
        model1 = new DefaultTableModel(null, Titulo1) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);
            while (rs.next()) {
                Registros1[0] = rs.getString(1);
                Registros1[1] = rs.getString(2);
                Registros1[2] = rs.getString(3);
                Registros1[3] = rs.getString(4);
                Registros1[4] = rs.getString(5);
                model1.addRow(Registros1);

            }
            TablaPedidos.setModel(model1);
            TablaPedidos.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            TablaPedidos.getColumnModel().getColumn(0).setPreferredWidth(20);
            TablaPedidos.getColumnModel().getColumn(1).setPreferredWidth(120);
            TablaPedidos.getColumnModel().getColumn(4).setPreferredWidth(120);
            alinear();
            TablaPedidos.getColumnModel().getColumn(0).setCellRenderer(alinearIzquierda);
            TablaPedidos.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            TablaPedidos.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            TablaPedidos.getColumnModel().getColumn(3).setCellRenderer(alinearIzquierda);
            TablaPedidos.getColumnModel().getColumn(4).setCellRenderer(alinearIzquierda);
            ///////Ultima Fila///////
            Rectangle r = TablaPedidos.getCellRect(TablaPedidos.getRowCount() - 1, 0, true);
            TablaPedidos.scrollRectToVisible(r);
            TablaPedidos.getSelectionModel().setSelectionInterval(TablaPedidos.getRowCount() - 1, TablaPedidos.getRowCount() - 1);
            //////////////////////////
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargartablaCuentaCorriente() {
        /////Tabla Ingresos    
        String[] Titulo1 = {"N°", "Fecha", "FormadePago", "Cliente", "Total ($)"};
        String[] Registros1 = new String[5];

        String sql1 = "SELECT fecha,detalle,nombre,haber FROM vista_ctacte_detalle where descripcion = 'PAGO DE CTA CTE'";
        model3 = new DefaultTableModel(null, Titulo1) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql1);
            int i = 1;
            while (rs.next()) {
                Registros1[0] = String.valueOf(i);
                Registros1[1] = rs.getString(1);
                Registros1[2] = rs.getString(2);
                Registros1[3] = rs.getString(3);
                Registros1[4] = rs.getString(4);
                model3.addRow(Registros1);
                i++;
            }
            TablaCC.setModel(model3);
            TablaCC.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            TablaCC.getColumnModel().getColumn(0).setPreferredWidth(20);
            TablaCC.getColumnModel().getColumn(1).setPreferredWidth(120);
            TablaCC.getColumnModel().getColumn(4).setPreferredWidth(120);
            alinear();
            TablaCC.getColumnModel().getColumn(0).setCellRenderer(alinearIzquierda);
            TablaCC.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            TablaCC.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            TablaCC.getColumnModel().getColumn(3).setCellRenderer(alinearIzquierda);
            TablaCC.getColumnModel().getColumn(4).setCellRenderer(alinearIzquierda);
            ///////Ultima Fila///////
            Rectangle r = TablaCC.getCellRect(TablaCC.getRowCount() - 1, 0, true);
            TablaCC.scrollRectToVisible(r);
            TablaCC.getSelectionModel().setSelectionInterval(TablaCC.getRowCount() - 1, TablaCC.getRowCount() - 1);
            //////////////////////////
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargartablaEgresos() {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        /////Tabla Egresos
        String[] Titulo1 = {"N°", "Fecha", "Descripción", "Total ($)"};
        String[] Registros1 = new String[4];

        String sql = "SELECT idEgreso,fecha,descripcion,total FROM egresos ";
        model2 = new DefaultTableModel(null, Titulo1) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros1[0] = rs.getString(1);
                Registros1[1] = rs.getString(2);
                Registros1[2] = rs.getString(3);
                Registros1[3] = rs.getString(4);
                model2.addRow(Registros1);
            }

            TablaEgreso.setModel(model2);
            TablaEgreso.setAutoCreateRowSorter(true);
            /////ajustar ancho de columna///////
            TablaEgreso.getColumnModel().getColumn(0).setPreferredWidth(20);
            TablaEgreso.getColumnModel().getColumn(1).setPreferredWidth(120);

            alinear();
            TablaEgreso.getColumnModel().getColumn(0).setCellRenderer(alinearIzquierda);
            TablaEgreso.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            TablaEgreso.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            TablaEgreso.getColumnModel().getColumn(3).setCellRenderer(alinearIzquierda);
            ///////Ultima Fila///////
            Rectangle r = TablaEgreso.getCellRect(TablaEgreso.getRowCount() - 1, 0, true);
            TablaEgreso.scrollRectToVisible(r);
            TablaEgreso.getSelectionModel().setSelectionInterval(TablaEgreso.getRowCount() - 1, TablaEgreso.getRowCount() - 1);
            //////////////////////////

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        }
    }

    void filtrar_fecha() {
        Date fechinicial = txtFecha1.getDate();
        if (fechinicial != null) {
            //String finicial = formato.format(fechinicial.getTime());
            Date fechafinal = txtFecha2.getDate();
            System.out.println("fecha inicial: " + formato.format(fechinicial));
            System.out.println("fecha final: " + formato.format(fechafinal));
            if (fechafinal != null) {
                //  String ffinal = formato.format(fechfinal.getTime());
                if (fechinicial.compareTo(fechafinal) <= 0) {

                    String sql = "";
                    //FECHA + ESTADO  + FORMA DE PAGO 
                    if (fechinicial != null && fechafinal != null) {
                        /////Tabla Ingresos    
                        String[] Titulo1 = {"N°", "Fecha", "FormadePago", "Cliente", "Total ($)"};
                        String[] Registros1 = new String[5];

                        String sql1 = "SELECT idPedidos,fecha,formadepago,cliente,total "
                                + "FROM vista_pedidos "
                                + "where estado != 'ANULADO' and (formadepago != 'CUENTA CORRIENTE') AND fecha BETWEEN '" + formato.format(fechinicial) + "' AND '" + formato.format(fechafinal) + "'";
                        model1 = new DefaultTableModel(null, Titulo1) {
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        ConexionMySQL cc = new ConexionMySQL();
                        Connection cn = cc.Conectar();
                        try {
                            Statement st = cn.createStatement();
                            ResultSet rs = st.executeQuery(sql1);
                            while (rs.next()) {
                                Registros1[0] = rs.getString(1);
                                Registros1[1] = rs.getString(2);
                                Registros1[2] = rs.getString(3);
                                Registros1[3] = rs.getString(4);
                                Registros1[4] = rs.getString(5);
                                model1.addRow(Registros1);

                            }
                            TablaPedidos.setModel(model1);
                            TablaPedidos.setAutoCreateRowSorter(true);
                            /////ajustar ancho de columna///////
                            TablaPedidos.getColumnModel().getColumn(0).setPreferredWidth(20);
                            TablaPedidos.getColumnModel().getColumn(1).setPreferredWidth(120);
                            TablaPedidos.getColumnModel().getColumn(4).setPreferredWidth(120);
                            alinear();
                            TablaPedidos.getColumnModel().getColumn(0).setCellRenderer(alinearIzquierda);
                            TablaPedidos.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
                            TablaPedidos.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
                            TablaPedidos.getColumnModel().getColumn(3).setCellRenderer(alinearIzquierda);
                            TablaPedidos.getColumnModel().getColumn(4).setCellRenderer(alinearIzquierda);
                            ///////Ultima Fila///////
                            Rectangle r = TablaPedidos.getCellRect(TablaPedidos.getRowCount() - 1, 0, true);
                            TablaPedidos.scrollRectToVisible(r);
                            TablaPedidos.getSelectionModel().setSelectionInterval(TablaPedidos.getRowCount() - 1, TablaPedidos.getRowCount() - 1);
                            //////////////////////////
                        } catch (SQLException ex) {

                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                }
            }
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);

        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void cargartotales() {
        double totalingreso = 0.00, totalingresoPedidos = 0.00, totalingresoCC = 0.00, sumatoria, totalegreso = 0.00, sumatoria2, totalcmv = 0.00, sumatoria3 = 0.00;
        //AQUI SE SUMAN LOS VALORES DE LA COLUMNAS
        int totalRow = TablaPedidos.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x = TablaPedidos.getValueAt(i, 4).toString();
            sumatoria = Double.valueOf(x).doubleValue();
            totalingresoPedidos = totalingresoPedidos + sumatoria;

        }
        txtingresosPedidos.setText(String.valueOf(Redondear(totalingresoPedidos)));

        int totalRow3 = TablaCC.getRowCount();
        totalRow3 -= 1;
        for (int i = 0; i <= (totalRow3); i++) {
            String x = TablaCC.getValueAt(i, 4).toString();
            sumatoria3 = Double.valueOf(x).doubleValue();
            totalingresoCC = totalingresoCC + sumatoria3;

        }
        txtingresosCC.setText(String.valueOf(Redondear(totalingresoCC)));

        int totalRow2 = TablaEgreso.getRowCount();
        totalRow2 -= 1;
        for (int i = 0; i <= (totalRow2); i++) {
            String y = TablaEgreso.getValueAt(i, 3).toString();
            sumatoria2 = Double.valueOf(y).doubleValue();
            totalegreso = totalegreso + sumatoria2;

        }
        txtegresos.setText(String.valueOf(Redondear(totalegreso)));

        txtingresostotal.setText(String.valueOf(Redondear(totalingresoPedidos + totalingresoCC)));
        txtsaldo.setText(String.valueOf(Redondear(totalingresoPedidos + totalingresoCC - totalegreso)));
    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargarfecha() {
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);

    }

    public String reverse(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaPedidos = new javax.swing.JTable();
        txtegresos = new javax.swing.JTextField();
        btnsalir = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaEgreso = new javax.swing.JTable();
        txtingresosPedidos = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtsaldo = new javax.swing.JTextField();
        btnimprimir = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TablaCC = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtingresosCC = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        txtFecha2 = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        txtFecha1 = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        btnfiltrar2 = new javax.swing.JButton();
        txtingresostotal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingresos por Pedidos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        TablaPedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        TablaPedidos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TablaPedidos.setOpaque(false);
        TablaPedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaPedidosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(TablaPedidos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtegresos.setEditable(false);
        txtegresos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtegresos.setForeground(new java.awt.Color(255, 0, 0));
        txtegresos.setBorder(null);
        txtegresos.setOpaque(false);
        txtegresos.setSelectedTextColor(new java.awt.Color(240, 240, 240));
        txtegresos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtegresosActionPerformed(evt);
            }
        });

        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Egresos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        TablaEgreso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        TablaEgreso.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TablaEgreso.setOpaque(false);
        jScrollPane2.setViewportView(TablaEgreso);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtingresosPedidos.setEditable(false);
        txtingresosPedidos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtingresosPedidos.setBorder(null);
        txtingresosPedidos.setOpaque(false);
        txtingresosPedidos.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Total Egresos: $");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 204));
        jLabel7.setText("Saldo:");

        txtsaldo.setEditable(false);
        txtsaldo.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        txtsaldo.setForeground(new java.awt.Color(0, 102, 204));
        txtsaldo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtsaldo.setBorder(null);
        txtsaldo.setOpaque(false);
        txtsaldo.setSelectedTextColor(new java.awt.Color(240, 240, 240));
        txtsaldo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsaldoActionPerformed(evt);
            }
        });

        btnimprimir.setMnemonic('i');
        btnimprimir.setText("Imprimir");
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Total Ingresos $:");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingresos por Cuentas Corrientes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        TablaCC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        TablaCC.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TablaCC.setOpaque(false);
        TablaCC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaCCKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(TablaCC);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Total Ingresos $:");

        txtingresosCC.setEditable(false);
        txtingresosCC.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtingresosCC.setBorder(null);
        txtingresosCC.setOpaque(false);
        txtingresosCC.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtFecha2.setDateFormatString("dd-MM-yyyy");
        txtFecha2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtFecha2.setMinimumSize(new java.awt.Dimension(120, 50));
        txtFecha2.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel9.setText("Desde:");

        txtFecha1.setDateFormatString("dd-MM-yyyy");
        txtFecha1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtFecha1.setMinimumSize(new java.awt.Dimension(120, 50));
        txtFecha1.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel10.setText("Hasta:");

        btnfiltrar2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnfiltrar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnfiltrar2.setMnemonic('f');
        btnfiltrar2.setText("Filtrar");
        btnfiltrar2.setMaximumSize(new java.awt.Dimension(120, 50));
        btnfiltrar2.setMinimumSize(new java.awt.Dimension(120, 50));
        btnfiltrar2.setPreferredSize(new java.awt.Dimension(61, 22));
        btnfiltrar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfiltrar2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFecha2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnfiltrar2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnfiltrar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtFecha2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtingresostotal.setEditable(false);
        txtingresostotal.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        txtingresostotal.setForeground(new java.awt.Color(0, 102, 204));
        txtingresostotal.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtingresostotal.setBorder(null);
        txtingresostotal.setOpaque(false);
        txtingresostotal.setSelectedTextColor(new java.awt.Color(240, 240, 240));
        txtingresostotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtingresostotalActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 102, 204));
        jLabel12.setText("Ingresos:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(329, 329, 329)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtingresosCC, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtingresosPedidos, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtingresostotal, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtegresos, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtingresosPedidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtingresosCC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtegresos, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtingresostotal, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TablaPedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaPedidosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            idPedidos = TablaPedidos.getValueAt(TablaPedidos.getSelectedRow(), 0).toString();
            new PedidosDetalle(null, true).setVisible(true);
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {

            TablaEgreso.transferFocus();
            evt.consume();

        }
    }//GEN-LAST:event_TablaPedidosKeyPressed

    private void txtegresosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtegresosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtegresosActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtsaldoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsaldoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsaldoActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed
        String total1 = txtingresosPedidos.getText();
        String total2 = txtegresos.getText();
        String total3 = txtingresosCC.getText();
        String ganancia = txtsaldo.getText();
        try {
            //Mensaje de encabezado
            MessageFormat headerFormat = new MessageFormat("Ingresos Pedidos");
            //Mensaje en el pie de pagina
            MessageFormat footerFormat = new MessageFormat("Total:" + total1 + "                                                                                Saldo:" + " $" + ganancia);
            //Imprimir JTable
            TablaPedidos.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
         try {
            //Mensaje de encabezado
            MessageFormat headerFormat = new MessageFormat("Ingresos Cuenta Corriente");
            //Mensaje en el pie de pagina
            MessageFormat footerFormat = new MessageFormat("Total:" + total3 + "                                                                                Saldo:" + " $" + ganancia);
            //Imprimir JTable
            TablaCC.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        try {
            //Mensaje de encabezado
            MessageFormat headerFormat = new MessageFormat("Egresos");
            //Mensaje en el pie de pagina
            MessageFormat footerFormat = new MessageFormat("Total:" + total2 + "                                                                                Saldo:" + " $" + ganancia);
            //Imprimir JTable
            TablaEgreso.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnimprimirActionPerformed

    private void TablaCCKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaCCKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaCCKeyPressed

    void filtraFechaPedido() {
        String[] Titulo = {"N°", "Fecha", "FormadePago", "Cliente", "Total ($)"};
        String[] Registros = new String[5];

        String pedidos = "SELECT idPedidos,fecha,formadepago,cliente,total,estado FROM vista_pedidos where (formadepago != 'CUENTA CORRIENTE') and (estado ='FACTURADO' or estado is null )";
        model1 = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Date fechinicial = txtFecha1.getDate();
        Statement Selectpedidos = null;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = txtFecha2.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    if ((fecha.compareTo(ffinal) >= 0) && (fecha.compareTo(finicial) >= 0)) {

                        try {
                            Selectpedidos = cn.createStatement();
                            ResultSet rs = Selectpedidos.executeQuery(pedidos);
                            while (rs.next()) {
                                fnReversa r = new fnReversa();
                                if ((r.reverse(rs.getString("fecha")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fecha")).compareTo(finicial) >= 0)) {
                                    Registros[0] = rs.getString(1);
                                    Registros[1] = rs.getString(2);
                                    Registros[2] = rs.getString(3);
                                    Registros[3] = rs.getString(4);
                                    Registros[4] = rs.getString(5);
                                    model1.addRow(Registros);
                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (Selectpedidos != null) {
                                    Selectpedidos.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        TablaPedidos.setModel(model1);
                        TablaPedidos.setAutoCreateRowSorter(true);
                        fnAlinear alinear = new fnAlinear();
                        TablaPedidos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
                        TablaPedidos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
                        TablaPedidos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearDerecha());
                        TablaPedidos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
                        TablaPedidos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

                        TablaPedidos.getColumnModel().getColumn(0).setPreferredWidth(100);
                        TablaPedidos.getColumnModel().getColumn(1).setPreferredWidth(85);
                        TablaPedidos.getColumnModel().getColumn(2).setPreferredWidth(80);
                        TablaPedidos.getColumnModel().getColumn(3).setPreferredWidth(165);
                        TablaPedidos.getColumnModel().getColumn(4).setPreferredWidth(90);
                    } else {
                        JOptionPane.showMessageDialog(null, "Se seleccionó fuera de rango de la fecha actual ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                txtFecha1.setDate(null);
                txtFecha2.setDate(null);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ingreso fecha inicial");
            txtFecha1.setDate(null);
            txtFecha2.setDate(null);
        }
    }

    void filtraFechaEgresos() {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        /////Tabla Egresos
        String[] Titulo1 = {"N°", "Fecha", "Descripción", "Total ($)"};
        String[] Registros1 = new String[4];

        String sql = "SELECT idEgreso,fecha,descripcion,total FROM egresos ";
        model2 = new DefaultTableModel(null, Titulo1) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Date fechinicial = txtFecha1.getDate();
        Statement Selectpedidos = null;
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = txtFecha2.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    if ((fecha.compareTo(ffinal) >= 0) && (fecha.compareTo(finicial) >= 0)) {

                        try {
                            Selectpedidos = cn.createStatement();
                            ResultSet rs = Selectpedidos.executeQuery(sql);
                            while (rs.next()) {
                                fnReversa r = new fnReversa();
                                if ((r.reverse(rs.getString("fecha")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fecha")).compareTo(finicial) >= 0)) {
                                    
                                    Registros1[0] = rs.getString(1);
                                    Registros1[1] = rs.getString(2);
                                    Registros1[2] = rs.getString(3);
                                    Registros1[3] = rs.getString(4);
                                    model2.addRow(Registros1);
                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (Selectpedidos != null) {
                                    Selectpedidos.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        TablaEgreso.setModel(model2);
                        TablaEgreso.setAutoCreateRowSorter(true);
                        fnAlinear alinear = new fnAlinear();
                        TablaEgreso.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
                        TablaEgreso.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
                        TablaEgreso.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
                        TablaEgreso.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());

                        TablaEgreso.getColumnModel().getColumn(0).setPreferredWidth(100);
                        TablaEgreso.getColumnModel().getColumn(1).setPreferredWidth(85);
                        TablaEgreso.getColumnModel().getColumn(2).setPreferredWidth(80);
                        TablaEgreso.getColumnModel().getColumn(3).setPreferredWidth(165);
                    } else {
                        JOptionPane.showMessageDialog(null, "Se seleccionó fuera de rango de la fecha actual ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                txtFecha1.setDate(null);
                txtFecha2.setDate(null);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ingreso fecha inicial");
            txtFecha1.setDate(null);
            txtFecha2.setDate(null);
        }
    }

    void filtraFechaCC() {
        String[] Titulo = {"N°", "Fecha", "FormadePago", "Cliente", "Total ($)"};
        String[] Registros = new String[5];

        String CC = "SELECT fecha,detalle,nombre,haber FROM vista_ctacte_detalle where descripcion = 'PAGO DE CTA CTE'";
        model3 = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Date fechinicial = txtFecha1.getDate();
        Statement Selectpedidos = null;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = txtFecha2.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    if ((fecha.compareTo(ffinal) >= 0) && (fecha.compareTo(finicial) >= 0)) {
                        try {
                            Selectpedidos = cn.createStatement();
                            ResultSet rs = Selectpedidos.executeQuery(CC);
                            int i=1;
                            while (rs.next()) {
                                fnReversa r = new fnReversa();
                                if ((r.reverse(rs.getString("fecha")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fecha")).compareTo(finicial) >= 0)) {
                                    Registros[0] = String.valueOf(i);
                                    Registros[1] = rs.getString(1);
                                    Registros[2] = rs.getString(2);
                                    Registros[3] = rs.getString(3);
                                    Registros[4] = rs.getString(4);
                                    model3.addRow(Registros);
                                    i++;
                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (Selectpedidos != null) {
                                    Selectpedidos.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        TablaCC.setModel(model3);
                        TablaCC.setAutoCreateRowSorter(true);
                        fnAlinear alinear = new fnAlinear();
                        TablaCC.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
                        TablaCC.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
                        TablaCC.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearDerecha());
                        TablaCC.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
                        TablaCC.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

                        TablaCC.getColumnModel().getColumn(0).setPreferredWidth(100);
                        TablaCC.getColumnModel().getColumn(1).setPreferredWidth(85);
                        TablaCC.getColumnModel().getColumn(2).setPreferredWidth(80);
                        TablaCC.getColumnModel().getColumn(3).setPreferredWidth(165);
                        TablaCC.getColumnModel().getColumn(4).setPreferredWidth(90);
                    } else {
                        JOptionPane.showMessageDialog(null, "Se seleccionó fuera de rango de la fecha actual ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                txtFecha1.setDate(null);
                txtFecha2.setDate(null);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ingreso fecha inicial");
            txtFecha1.setDate(null);
            txtFecha2.setDate(null);
        }
    }


    private void btnfiltrar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfiltrar2ActionPerformed
        ///filtrar_fecha();
        //////////////////////////////////////////////////////////////////
        filtraFechaPedido();
        System.out.println("1");
        filtraFechaCC();
        System.out.println("2");
        filtraFechaEgresos();
        System.out.println("3");
        cargartotales();
    }//GEN-LAST:event_btnfiltrar2ActionPerformed

    private void txtingresostotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtingresostotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtingresostotalActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TablaCC;
    private javax.swing.JTable TablaEgreso;
    private javax.swing.JTable TablaPedidos;
    private javax.swing.JButton btnfiltrar2;
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private com.toedter.calendar.JDateChooser txtFecha1;
    private com.toedter.calendar.JDateChooser txtFecha2;
    private javax.swing.JTextField txtegresos;
    private javax.swing.JTextField txtingresosCC;
    private javax.swing.JTextField txtingresosPedidos;
    private javax.swing.JTextField txtingresostotal;
    private javax.swing.JTextField txtsaldo;
    // End of variables declaration//GEN-END:variables
}
