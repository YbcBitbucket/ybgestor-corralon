package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnExportar;
import Clases.fnRedondear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class CuentasCorrientes extends javax.swing.JDialog {

    DefaultTableModel model;
    public static int id_cliente;
    public static String cliente;

    public CuentasCorrientes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        dobleclick();
        cargatorales();
    }

    void cargatorales() {
        int filasel = tablaclientes.getRowCount();
        double suma = 0.0;
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No hay registros en la tabla");
        } else {
            int i = 0;
            while (i < filasel) {                
                suma = suma + Double.valueOf(tablaclientes.getValueAt(i, 9).toString());
                i++;
            }
             fnRedondear redondear=new fnRedondear();
             suma= redondear.dosDigitos(suma);
            txttotal.setText(String.valueOf(suma));
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"id", "Razon Social/Nombre", "Resposabilidad IVA", "Tipo Doc", "Documento", "Margen", "Estado", "Debe", "Haber", "Saldo"};
        String[] Registros = new String[10];
        String sql = "SELECT * FROM vista_ctactes WHERE CONCAT(IvaDocumento, ' ', cliente)LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectClientes = null;
        try {
            SelectClientes = cn.createStatement();
            ResultSet rs = SelectClientes.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                model.addRow(Registros);
            }
            tablaclientes.setModel(model);
            tablaclientes.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablaclientes.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setMinWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaclientes.getColumnModel().getColumn(1).setPreferredWidth(250);
            tablaclientes.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaclientes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(4).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(5).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(6).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(7).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(8).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(9).setPreferredWidth(100);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectClientes != null) {
                    SelectClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablaclientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablaclientes.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
                        cliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 1).toString();
                        new CuentasCorrientesDetalle(null, true).setVisible(true);
                        cargartabla("");
                    }
                }
            }
        });
    }

//////////////////////FUNCION FILTRAR TABLA //////////////////////
    void filtrartabla(String valor) {
        String[] Titulo = {"id", "Razon Social/Nombre", "Resposabilidad IVA", "Tipo Doc", "Documento", "Margen", "Estado", "Debe", "Haber", "Saldo"};
        String[] Registros = new String[10];
        String sql = "SELECT * FROM vista_ctactes WHERE estado='" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectClientes = null;
        try {
            SelectClientes = cn.createStatement();
            ResultSet rs = SelectClientes.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                model.addRow(Registros);
            }
            tablaclientes.setModel(model);
            tablaclientes.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablaclientes.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setMinWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaclientes.getColumnModel().getColumn(1).setPreferredWidth(250);
            tablaclientes.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaclientes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(4).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(5).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(6).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(7).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(8).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(9).setPreferredWidth(100);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectClientes != null) {
                    SelectClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaclientes = new javax.swing.JTable();
        cboestado = new javax.swing.JComboBox();
        buscar = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        btnmodificar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnagregar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();
        btndetalle = new javax.swing.JButton();
        btnpago = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablaclientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaclientesKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaclientes);

        cboestado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Estado", "HABILITADO", "DESHABILITADO" }));
        cboestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboestadoActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        txttotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txttotal.setForeground(new java.awt.Color(0, 102, 153));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(buscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar)
                    .addComponent(cboestado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btndetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Documento.png"))); // NOI18N
        btndetalle.setText("Detalle");
        btndetalle.setMaximumSize(new java.awt.Dimension(120, 50));
        btndetalle.setMinimumSize(new java.awt.Dimension(120, 50));
        btndetalle.setPreferredSize(new java.awt.Dimension(120, 50));
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });

        btnpago.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnpago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Precio.png"))); // NOI18N
        btnpago.setText("Pago");
        btnpago.setMaximumSize(new java.awt.Dimension(120, 50));
        btnpago.setMinimumSize(new java.awt.Dimension(120, 50));
        btnpago.setPreferredSize(new java.awt.Dimension(120, 50));
        btnpago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnpago, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnpago, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
        cargatorales();
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        new CuentasCorrientesAgrega(null, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        int filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            new CuentasCorrientesModifica(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void tablaclientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaclientesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaclientesKeyPressed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablaclientes.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablaclientes);
            nom.add("Tabla Clientes Cta Cte");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            cliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 1).toString();
            new CuentasCorrientesDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btnpagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpagoActionPerformed
        int filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            new CuentasCorrientesPago(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btnpagoActionPerformed

    private void cboestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboestadoActionPerformed
        if (!cboestado.getSelectedItem().toString().equals("Estado")) {
            filtrartabla(cboestado.getSelectedItem().toString());
            cargatorales();
        } else {
            cargartabla("");
            cargatorales();
        }
    }//GEN-LAST:event_cboestadoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btndetalle;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnpago;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cboestado;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaclientes;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JLabel txttotal;
    // End of variables declaration//GEN-END:variables
}
