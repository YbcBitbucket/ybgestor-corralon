package Formularios;

import Clases.ClaseCaja;
import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnEscape;
import Clases.fnExportar;
import Clases.fnReversa;
import Clases.camposegresos;
import Clases.camposingresos;
import static Formularios.Login.usuario;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.io.File;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

public class Caja extends javax.swing.JDialog {

    DefaultTableModel model;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

    /*public static String Username = "leiramtb10@hotmail.com";
    public static String PassWord = "leiraindira18";*/
    public static String Username = "robles.lucasm@gmail.com";
    public static String PassWord = "31589460lmr85";
    String Mensage = "INFORME DIARIO DE CAJA";
    String To = "";
    String Subject = "Cierre de caja";
    String fecha, hora, total_ingresos = "0.0", total_egresos = "0.0", apertura, total, mesas, diferencia;

    ///double total_ingresos=0.00;
    public Caja(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        cargartabla();
    }

    void cargartabla() {

        String[] Titulo = {"Descripcion", "Total", "Diferencia", "Fecha", "Hora", "id_caja"};
        String[] Registros = new String[6];
        String sql = "SELECT * FROM caja";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(2);
                Registros[1] = rs.getString(3);
                Registros[2] = rs.getString(4);
                Registros[3] = rs.getString(5);
                Registros[4] = rs.getString(6);
                Registros[5] = rs.getString(1);
                model.addRow(Registros);
            }
            tablacaja.setModel(model);
            tablacaja.setAutoCreateRowSorter(true);
            //escondo columna 
            tablacaja.getColumnModel().getColumn(5).setMaxWidth(0);
            tablacaja.getColumnModel().getColumn(5).setMinWidth(0);
            tablacaja.getColumnModel().getColumn(5).setPreferredWidth(0);

            tablacaja.getColumnModel().getColumn(0).setPreferredWidth(170);
            tablacaja.getColumnModel().getColumn(1).setPreferredWidth(100);
            tablacaja.getColumnModel().getColumn(2).setPreferredWidth(100);
            tablacaja.getColumnModel().getColumn(3).setPreferredWidth(100);

            fnAlinear alinear = new fnAlinear();
            tablacaja.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearIzquierda());
            tablacaja.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablacaja.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearCentro());
            tablacaja.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            Rectangle r = tablacaja.getCellRect(tablacaja.getRowCount() - 1, 0, true);
            tablacaja.scrollRectToVisible(r);
            tablacaja.getSelectionModel().setSelectionInterval(tablacaja.getRowCount() - 1, tablacaja.getRowCount() - 1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnsalir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablacaja = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        fechainicial = new com.toedter.calendar.JDateChooser();
        btnfiltrar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();
        btnapertura = new javax.swing.JButton();
        btncierre = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        tablacaja.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablacaja);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        fechainicial.setDateFormatString("dd-MM-yyyy");
        fechainicial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        fechainicial.setMinimumSize(new java.awt.Dimension(120, 50));
        fechainicial.setPreferredSize(new java.awt.Dimension(120, 50));

        btnfiltrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnfiltrar.setMnemonic('f');
        btnfiltrar.setText("Filtrar");
        btnfiltrar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnfiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnfiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(181, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnfiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setMnemonic('s');
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btnapertura.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnapertura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Key.png"))); // NOI18N
        btnapertura.setMnemonic('s');
        btnapertura.setText("Apertura");
        btnapertura.setMaximumSize(new java.awt.Dimension(120, 50));
        btnapertura.setMinimumSize(new java.awt.Dimension(120, 50));
        btnapertura.setPreferredSize(new java.awt.Dimension(120, 50));
        btnapertura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaperturaActionPerformed(evt);
            }
        });

        btncierre.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncierre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Bloqueado.png"))); // NOI18N
        btncierre.setMnemonic('s');
        btncierre.setText("Cierre");
        btncierre.setMaximumSize(new java.awt.Dimension(120, 50));
        btncierre.setMinimumSize(new java.awt.Dimension(120, 50));
        btncierre.setPreferredSize(new java.awt.Dimension(120, 50));
        btncierre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncierreActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Mail.png"))); // NOI18N
        jButton1.setText("Enviar Mail");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 810, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnapertura, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btncierre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnapertura, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btncierre, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnfiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfiltrarActionPerformed
        SimpleDateFormat formato2 = new SimpleDateFormat("dd-MM-yyyy");
        Date fechinicial = fechainicial.getDate();
        String finicial = formato2.format(fechinicial.getTime());
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + finicial + ".*"));
        tablacaja.setRowSorter(sorter);
    }//GEN-LAST:event_btnfiltrarActionPerformed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablacaja.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablacaja);
            nom.add("Tabla Caja");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btnaperturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaperturaActionPerformed
        ClaseCaja caja = new ClaseCaja();
        if (caja.iniciocaja() != 0) {
            JOptionPane.showMessageDialog(null, "No realizo el Cierre de Caja");
        }
        cargartabla();
    }//GEN-LAST:event_btnaperturaActionPerformed

    private void btncierreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncierreActionPerformed
        ClaseCaja caja = new ClaseCaja();
        if (caja.iniciocaja() != 0) {
            new CajaCierre(null, true).setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Debe Realizar la Apertura de Caja");
        }
        cargartabla();
    }//GEN-LAST:event_btncierreActionPerformed

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public String reverse(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cursor();
        LinkedList<camposingresos> ResultadosIngresos = new LinkedList<camposingresos>();
        ResultadosIngresos.clear();
        LinkedList<camposegresos> ResultadosEgresos = new LinkedList<camposegresos>();
        ResultadosEgresos.clear();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date currentDate = new java.util.Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        double sumatoria = 0.0;
        /////////////////////////////////////////////////////////////////////
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        String id_caja = "";

        ///////////////////////INGRESOS////////////////
        ///////traigo la tabla con la info de los ingresos
        if (tablacaja.getValueAt(tablacaja.getSelectedRow(), 0).toString().equals("APERTURA")) {
            id_caja = tablacaja.getValueAt(tablacaja.getSelectedRow(), 5).toString();
            try {
                String ingreso;
                ingreso = "SELECT \n"
                        + "vista_pedidos.`idPedidos` as 'Pedido',\n"
                        + "vista_pedidos.`cliente`,\n"
                        + "vista_pedidos.`formadepago`,\n"
                        + "vista_pedidos.`fecha`,\n"
                        + "vista_pedidos_detalle.`codigo`, \n"
                        + "vista_pedidos_detalle.`nombre`, \n"
                        + "vista_pedidos_detalle.`precioVenta`, \n"
                        + "vista_pedidos_detalle.bonificacion,  \n"
                        + "vista_pedidos_detalle.`cantidad`,  \n"
                        + "vista_pedidos_detalle.`tipoiva`,  \n"
                        + "vista_pedidos_detalle.`descuento` as 'Descuento Gral',\n"
                        + "vista_pedidos_detalle.`interesporcentaje`,\n"
                        + "ROUND(\n"
                        + "((((vista_pedidos_detalle.`precioVenta` * ( 1 - (vista_pedidos_detalle.bonificacion / 100))) * (1 + (vista_pedidos_detalle.tipoiva / 100))) * vista_pedidos_detalle.`cantidad`)* ( 1 - (vista_pedidos_detalle.`descuento` / 100))), 2) as 'total'        \n"
                        + "FROM vista_pedidos \n"
                        + "inner join vista_pedidos_detalle on vista_pedidos_detalle.idPedidos = vista_pedidos.idPedidos \n"
                        + "WHERE idcaja=" + id_caja + " order by Pedido";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(ingreso);
                while (rs.next()) {
                    camposingresos fila;
                    fila = new camposingresos(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11) + " %", rs.getString(12) + " %", rs.getString(13));
                    ResultadosIngresos.add(fila);
                }
            } catch (SQLException ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            try {
                //Ingresos por Pedidos PAGO DE CTA CTE
                String sql5 = "SELECT * FROM vista_ctacte_detalle WHERE id_Caja=" + id_caja + " and descripcion='PAGO DE CTA CTE'";
                Statement SelectPedidos = null;
                SelectPedidos = cn.createStatement();
                ResultSet rsPedidosPagosCC = SelectPedidos.executeQuery(sql5);
                while (rsPedidosPagosCC.next()) {
                    camposingresos fila;
                    fila = new camposingresos("---", rsPedidosPagosCC.getString(8), "CONTADO", rsPedidosPagosCC.getString(4), "0000", rsPedidosPagosCC.getString(5), rsPedidosPagosCC.getString(3), "0", "1", "0", "0 %", "0 %", rsPedidosPagosCC.getString(3));
                    ResultadosIngresos.add(fila);

                    sumatoria = sumatoria + rsPedidosPagosCC.getDouble("haber");
                }
                System.out.println("totalPedidosContado con cc " + sumatoria);
            } catch (SQLException ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            /////////traigo el total de ingresos
            try {
                String ingreso;
                ingreso = "SELECT Sum(vista_pedidos.total) FROM vista_pedidos WHERE idcaja=" + id_caja;
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(ingreso);
                while (rs.next()) {
                    sumatoria = sumatoria + rs.getDouble(1);

                }
            } catch (SQLException ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            total_ingresos = String.valueOf(sumatoria);

            ///////////////EGRESOS//////////////       
            ///////traigo la tabla con la info de los Egresos
            System.out.println("fechas " + tablacaja.getValueAt(tablacaja.getSelectedRow(), 3).toString());
            System.out.println("fecha 2" + tablacaja.getValueAt(tablacaja.getSelectedRow() + 1, 3).toString());
            try {
                String egreso;
                egreso = "SELECT * FROM egresos WHERE idcaja=" + id_caja + " order by idEgreso";;
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(egreso);
                while (rs.next()) {
                    camposegresos fila;
                    fila = new camposegresos(rs.getString(2), rs.getString(4) + rs.getString(5), rs.getString(3));
                    ResultadosEgresos.add(fila);
                }
            } catch (SQLException ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            /////////traigo el total de Egresos
            try {
                String egreso;
                egreso = "SELECT Sum(egresos.total) FROM egresos WHERE idcaja=" + id_caja;
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(egreso);
                while (rs.next()) {
                    total_egresos = rs.getString(1);
                    sumatoria = sumatoria - rs.getDouble(1);
                }
            } catch (SQLException ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            if (total_ingresos == null) {
                total_ingresos = "0.0";
            }
            if (total_egresos == null) {
                total_egresos = "0.0";
            }
            if (diferencia == null) {
                diferencia = "0.0";
            }
            ////////////////////JASPER///////////////
            try {
                JasperReport reporteingreso = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ReporteDiarioIngreso.jasper"));
                Map parametrosin = new HashMap();
                parametrosin.put("fecha", fecha);
                parametrosin.put("total_ingresos", total_ingresos);
                ///parametrosin.put("total_egresos", total_egresos);
                //  JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, new JREmptyDataSource());
                //  JasperViewer.viewReport(jPrint, true);
                JasperPrint jPrinting = JasperFillManager.fillReport(reporteingreso, parametrosin, new JRBeanCollectionDataSource(ResultadosIngresos));
                JasperExportManager.exportReportToPdfFile(jPrinting, "C:\\Informe_caja\\" + "Ingresos " + id_caja + " - " + fecha + ".pdf");
                //JasperPrintManager.printReport(jPrint, false);
                //JasperPrintManager.printReport(jPrint, false); 
            } catch (Exception e) {
                cursor2();
                JOptionPane.showMessageDialog(null, e);
            }
            ////egresos///////////////////////////////////////////////////
            try {
                JasperReport reporteegreso = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ReporteDiarioEgreso.jasper"));
                Map parametrosin = new HashMap();
                parametrosin.put("fecha", fecha);
                parametrosin.put("total_egresos", total_egresos);
                ///parametrosin.put("total_egresos", total_egresos);
                //  JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, new JREmptyDataSource());
                //  JasperViewer.viewReport(jPrint, true);
                JasperPrint jPrinting = JasperFillManager.fillReport(reporteegreso, parametrosin, new JRBeanCollectionDataSource(ResultadosEgresos));
                JasperExportManager.exportReportToPdfFile(jPrinting, "C:\\Informe_caja\\" + "Egresos " + id_caja + " - " + fecha + ".pdf");
                //JasperPrintManager.printReport(jPrint, false);
                //JasperPrintManager.printReport(jPrint, false); 
            } catch (Exception e) {
                cursor2();
                JOptionPane.showMessageDialog(null, e);
            }

            //////////////////////////////////////////////////////////////////////
            hora = tablacaja.getValueAt(tablacaja.getSelectedRow(), 4).toString();
            //////////////enviar////////////////////////////////////
            Subject = "Control de caja n°: " + id_caja + " del dia " + fecha;
            Properties props = new Properties();
            props.put("mail.debug", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            /// String mail2 = Username.substring(Username.indexOf("@"), Username.indexOf(".com"));
            //if (mail2.equals("@hotmail")) {
            // props.put("mail.smtp.host", "smtp.live.com");
            /// props.put("mail.smtp.port", "587");
            // }
            // if (mail2.equals("@gmail")) {
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            /// }
            //  if (mail2.equals("@yahoo")) {
            /// props.put("mail.smtp.host", "smtp.mail.yahoo.com");
            ///  props.put("mail.smtp.port", "587");
            /// }
            ///To = "leiramtb10@hotmail.com";
           // To = "kabubi.araoz@gmail.com";
            To = "robles.lucasm@gmail.com";
            // Hotmail o Gmail o yahoo 
            ///props.put("mail.smtp.port", "587");
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Username, PassWord);
                }
            });

            try {
                //////////////////////////////////////////
                BodyPart texto = new MimeBodyPart();
                texto.setText("Caja número: " + id_caja + " usuario: " + usuario);
                // Se compone el adjunto con la imagen.,
                BodyPart adjunto1 = new MimeBodyPart();
                adjunto1.setDataHandler(
                        new DataHandler(new FileDataSource("C:\\Informe_caja\\Ingresos " + id_caja + " - " + fecha + ".pdf")));
                adjunto1.setFileName("Ingresos " + fecha + ".pdf");
                /////////////////////////////////////////////////
                BodyPart adjunto2 = new MimeBodyPart();
                adjunto2.setDataHandler(
                        new DataHandler(new FileDataSource("C:\\Informe_caja\\Egresos " + id_caja + " - " + fecha + ".pdf")));
                adjunto2.setFileName("Egresos " + fecha + ".pdf");
                /////////////////////////////////////////////////
                BodyPart adjunto3 = new MimeBodyPart();
                adjunto3.setDataHandler(
                        new DataHandler(new FileDataSource("C:\\Informe_caja\\Caja " + id_caja + " - " + fecha + ".pdf")));
                adjunto3.setFileName("Caja " + fecha + ".pdf");

                ////////////////////////////////////////////////////////////////////////////////            
                // Se compone el adjunto con el archivo
                MimeMultipart multiParte = new MimeMultipart();
                multiParte.addBodyPart(texto);
                multiParte.addBodyPart(adjunto1);
                multiParte.addBodyPart(adjunto2);
                multiParte.addBodyPart(adjunto3);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(Username));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(To));
                message.setSubject(Subject);
                message.setText(Mensage);
                message.setContent(multiParte);
                // Se envia el correo.
                Transport.send(message);
                cursor2();
                JOptionPane.showMessageDialog(this, "Su mensaje ha sido enviado");
            } catch (MessagingException e) {
                cursor2();
                JOptionPane.showMessageDialog(this, e);
                JOptionPane.showMessageDialog(this, " Error al enviar el mensaje...verifique su conexion a internet...");
                throw new RuntimeException(e);
            }
        } else {
            cursor2();
            JOptionPane.showMessageDialog(null, "Debe posicionarse sobre el registro de apertura...");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnapertura;
    private javax.swing.JButton btncierre;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnfiltrar;
    private javax.swing.JButton btnsalir;
    private com.toedter.calendar.JDateChooser fechainicial;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablacaja;
    // End of variables declaration//GEN-END:variables
}
