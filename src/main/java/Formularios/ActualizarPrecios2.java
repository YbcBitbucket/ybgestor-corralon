/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Clases.ClaseStock;
import Clases.ConexionMySQL;
import static Clases.fnEscape.funcionescape;
import Clases.cboCategoria;
import Clases.cboMarca;
import Clases.cboProveedor;
import Clases.fnCargarFecha;
import Clases.fnRedondear;
import static Formularios.ActualizarPrecios.idDeposito;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class ActualizarPrecios2 extends javax.swing.JDialog {

   /// String idpro = "", idcat = "", idmar = "";

    public ActualizarPrecios2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        this.setLocationRelativeTo(null);
        cargarcategoria();
        cargarproveedor();
        cargarmarca();
        funcionescape(this);
        this.setResizable(false);

    }

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    void cargarcategoria() {
        String sSQL = "";
        Statement SelectCategorias = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbocategoria.setModel(value);

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        cbocategoria.addItem("Categorías");
        sSQL = "SELECT * FROM categorias";

        try {
            SelectCategorias = cn.createStatement();
            ResultSet rs = SelectCategorias.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboCategoria(rs.getInt("idCategorias"), rs.getString("nombre")));
            }
            cbocategoria.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCategorias != null) {
                    SelectCategorias.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargarmarca() {
        Statement SelectMarcas = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbomarca.setModel(value);

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        cbomarca.addItem("Marcas");
        String sSQL = "SELECT * FROM marcas";

        try {
            SelectMarcas = cn.createStatement();
            ResultSet rs = SelectMarcas.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboMarca(rs.getInt("idMarcas"), rs.getString("nombre")));
            }
            cbomarca.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarcas != null) {
                    SelectMarcas.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargarproveedor() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProveedores = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedor.setModel(value);

        String sSQL = "SELECT * FROM proveedores";
        try {
            SelectProveedores = cn.createStatement();
            ResultSet rs = SelectProveedores.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboProveedor(rs.getInt("idProveedores"), rs.getString("razonsocial")));
            }
            cboproveedor.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtgeneral = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnaplicargral = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtproveedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnaplicarprov = new javax.swing.JButton();
        cboproveedor = new javax.swing.JComboBox();
        btnsalir = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        cbocategoria = new javax.swing.JComboBox();
        txtcategoria = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnaplicarcat = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        cbomarca = new javax.swing.JComboBox();
        txtmarca = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnaplicarmarca = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Actualizar Precios"); // NOI18N
        setBounds(new java.awt.Rectangle(6576, 765, 765, 756));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "General", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setText("Porcentaje:");

        txtgeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtgeneralActionPerformed(evt);
            }
        });
        txtgeneral.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtgeneralKeyReleased(evt);
            }
        });

        jLabel4.setText("%");

        btnaplicargral.setMnemonic('s');
        btnaplicargral.setText("Aplicar");
        btnaplicargral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicargralActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtgeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaplicargral, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtgeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(btnaplicargral))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtproveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtproveedorActionPerformed(evt);
            }
        });
        txtproveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtproveedorKeyReleased(evt);
            }
        });

        jLabel2.setText("Porcentaje:");

        jLabel5.setText("%");

        btnaplicarprov.setMnemonic('s');
        btnaplicarprov.setText("Aplicar");
        btnaplicarprov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicarprovActionPerformed(evt);
            }
        });

        cboproveedor.setNextFocusableComponent(txtproveedor);
        cboproveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboproveedorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaplicarprov, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(btnaplicarprov)
                    .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Categoria", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        cbocategoria.setNextFocusableComponent(txtcategoria);
        cbocategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriaActionPerformed(evt);
            }
        });

        txtcategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcategoriaActionPerformed(evt);
            }
        });
        txtcategoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcategoriaKeyReleased(evt);
            }
        });

        jLabel3.setText("Porcentaje:");

        jLabel6.setText("%");

        btnaplicarcat.setMnemonic('s');
        btnaplicarcat.setText("Aplicar");
        btnaplicarcat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicarcatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(cbocategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaplicarcat, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbocategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(btnaplicarcat))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Marca", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        cbomarca.setNextFocusableComponent(txtmarca);
        cbomarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomarcaActionPerformed(evt);
            }
        });

        txtmarca.setNextFocusableComponent(cbomarca);
        txtmarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmarcaActionPerformed(evt);
            }
        });
        txtmarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmarcaKeyReleased(evt);
            }
        });

        jLabel7.setText("Porcentaje:");

        jLabel8.setText("%");

        btnaplicarmarca.setMnemonic('s');
        btnaplicarmarca.setText("Aplicar");
        btnaplicarmarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicarmarcaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmarca, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(cbomarca, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaplicarmarca, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbomarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtmarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(btnaplicarmarca))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtgeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtgeneralActionPerformed
        btnaplicargral.doClick();
    }//GEN-LAST:event_txtgeneralActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtproveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtproveedorActionPerformed

        btnaplicarprov.doClick();
    }//GEN-LAST:event_txtproveedorActionPerformed

    private void cboproveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboproveedorActionPerformed


    }//GEN-LAST:event_cboproveedorActionPerformed

    private void txtcategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcategoriaActionPerformed
        btnaplicarcat.doClick();
    }//GEN-LAST:event_txtcategoriaActionPerformed

    private void cbocategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriaActionPerformed

    }//GEN-LAST:event_cbocategoriaActionPerformed

    private void cbomarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomarcaActionPerformed

    }//GEN-LAST:event_cbomarcaActionPerformed

    private void txtmarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmarcaActionPerformed
        btnaplicarmarca.doClick();
    }//GEN-LAST:event_txtmarcaActionPerformed

    private void btnaplicargralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicargralActionPerformed
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizaPrecioGeneral = null;

        try {
            String sql = "SELECT\n"
                    + "vista_proveedores_detalle.idProductos,\n"
                    + "vista_proveedores_detalle.precioCompra,\n"
                    + "vista_proveedores_detalle.precioVenta,\n"
                    + "vista_proveedores_detalle.bonificacion,\n"
                    + "vista_proveedores_detalle.idDepositos,\n"
                    + "vista_proveedores_detalle.idTipoiva,\n"
                    + "ROUND((((vista_proveedores_detalle.precioVenta / vista_proveedores_detalle.precioCompra) - 1) * 100),2) AS Utilidad,\n"
                    + "vista_proveedores_detalle.idProveedores\n"
                    + "FROM\n"
                    + "vista_proveedores_detalle";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                fnCargarFecha fecha = new fnCargarFecha();
                String fechastock = fecha.cargarfecha();
                int idProducto = rs.getInt(1);
                fnRedondear redondear = new fnRedondear();
                double precio_compra = redondear.dosDigitos(rs.getDouble(2) * (1 + (Double.valueOf(txtgeneral.getText()) / 100)));
                double precio_venta = redondear.dosDigitos(precio_compra * (1 + (rs.getDouble(7) / 100)));

                String pc = String.valueOf(precio_compra);
                String pv = String.valueOf(precio_venta);
                String bonif = rs.getString(4);
                int idDeposito = rs.getInt(5);
                int idiva = rs.getInt(6);
                int idprov = rs.getInt(8);
                ClaseStock act = new ClaseStock();
                i = i + act.ActualizacionPrecio(idProducto, pc, pv, bonif, fechastock, idDeposito, idiva,idprov);

            }
            JOptionPane.showMessageDialog(null, "Se actualizaron " + i + " productos");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizaPrecioGeneral != null) {
                    ActualizaPrecioGeneral.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }//GEN-LAST:event_btnaplicargralActionPerformed

    private void btnaplicarprovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicarprovActionPerformed

        cboProveedor pro = (cboProveedor) cboproveedor.getSelectedItem();
        int idProveedor = pro.getidProveedores();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizaPrecioGeneral = null;

        try {
            String sql = "SELECT\n"
                    + "vista_proveedores_detalle.idProductos,\n"
                    + "vista_proveedores_detalle.precioCompra,\n"
                    + "vista_proveedores_detalle.precioVenta,\n"
                    + "vista_proveedores_detalle.bonificacion,\n"
                    + "vista_proveedores_detalle.idDepositos,\n"
                    + "vista_proveedores_detalle.idTipoiva,\n"
                    + "ROUND((((vista_proveedores_detalle.precioVenta / vista_proveedores_detalle.precioCompra) - 1) * 100),2) AS Utilidad,\n"
                    + "vista_proveedores_detalle.idProveedores\n"
                    + "FROM\n"
                    + "vista_proveedores_detalle where idProveedores=" + idProveedor;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                fnCargarFecha fecha = new fnCargarFecha();
                String fechastock = fecha.cargarfecha();
                int idProducto = rs.getInt(1);
                fnRedondear redondear = new fnRedondear();
                double precio_compra = redondear.dosDigitos(rs.getDouble(2) * (1 + (Double.valueOf(txtproveedor.getText()) / 100)));
                double precio_venta = redondear.dosDigitos(precio_compra * (1 + (rs.getDouble(7) / 100)));

                String pc = String.valueOf(precio_compra);
                String pv = String.valueOf(precio_venta);
                String bonif = rs.getString(4);
                int idDeposito = rs.getInt(5);
                int idiva = rs.getInt(6);
                int idprov = rs.getInt(8);
                ClaseStock act = new ClaseStock();
                i = i + act.ActualizacionPrecio(idProducto, pc, pv, bonif, fechastock, idDeposito, idiva,idprov);

            }
            JOptionPane.showMessageDialog(null, "Se actualizaron " + i + " productos");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizaPrecioGeneral != null) {
                    ActualizaPrecioGeneral.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_btnaplicarprovActionPerformed

    private void btnaplicarcatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicarcatActionPerformed
        cboCategoria cat = (cboCategoria) cbocategoria.getSelectedItem();
        int idCategoria = cat.getidCategorias();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizaPrecioGeneral = null;

        try {
            String sql = "SELECT\n"
                    + "vista_proveedores_detalle.idProductos,\n"
                    + "vista_proveedores_detalle.precioCompra,\n"
                    + "vista_proveedores_detalle.precioVenta,\n"
                    + "vista_proveedores_detalle.bonificacion,\n"
                    + "vista_proveedores_detalle.idDepositos,\n"
                    + "vista_proveedores_detalle.idTipoiva,\n"
                    + "ROUND((((vista_proveedores_detalle.precioVenta / vista_proveedores_detalle.precioCompra) - 1) * 100),2) AS Utilidad,\n"
                    + "vista_proveedores_detalle.idProveedores\n"
                    + "FROM\n"
                    + "vista_proveedores_detalle where idCategorias=" + idCategoria;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                fnCargarFecha fecha = new fnCargarFecha();
                String fechastock = fecha.cargarfecha();
                int idProducto = rs.getInt(1);
                fnRedondear redondear = new fnRedondear();
                double precio_compra = redondear.dosDigitos(rs.getDouble(2) * (1 + (Double.valueOf(txtcategoria.getText()) / 100)));
                double precio_venta = redondear.dosDigitos(precio_compra * (1 + (rs.getDouble(7) / 100)));

                String pc = String.valueOf(precio_compra);
                String pv = String.valueOf(precio_venta);
                String bonif = rs.getString(4);
                int idDeposito = rs.getInt(5);
                int idiva = rs.getInt(6);
                int idprov = rs.getInt(8);
                ClaseStock act = new ClaseStock();
                i = i + act.ActualizacionPrecio(idProducto, pc, pv, bonif, fechastock, idDeposito, idiva,idprov);

            }
            JOptionPane.showMessageDialog(null, "Se actualizaron " + i + " productos");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizaPrecioGeneral != null) {
                    ActualizaPrecioGeneral.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }//GEN-LAST:event_btnaplicarcatActionPerformed

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    private void btnaplicarmarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicarmarcaActionPerformed
        cboMarca marca = (cboMarca) cbomarca.getSelectedItem();
        int idmarcas = marca.getidMarcas();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizaPrecioGeneral = null;

        try {
            String sql = "SELECT\n"
                    + "vista_proveedores_detalle.idProductos,\n"
                    + "vista_proveedores_detalle.precioCompra,\n"
                    + "vista_proveedores_detalle.precioVenta,\n"
                    + "vista_proveedores_detalle.bonificacion,\n"
                    + "vista_proveedores_detalle.idDepositos,\n"
                    + "vista_proveedores_detalle.idTipoiva,\n"
                    + "ROUND((((vista_proveedores_detalle.precioVenta / vista_proveedores_detalle.precioCompra) - 1) * 100),2) AS Utilidad,\n"
                    + "vista_proveedores_detalle.idProveedores\n"
                    + "FROM\n"
                    + "vista_proveedores_detalle where idmarcas=" + idmarcas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                fnCargarFecha fecha = new fnCargarFecha();
                String fechastock = fecha.cargarfecha();
                int idProducto = rs.getInt(1);
                fnRedondear redondear = new fnRedondear();
                double precio_compra = redondear.dosDigitos(rs.getDouble(2) * (1 + (Double.valueOf(txtmarca.getText()) / 100)));
                double precio_venta = redondear.dosDigitos(precio_compra * (1 + (rs.getDouble(7) / 100)));

                String pc = String.valueOf(precio_compra);
                String pv = String.valueOf(precio_venta);
                String bonif = rs.getString(4);
                int idDeposito = rs.getInt(5);
                int idiva = rs.getInt(6);
                int idprov = rs.getInt(8);
                ClaseStock act = new ClaseStock();
                i = i + act.ActualizacionPrecio(idProducto, pc, pv, bonif, fechastock, idDeposito, idiva,idprov);

            }
            JOptionPane.showMessageDialog(null, "Se actualizaron " + i + " productos");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizaPrecioGeneral != null) {
                    ActualizaPrecioGeneral.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_btnaplicarmarcaActionPerformed

    private void txtgeneralKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtgeneralKeyReleased
        if (!isNumeric(txtgeneral.getText())) {
            txtgeneral.setText("");
        }
    }//GEN-LAST:event_txtgeneralKeyReleased

    private void txtproveedorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtproveedorKeyReleased

        if (!isNumeric(txtproveedor.getText())) {
            txtproveedor.setText("");
        }
    }//GEN-LAST:event_txtproveedorKeyReleased

    private void txtcategoriaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcategoriaKeyReleased
        if (!isNumeric(txtcategoria.getText())) {
            txtcategoria.setText("");
        }
    }//GEN-LAST:event_txtcategoriaKeyReleased

    private void txtmarcaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmarcaKeyReleased
        if (!isNumeric(txtmarca.getText())) {
            txtmarca.setText("");
        }
    }//GEN-LAST:event_txtmarcaKeyReleased

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaplicarcat;
    private javax.swing.JButton btnaplicargral;
    private javax.swing.JButton btnaplicarmarca;
    private javax.swing.JButton btnaplicarprov;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cbocategoria;
    private javax.swing.JComboBox cbomarca;
    private javax.swing.JComboBox cboproveedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTextField txtcategoria;
    private javax.swing.JTextField txtgeneral;
    private javax.swing.JTextField txtmarca;
    private javax.swing.JTextField txtproveedor;
    // End of variables declaration//GEN-END:variables
}
