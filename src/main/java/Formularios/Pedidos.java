package Formularios;

import Clases.ClaseCuentaCorriente;
import Clases.ClaseFacturacion;
import Clases.ConexionMySQL;
import Clases.ClasePedidos;
import Clases.fnAlinear;
import Clases.fnReversa;
import static Formularios.Login.id_usuario;
import static Formularios.Main.idpedido;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static Formularios.PedidosDetalle.idPedidos;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.sql.*;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Pedidos extends javax.swing.JDialog {

    DefaultTableModel model;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    String fecha;

    public Pedidos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        cargardatosfacturacion();
        dobleclick();
    }

    //////////////////////FUNCION CARGAR MARCA //////////////////////
    void cargardatosfacturacion() {
        cboestado.removeAllItems();
        cboestado.addItem("PEDIDO");
        cboestado.addItem("FACTURADO");
        cboestado.addItem("ANULADO");

    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    public void cargartabla(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectpedidos = null;
        String[] Titulo = {"Nro Pedido", "Fecha", "Hora", "Cliente", "Subtotal", "Descuento", "Total", "Forma de Pago", "Estado", "id_cliente", "id_caja", "Interes", "Usuario"};
        Object[] Registros = new Object[13];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_pedidos WHERE CONCAT(idpedidos, ' ', cliente)"
                + "LIKE '%" + valor + "%' order by idpedidos";
        try {
            Selectpedidos = cn.createStatement();
            ResultSet rs = Selectpedidos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getInt(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                Registros[12] = rs.getString(13);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (Selectpedidos != null) {
                    Selectpedidos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablapedidos.setModel(model);
        tablapedidos.setAutoCreateRowSorter(true);
        fnAlinear alinear = new fnAlinear();
        tablapedidos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablapedidos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearIzquierda());
        tablapedidos.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearCentro());
        tablapedidos.getColumnModel().getColumn(11).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(12).setCellRenderer(alinear.alinearDerecha());

        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(100);
        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(85);
        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(80);
        tablapedidos.getColumnModel().getColumn(3).setPreferredWidth(165);
        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(105);
        tablapedidos.getColumnModel().getColumn(8).setPreferredWidth(100);
        tablapedidos.getColumnModel().getColumn(11).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(12).setPreferredWidth(90);

        tablapedidos.getColumnModel().getColumn(9).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(9).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setPreferredWidth(0);
        Rectangle r = tablapedidos.getCellRect(tablapedidos.getRowCount() - 1, 0, true);
        tablapedidos.scrollRectToVisible(r);
        tablapedidos.getSelectionModel().setSelectionInterval(tablapedidos.getRowCount() - 1, tablapedidos.getRowCount() - 1);
    }

    //////////////////////FUNCION FILTRAR MARCA//////////////////////
    void filtrarestado(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectpedidos = null;
        //                      0           1       2       3           4           5           6           7               8           9           10          11      12      
        String[] Titulo = {"Nro Pedido", "Fecha", "Hora", "Cliente", "Subtotal", "Descuento", "Total", "Forma de Pago", "Estado", "id_cliente", "id_caja", "Interes", "Usuario"};
        Object[] Registros = new Object[13];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_pedidos WHERE CONCAT(idpedidos, ' ', cliente) AND estado='" + valor + "' order by idpedidos";
        try {
            Selectpedidos = cn.createStatement();
            ResultSet rs = Selectpedidos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getInt(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);
                Registros[9] = rs.getString(10);
                Registros[10] = rs.getString(11);
                Registros[11] = rs.getString(12);
                Registros[12] = rs.getString(13);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (Selectpedidos != null) {
                    Selectpedidos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablapedidos.setModel(model);
        tablapedidos.setAutoCreateRowSorter(true);
        fnAlinear alinear = new fnAlinear();
        tablapedidos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablapedidos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearIzquierda());
        tablapedidos.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearCentro());
        tablapedidos.getColumnModel().getColumn(11).setCellRenderer(alinear.alinearDerecha());
        tablapedidos.getColumnModel().getColumn(12).setCellRenderer(alinear.alinearDerecha());

        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(100);
        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(85);
        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(80);
        tablapedidos.getColumnModel().getColumn(3).setPreferredWidth(165);
        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(105);
        tablapedidos.getColumnModel().getColumn(8).setPreferredWidth(100);
        tablapedidos.getColumnModel().getColumn(11).setPreferredWidth(90);
        tablapedidos.getColumnModel().getColumn(12).setPreferredWidth(90);

        tablapedidos.getColumnModel().getColumn(9).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(9).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setPreferredWidth(0);
        Rectangle r = tablapedidos.getCellRect(tablapedidos.getRowCount() - 1, 0, true);
        tablapedidos.scrollRectToVisible(r);
        tablapedidos.getSelectionModel().setSelectionInterval(tablapedidos.getRowCount() - 1, tablapedidos.getRowCount() - 1);
    }

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablapedidos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablapedidos.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idPedidos = tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString();
                        new PedidosDetalle(null, true).setVisible(true);
                        cargartabla("");
                    }
                }

            }
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapedidos = new javax.swing.JTable();
        cboestado = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        fechafinal = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        fechainicial = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        btnfiltrar = new javax.swing.JButton();
        buscar = new javax.swing.JLabel();
        btnanular = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnfacturar = new javax.swing.JButton();
        btndetalle = new javax.swing.JButton();
        btnimprimir = new javax.swing.JButton();
        btnanular1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablapedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablapedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapedidosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablapedidos);

        cboestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboestadoActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        fechafinal.setDateFormatString("dd-MM-yyyy");
        fechafinal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        fechafinal.setMinimumSize(new java.awt.Dimension(120, 50));
        fechafinal.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("Desde:");

        fechainicial.setDateFormatString("dd-MM-yyyy");
        fechainicial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        fechainicial.setMinimumSize(new java.awt.Dimension(120, 50));
        fechainicial.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel3.setText("Hasta:");

        btnfiltrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnfiltrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnfiltrar.setMnemonic('f');
        btnfiltrar.setText("Filtrar");
        btnfiltrar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setPreferredSize(new java.awt.Dimension(61, 22));
        btnfiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnfiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnfiltrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboestado, 0, 151, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );

        btnanular.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnanular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btnanular.setText("Anular");
        btnanular.setMaximumSize(new java.awt.Dimension(120, 50));
        btnanular.setMinimumSize(new java.awt.Dimension(120, 50));
        btnanular.setPreferredSize(new java.awt.Dimension(120, 50));
        btnanular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnanularActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnfacturar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnfacturar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Noticias.png"))); // NOI18N
        btnfacturar.setText("Facturar");
        btnfacturar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnfacturar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnfacturar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnfacturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfacturarActionPerformed(evt);
            }
        });

        btndetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Documento.png"))); // NOI18N
        btndetalle.setText("Detalle");
        btndetalle.setMaximumSize(new java.awt.Dimension(120, 50));
        btndetalle.setMinimumSize(new java.awt.Dimension(120, 50));
        btndetalle.setPreferredSize(new java.awt.Dimension(120, 50));
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Imprimir.png"))); // NOI18N
        btnimprimir.setText("Imprimir");
        btnimprimir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnimprimir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnimprimir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnanular1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnanular1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Editar.png"))); // NOI18N
        btnanular1.setText("Modificar");
        btnanular1.setMaximumSize(new java.awt.Dimension(120, 50));
        btnanular1.setMinimumSize(new java.awt.Dimension(120, 50));
        btnanular1.setPreferredSize(new java.awt.Dimension(120, 50));
        btnanular1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnanular1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnfacturar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnanular, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnanular1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnfacturar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnanular, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnanular1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(59, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }
    
    private void btnfacturarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfacturarActionPerformed
      int filasel = tablapedidos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            int confirmado = JOptionPane.showConfirmDialog(null, "¿Quiere Facturar el Pedido Nro " + tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString() + " ?");
            if (JOptionPane.YES_NO_OPTION == confirmado) {

                if (null == tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 8)) {
                    int idpedido = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString());
                    int IdCliente = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 9).toString());
                    ClaseFacturacion facturacion = new ClaseFacturacion();

                    //Elejir Pto de Venta
                    int PtodeVenta = 3;
                    if (PtodeVenta != 0) {

                        cursor();
                        int idFacturacion = facturacion.FacturarPedido(IdCliente, idpedido, PtodeVenta);
                        /////////////////////////////////////////////////       
                        cursor2();
                        //////////////////////////////////
                        if (idFacturacion != 0) {

                            facturacion.ImprimirFactura(idFacturacion);

                            cargartabla("");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "No ha seleccionado ningun Punto Venta");
                    }
                }
            }
        }
    }//GEN-LAST:event_btnfacturarActionPerformed

    private void btnanularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnanularActionPerformed
        int filasel = tablapedidos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            int confirmado = JOptionPane.showConfirmDialog(null, "¿Quiere Anuluar el Pedido Nro " + tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString() + " ?");
            if (JOptionPane.YES_NO_OPTION == confirmado) {
                if (null == tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 8)) {
                    int idpedido = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString());
                    ClasePedidos pedido = new ClasePedidos();
                    pedido.AnularPedido(idpedido);
                    ClaseCuentaCorriente cc = new ClaseCuentaCorriente();
                    int idCliente = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 9).toString());
                    System.out.println("aaaa");
                    if (idCliente != 1) {
                        if (cc.idCuentaCorriente(idCliente) != 0) {
                            System.out.println("bbbb");
                            cc.anulacionCuentaCorriente(cc.idCuentaCorriente(idCliente), idpedido, Double.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 6).toString()));
                            System.out.println("cccc");
                        }
                    }
                    cargartabla("");
                } else {
                    JOptionPane.showMessageDialog(null, "El Pedido no se puede Anular");
                }
            }
        }
    }//GEN-LAST:event_btnanularActionPerformed

    private void tablapedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapedidosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablapedidosKeyPressed

    private void cboestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboestadoActionPerformed
        if (!cboestado.getSelectedItem().toString().equals("PEDIDO")) {
            filtrarestado(cboestado.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cboestadoActionPerformed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablapedidos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idPedidos = tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString();
            new PedidosDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btnfiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfiltrarActionPerformed
        String[] Titulo = {"Nro Pedido", "Fecha", "Hora", "Cliente", "Subtotal", "Descuento", "Total", "Forma de Pago", "Estado", "id_cliente", "id_caja", "Interes", "Usuario"};
        Object[] Registros = new Object[13];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_pedidos order by idpedidos";
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Date fechinicial = fechainicial.getDate();
        Statement Selectpedidos = null;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = fechafinal.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    if ((fecha.compareTo(ffinal) >= 0) && (fecha.compareTo(finicial) >= 0)) {
                        model = new DefaultTableModel(null, Titulo) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Selectpedidos = cn.createStatement();
                            ResultSet rs = Selectpedidos.executeQuery(sql);
                            while (rs.next()) {
                                fnReversa r = new fnReversa();
                                if ((r.reverse(rs.getString("fecha")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fecha")).compareTo(finicial) >= 0)) {
                                    Registros[0] = rs.getInt(1);
                                    Registros[1] = rs.getString(2);
                                    Registros[2] = rs.getString(3);
                                    Registros[3] = rs.getString(4);
                                    Registros[4] = rs.getString(5);
                                    Registros[5] = rs.getString(6);
                                    Registros[6] = rs.getString(7);
                                    Registros[7] = rs.getString(8);
                                    Registros[8] = rs.getString(9);
                                    Registros[9] = rs.getString(10);
                                    Registros[10] = rs.getString(11);
                                    Registros[11] = rs.getString(12);
                                    Registros[12] = rs.getString(13);
                                    model.addRow(Registros);
                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (Selectpedidos != null) {
                                    Selectpedidos.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        tablapedidos.setModel(model);
                        tablapedidos.setAutoCreateRowSorter(true);
                        fnAlinear alinear = new fnAlinear();
                        tablapedidos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
                        tablapedidos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearIzquierda());
                        tablapedidos.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearCentro());
                        tablapedidos.getColumnModel().getColumn(11).setCellRenderer(alinear.alinearDerecha());
                        tablapedidos.getColumnModel().getColumn(12).setCellRenderer(alinear.alinearDerecha());

                        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(100);
                        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(85);
                        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(80);
                        tablapedidos.getColumnModel().getColumn(3).setPreferredWidth(165);
                        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(90);
                        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(90);
                        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(90);
                        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(105);
                        tablapedidos.getColumnModel().getColumn(8).setPreferredWidth(100);
                        tablapedidos.getColumnModel().getColumn(11).setPreferredWidth(90);
                        tablapedidos.getColumnModel().getColumn(12).setPreferredWidth(90);

                        tablapedidos.getColumnModel().getColumn(9).setMaxWidth(0);
                        tablapedidos.getColumnModel().getColumn(9).setMinWidth(0);
                        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(0);
                        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
                        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
                        tablapedidos.getColumnModel().getColumn(10).setPreferredWidth(0);
                        Rectangle r = tablapedidos.getCellRect(tablapedidos.getRowCount() - 1, 0, true);
                        tablapedidos.scrollRectToVisible(r);
                        tablapedidos.getSelectionModel().setSelectionInterval(tablapedidos.getRowCount() - 1, tablapedidos.getRowCount() - 1);
                    } else {
                        JOptionPane.showMessageDialog(null, "Se seleccionó fuera de rango de la fecha actual ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                fechainicial.setDate(null);
                fechafinal.setDate(null);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ingreso fecha inicial");
            fechainicial.setDate(null);
            fechafinal.setDate(null);
        }
    }//GEN-LAST:event_btnfiltrarActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed
        int filasel = tablapedidos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            int confirmado = JOptionPane.showConfirmDialog(null, "¿Quiere Imprimir el Pedido Nro " + tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString() + " ?");
            if (JOptionPane.YES_NO_OPTION == confirmado) {
                int idpedido = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString());
                ClasePedidos pedido = new ClasePedidos();
                pedido.ImprimirPedido(idpedido);
                cargartabla("");
            }
        }
    }//GEN-LAST:event_btnimprimirActionPerformed

    private void btnanular1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnanular1ActionPerformed
        int filasel = tablapedidos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            int confirmado = JOptionPane.showConfirmDialog(null, "¿Quiere Modificar el Pedido Nro " + tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString() + " ?");
            if (JOptionPane.YES_NO_OPTION == confirmado) {
                Main.ModificarPedido = 1;
                idpedido = Integer.valueOf(tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString());
                this.dispose();
                cargartabla("");
            }
        }
    }//GEN-LAST:event_btnanular1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnanular;
    private javax.swing.JButton btnanular1;
    private javax.swing.JButton btndetalle;
    private javax.swing.JButton btnfacturar;
    private javax.swing.JButton btnfiltrar;
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cboestado;
    private com.toedter.calendar.JDateChooser fechafinal;
    private com.toedter.calendar.JDateChooser fechainicial;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablapedidos;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
