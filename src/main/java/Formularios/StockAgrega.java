package Formularios;

import Clases.ClaseProductos;
import Clases.ClaseStock;
import Clases.ConexionMySQL;
import Clases.fnEscape;
import Clases.cboTipoIva;
import Clases.cboDeposito;
import Clases.cboProveedor;
import Clases.fnCargarFecha;
import Clases.fnRedondear;
import Clases.fnesNumerico;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class StockAgrega extends javax.swing.JDialog {

    DefaultTableModel model;
    int mod = 0, idusuarios, idProducto;
    String fechastock;

    public StockAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        cargartablaproducto("");
        dobleclick();
        cargardatosdepositos();
        cargarTipoIva();
        cargardatosproveedor();
        cbotipoiva.setSelectedIndex(2);
    }

    void cargartablaproducto(String Valor) {
        String[] Titulo = {"Id", "Codigo", "Nombre Producto"};
        String[] Registros = new String[3];
        String sql = "SELECT idProductos, codigo, nombre FROM productos "
                + "WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + Valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProducto = null;
        try {
            SelectProducto = cn.createStatement();
            ResultSet rs = SelectProducto.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                model.addRow(Registros);
            }
            tablaproducto.setModel(model);
            tablaproducto.setAutoCreateRowSorter(true);
            tablaproducto.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setMinWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaproducto.getColumnModel().getColumn(1).setPreferredWidth(30);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProducto != null) {
                    SelectProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargarTipoIva() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipoiva.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoIVA = null;
        String sSQL = "SELECT * FROM afip_tipoiva";
        try {
            SelectTipoIVA = cn.createStatement();
            ResultSet rs = SelectTipoIVA.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoIva(rs.getInt("idTipoiva"), rs.getString("descripcion")));
            }
            cbotipoiva.setSelectedIndex(2);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoIVA != null) {
                    SelectTipoIVA.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void dobleclick() {
        tablaproducto.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
                    txtproducto.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());
                    cargar_ultimo_precio(idProducto);
                    cargardatosproveedorseleccionado(idProducto);
                }
            }
        });
    }

    void cargardatosproveedorseleccionado(int idProducto) {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedores.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement proveedor = null;
        Statement SelectProveedores = null;
        String sSQL2 = "SELECT proveedores.razonsocial FROM stock inner join proveedores on proveedores.idproveedores = stock.idproveedores where idProductos=" + idProducto + " and precioCompra is not null  order by idstock DESC limit 1";
        String sSQL = "SELECT idproveedores,razonsocial FROM proveedores";
        try {
            proveedor = cn.createStatement();
            ResultSet rs = proveedor.executeQuery(sSQL2);
            if (rs.next()) {
                SelectProveedores = cn.createStatement();
                ResultSet rs2 = SelectProveedores.executeQuery(sSQL);
                int index = 0;
                int i = 0;
                while (rs2.next()) {
                    if (rs2.getString(2).equals(rs.getString(1))) {
                        index = i;
                    }
                    value.addElement(new cboProveedor(rs2.getInt("idProveedores"), rs2.getString("razonsocial")));
                    i++;
                }
                cboproveedores.setSelectedIndex(index);
            } else {
                cargardatosproveedor();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (proveedor != null) {
                    proveedor.close();
                }
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargar_ultimo_precio(int valor) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectPrecio = null;
        int bandera = 0;
        String sSQL = "SELECT precioCompra, precioVenta, afip_tipoiva.descripcion FROM stock inner join afip_tipoiva on afip_tipoiva.idTipoiva = stock.idTipoiva where idProductos=" + valor + " and precioCompra is not null  order by idstock DESC limit 1";
        try {
            SelectPrecio = cn.createStatement();
            ResultSet rs = SelectPrecio.executeQuery(sSQL);
            while (rs.next()) {

                txtpcompra.setText(rs.getString(1));
                txtpventa.setText(rs.getString(2));
                if (rs.getString(3).equals("21")) {
                    cbotipoiva.setSelectedIndex(4);
                } else {
                    cbotipoiva.setSelectedIndex(2);
                }
                fnRedondear r = new fnRedondear();
                txtutilidad.setText(Double.toString(r.dosDigitos((((rs.getDouble(2)/rs.getDouble(1)) - 1)*100))));
                bandera = 1;
            }
            if (bandera == 0) {
                txtpcompra.setText("");
                txtpventa.setText("");
                cbotipoiva.setSelectedIndex(2);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPrecio != null) {
                    SelectPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    /////CARGAR DEPOSITOS/////////////
    void cargardatosdepositos() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDepositos = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbodeposito.setModel(value);

        String sSQL = "SELECT * FROM deposito";
        try {
            SelectDepositos = cn.createStatement();
            ResultSet rs = SelectDepositos.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboDeposito(rs.getInt("idDepositos"), rs.getString("nombre")));
            }
            cbodeposito.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDepositos != null) {
                    SelectDepositos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    /////CARGAR PROVEEDORES/////////////
    void cargardatosproveedor() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProveedores = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedores.setModel(value);

        String sSQL = "SELECT * FROM proveedores";
        try {
            SelectProveedores = cn.createStatement();
            ResultSet rs = SelectProveedores.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboProveedor(rs.getInt("idProveedores"), rs.getString("razonsocial")));
            }
            cboproveedores.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GruopMoneda = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtproducto = new javax.swing.JTextField();
        txtcantidad = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtpcompra = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtpventa = new javax.swing.JTextField();
        cbodeposito = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cbotipoiva = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        cboproveedores = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        txtncbte = new javax.swing.JTextField();
        txtutilidad = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnagregar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproducto = new javax.swing.JTable();
        buscar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Producto:");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Cantidad:");

        txtproducto.setEditable(false);

        txtcantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcantidadKeyReleased(evt);
            }
        });

        jLabel4.setText("Precio Compra:");

        txtpcompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpcompraKeyReleased(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Precio Venta:");

        txtpventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpventaActionPerformed(evt);
            }
        });
        txtpventa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpventaKeyReleased(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Deposito");

        jLabel24.setText("Porcentaje IVA:");

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Proveedor");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("N° Comprobante:");

        txtutilidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtutilidadActionPerformed(evt);
            }
        });

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Utilidad:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboproveedores, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtncbte))
                        .addGap(93, 93, 93))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 27, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel24))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(28, 28, 28)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtproducto, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 8, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cbodeposito, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(93, 93, 93))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtcantidad)
                                    .addComponent(txtpcompra, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtutilidad, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtpventa, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbotipoiva, javax.swing.GroupLayout.Alignment.LEADING, 0, 91, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtproducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtpcompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtutilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(txtpventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(cbotipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbodeposito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboproveedores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtncbte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setMnemonic('a');
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btncancelar.setMnemonic('s');
        btncancelar.setText("Salir");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablaproducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaproducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaproductoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproducto);

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 546, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(buscar)
                        .addGap(10, 10, 10)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        if (!"".equals(txtproducto.getText()) && !"".equals(txtcantidad.getText())
                && !"".equals(txtpcompra.getText()) && !"".equals(txtpventa.getText())) {
            ///////////////////agregar datos//////////////////////////////
            fnCargarFecha fecha = new fnCargarFecha();
            String fechastock = fecha.cargarfecha();

            double cantidad = Double.parseDouble(txtcantidad.getText());

            cboTipoIva iva = (cboTipoIva) cbotipoiva.getSelectedItem();
            int idTipoiva = iva.getidTipoIva();

            cboDeposito dep = (cboDeposito) cbodeposito.getSelectedItem();
            int idDeposito = dep.getidDeposito();

            cboProveedor pro = (cboProveedor) cboproveedores.getSelectedItem();
            int idProveedor = pro.getidProveedores();

            ClaseStock stock = new ClaseStock();
            if (stock.AgregarStock(cantidad, fechastock, txtpcompra.getText(), txtpventa.getText(), idProducto, idDeposito, idTipoiva, idProveedor, txtncbte.getText()) == 1) {
                txtproducto.setText("");
                txtcantidad.setText("");
                txtpcompra.setText("");
                txtpventa.setText("");
                cbodeposito.setSelectedIndex(0);
                cboproveedores.setSelectedIndex(0);
                txtncbte.setText("");
                cargarTipoIva();

            }
        } else {
            JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
        }

    }//GEN-LAST:event_btnagregarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        if (mod == 1) {
            txtproducto.setText("");
            txtcantidad.setText("");
            txtpcompra.setText("");
            txtpventa.setText("");
            cbodeposito.setSelectedIndex(0);
            cboproveedores.setSelectedIndex(0);
            txtncbte.setText("");
            cargarTipoIva();
            mod = 0;
            btncancelar.setText("Salir");
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtbuscar.transferFocus();
        }
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartablaproducto(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void tablaproductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
            txtproducto.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());
        }
    }//GEN-LAST:event_tablaproductoKeyReleased

    private void txtcantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantidadKeyReleased
        /*   fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtcantidad.getText())) {
            txtcantidad.setText("");
        }*/
    }//GEN-LAST:event_txtcantidadKeyReleased

    private void txtpventaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpventaKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtpventa.getText())) {
            txtpventa.setText("");
        }
    }//GEN-LAST:event_txtpventaKeyReleased

    private void txtpcompraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpcompraKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtpcompra.getText())) {
            txtpcompra.setText("");
        }
    }//GEN-LAST:event_txtpcompraKeyReleased

    private void txtutilidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtutilidadActionPerformed
        double pc = 0.0;
        double uti = 0.0;
        double pv = 0.0;

        pc = Double.valueOf(txtpcompra.getText());
        uti = Double.valueOf(txtutilidad.getText());
        pv = (((uti * pc) / 100)+ pc);
        fnRedondear r = new fnRedondear();
        txtpventa.setText(Double.toString(r.dosDigitos(pv)));
    }//GEN-LAST:event_txtutilidadActionPerformed

    private void txtpventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpventaActionPerformed
        double pc = 0.0;
        double ut = 0.0;
        double pv = 0.0;

        pc = Double.valueOf(txtpcompra.getText());
        pv = Double.valueOf(txtpventa.getText());
        ut = (((pv *100) / pc)-100);
        fnRedondear r = new fnRedondear();
        txtutilidad.setText(Double.toString(r.dosDigitos(ut)));
    }//GEN-LAST:event_txtpventaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup GruopMoneda;
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cbodeposito;
    private javax.swing.JComboBox cboproveedores;
    private javax.swing.JComboBox cbotipoiva;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaproducto;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcantidad;
    private javax.swing.JTextField txtncbte;
    private javax.swing.JTextField txtpcompra;
    private javax.swing.JTextField txtproducto;
    private javax.swing.JTextField txtpventa;
    private javax.swing.JTextField txtutilidad;
    // End of variables declaration//GEN-END:variables
}
