package Formularios;

import Clases.ClaseIvaCompra;
import Clases.ConexionMySQL;
import Clases.cboTipoCbte;
import Clases.cboTipoDoc;
import Clases.cboTipoIva;
import Clases.fnesNumerico;
import static Formularios.IvaCompra.id_ivacompra;
import static Formularios.IvaCompraAlicuota.id_ivacompraalicuota;
import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.*;

public class IvaCompraAlicuotaModifica extends javax.swing.JDialog {

    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

    public IvaCompraAlicuotaModifica(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardatos(id_ivacompra, id_ivacompraalicuota);

    }

    ///// CARGAR CBO TIPO COMPROBANTE /////
    void cargartipocomprobante() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipocbte.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement TipoCbte = null;
        Statement idTipocbte = null;
        String sSQL = "SELECT * FROM afip_tipocbte";
        String sSQL2 = "SELECT idTipoCbte FROM iva_compra WHERE idIvaCompra=" + id_ivacompra;
        try {
            TipoCbte = cn.createStatement();
            ResultSet rs = TipoCbte.executeQuery(sSQL);
            idTipocbte = cn.createStatement();
            ResultSet rs2 = idTipocbte.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idTipoCbte"))) {
                    index = i;
                }
                value.addElement(new cboTipoCbte(rs.getInt("idTipoCbte"), rs.getString("descripcion")));
                i++;
            }
            cbotipocbte.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TipoCbte != null) {
                    TipoCbte.close();
                }
                if (idTipocbte != null) {
                    idTipocbte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargarproveedor() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedor.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement Proveedores = null;
        Statement idProveedores = null;
        String sSQL = "SELECT * FROM proveedores";
        String sSQL2 = "SELECT idProveedores FROM iva_compra WHERE idIvaCompra=" + id_ivacompra;
        try {
            Proveedores = cn.createStatement();
            ResultSet rs = Proveedores.executeQuery(sSQL);
            idProveedores = cn.createStatement();
            ResultSet rs2 = idProveedores.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idProveedores"))) {
                    index = i;
                }
                value.addElement(new cboTipoDoc(rs.getInt("idProveedores"), rs.getString("razonsocial")));
                i++;
            }
            cboproveedor.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (Proveedores != null) {
                    Proveedores.close();
                }
                if (idProveedores != null) {
                    idProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO IVA /////
    void cargartipoiva() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipoiva.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoIVA = null;
        Statement idTipoIVA = null;
        String sSQL = "SELECT * FROM afip_tipoiva";
        String sSQL2 = "SELECT idTipoiva FROM iva_compra_alicuotas WHERE idIvaCompraAlicuota=" + id_ivacompraalicuota;
        try {
            SelectTipoIVA = cn.createStatement();
            ResultSet rs = SelectTipoIVA.executeQuery(sSQL);
            idTipoIVA = cn.createStatement();
            ResultSet rs2 = idTipoIVA.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idTipoiva"))) {
                    index = i;
                }
                value.addElement(new cboTipoIva(rs.getInt("idTipoiva"), rs.getString("descripcion")));
                i++;
            }
            cbotipoiva.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoIVA != null) {
                    SelectTipoIVA.close();
                }
                if (idTipoIVA != null) {
                    idTipoIVA.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR DATOS //////////////////////
    void cargardatos(int id, int id_ivacompraalicuota) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectIvaCompra = null;
        Statement SelectIvaCompraAlicuota = null;

        String sSQL = "SELECT * FROM iva_compra WHERE idIvaCompra=" + id;
        String sSQLAlicuota = "SELECT * FROM iva_compra_alicuotas WHERE idIvaCompraAlicuota=" + id_ivacompraalicuota;
        try {
            SelectIvaCompra = cn.createStatement();
            ResultSet rs = SelectIvaCompra.executeQuery(sSQL);
            rs.next();
            cargartipocomprobante();
            txtptoventa.setText(rs.getString("puntodeventa"));
            txtnumerocbte.setText(rs.getString("numerocbte"));
            cargarproveedor();
            txttotal.setText(rs.getString("importeoperacion"));

            SelectIvaCompraAlicuota = cn.createStatement();
            ResultSet rs2 = SelectIvaCompraAlicuota.executeQuery(sSQLAlicuota);
            rs2.next();
            txtnetogravado.setText(rs2.getString("importenetogravado"));
            cargartipoiva();
            txtiva.setText(rs2.getString("impuestoliquidado"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectIvaCompra != null) {
                    SelectIvaCompra.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        btnagregar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtnumerocbte = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtnetogravado = new javax.swing.JTextField();
        txtiva = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cbotipoiva = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txttotal = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        cboproveedor = new javax.swing.JComboBox();
        cbotipocbte = new javax.swing.JComboBox();
        txtptoventa = new javax.swing.JTextField();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Comprobante IVA Compra", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnagregar.setText("Modificar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jLabel3.setText("Tipo de Comprobante");

        jLabel11.setText("Punto de Venta");

        txtnumerocbte.setEditable(false);

        jLabel12.setText("Número de Comprobante");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Iva", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setText("Alícuota de IVA");

        jLabel6.setText("Importe Neto Gravado");

        txtnetogravado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnetogravadoKeyReleased(evt);
            }
        });

        txtiva.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtivaKeyReleased(evt);
            }
        });

        jLabel9.setText("Importe IVA");

        jLabel7.setText("ImporteTotal");

        txttotal.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtiva, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                        .addComponent(txtnetogravado, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(cbotipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(137, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtnetogravado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbotipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vendedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel10.setText("Proveedor");

        cboproveedor.setEnabled(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addGap(18, 18, 18)
                .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        cbotipocbte.setEnabled(false);

        txtptoventa.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbotipocbte, 0, 301, Short.MAX_VALUE)
                            .addComponent(txtnumerocbte)
                            .addComponent(txtptoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbotipocbte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtptoventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnumerocbte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btncancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        //Datos entrada
        cboTipoIva tipoiva = (cboTipoIva) cbotipoiva.getSelectedItem();
        int IdtipoIva = tipoiva.getidTipoIva();

        double netogravado = 0.0;
        double iva = 0.0;

        if (!"".equals(txtnetogravado.getText())) {
            netogravado = Double.valueOf(txtnetogravado.getText());
        }
        if (!"".equals(txtiva.getText())) {
            iva = Double.valueOf(txtiva.getText());
        }

        ClaseIvaCompra IvaCompra = new ClaseIvaCompra();
        IvaCompra.ModificarIvaCompraAlicuota(id_ivacompraalicuota, netogravado, IdtipoIva, iva);
        cargardatos(id_ivacompra, id_ivacompraalicuota);
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        cargardatos(id_ivacompra, id_ivacompraalicuota);
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txtivaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtivaKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtiva.getText())) {
            txtiva.setText("");
        }
    }//GEN-LAST:event_txtivaKeyReleased

    private void txtnetogravadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnetogravadoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtnetogravado.getText())) {
            txtnetogravado.setText("");
        }
    }//GEN-LAST:event_txtnetogravadoKeyReleased
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cboproveedor;
    private javax.swing.JComboBox cbotipocbte;
    private javax.swing.JComboBox cbotipoiva;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField txtiva;
    private javax.swing.JTextField txtnetogravado;
    private javax.swing.JTextField txtnumerocbte;
    private javax.swing.JTextField txtptoventa;
    private javax.swing.JTextField txttotal;
    // End of variables declaration//GEN-END:variables

}
