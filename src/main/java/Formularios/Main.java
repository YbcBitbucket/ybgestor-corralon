package Formularios;

import Clases.ClaseCaja;
import Clases.ClaseCuentaCorriente;
import Clases.ClaseFacturacionTimer;
import Clases.ConexionMySQL;
import Clases.cboDeposito;
import Clases.ClasePedidos;
import Clases.fnEditarCeldas;
import Clases.ClasePresupuestos;
import Clases.cboFormaCheque;
import Clases.cboFormaCtaCte;
import Clases.cboFormasdepago;
import Clases.cboTarjetaCredito;
import Clases.cboTarjetaDebito;
import Clases.fnAlinear;
import Clases.fnCargarFecha;
import Clases.fnCompletar;
import Clases.fnRedondear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import static Formularios.Descripcion_Pedido.descripcion;
import static Formularios.Login.id_usuario;
import static Formularios.Login.usuario;

public class Main extends javax.swing.JFrame {

    DefaultTableModel model, model2, model3;
    public static int ConvertirPresupuesto = 0, AgregarPresupuesto = 0, ModificarPedido = 0;
    public static double bcant = 0;
    public static String total, subtotal;
    public static String nombreCliente, ResposabilidadCliente, TipodocCliente, DocCliente;
    public static int idpedido = 0, IdDeposito = 0, IdCliente = 0, idPtodeVenta = 0, idPresupuesto = 0, idFormaDePago = 0, idTipoDePago = 0;
    //Contado, Ctacte, TCredito 1 2 3 4, TDebito 1 2 3 4, Cheque, Otros
    static double[] descuentobase = new double[12];
    static double[] descuentoporcentaje = new double[12];
    static double[] interesporcentaje = new double[12];
    static double[] interesbase = new double[12];
    static int[] formas_detalle = new int[100];
    cboFormasdepago[] forma = new cboFormasdepago[7];
    static int indexformapago = 0;

    public Main() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());

        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setExtendedState(MAXIMIZED_BOTH);
        ////// FUNCIONES /////
        AccesoUsuarios();
        ClaseCaja caja = new ClaseCaja();
        caja.iniciocaja();
        cargarconsumidorfinal();
        cargarnumero();
        cargarformadepago();
        cargartarjetacredito();
        cargartarjetadebito();
        cargarformactacte();
        cargarformacheque();
        cargardepositos();
        //cargartablabuscar("");
        txtinteres.setText("0.0");
        txtusuario.setText(usuario);
        cargatotales();
        dobleclickagregaproducto();
        tamañotablapedido();
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        indexformapago = 0;
    }

    void Timer() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;
        String sSQL = "SELECT * FROM afip_datos";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            rs.next();
            ClaseFacturacionTimer Timer = new ClaseFacturacionTimer();
            if (rs.getInt("automatizacion") == 1) {

                Timer.ClaseFacturacionTimer(rs.getInt("minimo") * 60000, rs.getInt("maximo") * 60000);
                Timer.FacturarZ(rs.getInt("horaZ"), rs.getInt("minutoZ"));
            } else {
                Timer.stopTimer();
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    void AccesoUsuarios() {
        //Datos
        char[] acceso_datos = Login.acceso_datos.toCharArray();
        if (acceso_datos[0] == '0') {
            mncategoria.setVisible(false);
        }
        if (acceso_datos[1] == '0') {
            mnmarcas.setVisible(false);
        }
        if (acceso_datos[2] == '0') {
            mnproveedor.setVisible(false);
        }
        if (acceso_datos[3] == '0') {
            mnproducto.setVisible(false);
        }
        if (acceso_datos[4] == '0') {
            mnstock.setVisible(false);
        }
        if (acceso_datos[5] == '0') {
            mnclientes.setVisible(false);
        }
        if (acceso_datos[6] == '0') {
            mnivacompra.setVisible(false);
        }
        if (acceso_datos[7] == '0') {
            mnegresos.setVisible(false);
        }
        if (acceso_datos[8] == '0') {
            mnusuarios.setVisible(false);
        }
        //Ventas
        char[] acceso_ventas = Login.acceso_ventas.toCharArray();
        if (acceso_ventas[0] == '0') {
            mnpedidos.setVisible(false);
        }
        if (acceso_ventas[1] == '0') {
            mnpresupuestos.setVisible(false);
        }
        if (acceso_ventas[2] == '0') {
            mnbonificacion.setVisible(false);
        }
        if (acceso_ventas[3] == '0') {
            mnformasdepago.setVisible(false);
        }
        if (acceso_ventas[4] == '0') {
            mnctacte.setVisible(false);
        }
        if (acceso_ventas[5] == '0') {
            mncaja.setVisible(false);
        }
        //Consultas
        char[] acceso_consultas = Login.acceso_consultas.toCharArray();
        if (acceso_consultas[0] == '0') {
            RankingClientes.setVisible(false);
        }
        if (acceso_consultas[1] == '0') {
            RankingProductos.setVisible(false);
        }
        if (acceso_consultas[2] == '0') {
            OrdenesdeCompra.setVisible(false);
        }
        //Afip
        char[] acceso_afip = Login.acceso_afip.toCharArray();
        if (acceso_afip[0] == '0') {
            mntipofacturacion.setVisible(false);
        }

        /////////Tipo Facturacion
        String sql = "SELECT descripcion FROM afip_tipo_facturacion WHERE tipo=" + 1;
        String tipoFacturacion = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Select = null;
        try {
            Select = cn.createStatement();
            ResultSet rs = Select.executeQuery(sql);
            while (rs.next()) {
                tipoFacturacion = rs.getString(1);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (Select != null) {
                    Select.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        if (tipoFacturacion.equals("WSFE")) {
            jMenuWebServices.setVisible(true);

        }

    }

    void cargarconsumidorfinal() {
        String sql = "SELECT * FROM vista_clientes WHERE idClientes=" + 1;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectCliente = null;
        try {
            SelectCliente = cn.createStatement();
            ResultSet rs = SelectCliente.executeQuery(sql);
            while (rs.next()) {
                IdCliente = rs.getInt(1);
                nombreCliente = rs.getString(2);
                ResposabilidadCliente = rs.getString(3);
                TipodocCliente = rs.getString(4);
                DocCliente = rs.getString(5);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCliente != null) {
                    SelectCliente.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        txtnombreCliente.setText(nombreCliente);
        txtresposabilidadCliente.setText(ResposabilidadCliente);
        txttipodocCliente.setText(TipodocCliente);
        txtdocCliente.setText(DocCliente);
    }

    ///// CARGAR CBO DEPOSITOS /////
    void cargardepositos() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbodeposito.setModel(value);
        IdDeposito = 1;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDepositos = null;
        String sSQL = "SELECT idDepositos, nombre FROM deposito";
        try {
            SelectDepositos = cn.createStatement();
            ResultSet rs = SelectDepositos.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboDeposito(rs.getInt("idDepositos"), rs.getString("nombre")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDepositos != null) {
                    SelectDepositos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// CARGAR TABLA BUSCAR /////
    void cargartablabuscar(String valor) {
        //                  0       1        2               3         4      5        6       7       8           9
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal", "Descripcion"};
        String[] Registros = new String[9];
        String sql = "SELECT * FROM vista_productos_busqueda WHERE idDepositos=" + IdDeposito + "";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString("precioVenta");
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                ////////////////////////
                Registros[7] = rs.getString("stock");
                Registros[8] = rs.getString("precioVenta");
                model.addRow(Registros);

            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(3).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(4).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(5).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(6).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(0);

            tablabuscar.getColumnModel().getColumn(9).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(9).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(9).setPreferredWidth(0);
            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(20);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(new fnEditarCeldas());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// FILTRAR TABLA BUSCAR DEPOSITOS /////
    void filtrartablabuscardep(int codigo) {
        //                  0       1           2       3             4         5       6       7       8
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal"};
        String[] Registros = new String[9];
        String sql = "SELECT * FROM vista_productos_busqueda WHERE idDepositos=" + codigo + "";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString("precioVenta");
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                ////////////////////////
                Registros[7] = rs.getString("stock");
                Registros[8] = rs.getString("precioVenta");
                model.addRow(Registros);
            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna //0 3 5 6
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(3).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(4).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(5).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(6).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(0);

            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(20);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(new fnEditarCeldas());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// ULTIMO NUMERO PEDIDO /////
    void cargarnumero() {
        if ("Pedido".equals(tipoventa.getText())) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            Statement SelectIdPedidos = null;
            String sSQL = "SELECT MAX(idPedidos) AS idPedidos FROM pedidos";
            fnCompletar c = new fnCompletar();
            try {
                SelectIdPedidos = cn.createStatement();
                ResultSet rs = SelectIdPedidos.executeQuery(sSQL);
                if (rs.next()) {
                    String pedido = String.valueOf(rs.getInt("idPedidos") + 1);
                    txtnumero.setText(c.completarString(pedido, 8));
                } else {
                    txtnumero.setText(c.completarString("1", 8));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                try {
                    if (SelectIdPedidos != null) {
                        SelectIdPedidos.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
        if ("Presupuesto".equals(tipoventa.getText())) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            Statement SelectIdPresupuestos = null;
            String sSQL = "SELECT MAX(idPresupuestos) AS idPresupuestos FROM presupuestos";
            fnCompletar c = new fnCompletar();
            try {
                SelectIdPresupuestos = cn.createStatement();
                ResultSet rs = SelectIdPresupuestos.executeQuery(sSQL);

                if (rs.next()) {
                    String pedido = String.valueOf(rs.getInt("idPresupuestos") + 1);
                    txtnumero.setText(c.completarString(pedido, 8));
                } else {
                    txtnumero.setText(c.completarString("1", 8));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, ex);
            } finally {
                try {
                    if (SelectIdPresupuestos != null) {
                        SelectIdPresupuestos.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }

        }
    }
    ////////////////////////////////////////

    ///// CARGAR CBO TarjetaCredito /////
    void cargartarjetacredito() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotarjetacredito.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTarjetaCredito = null;
        String sSQL = "SELECT * FROM tarjetacredito";
        try {
            SelectTarjetaCredito = cn.createStatement();
            ResultSet rs = SelectTarjetaCredito.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTarjetaCredito(rs.getInt("idTarjetaCredito"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaCredito != null) {
                    SelectTarjetaCredito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ////////////////////////////////////////
    ///// CARGAR CBO CtaCte /////
    void cargarformactacte() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboformactacte.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectFormaPago_CtaCte = null;
        String sSQL = "SELECT * FROM formasdepago_ctacte";
        try {
            SelectFormaPago_CtaCte = cn.createStatement();
            ResultSet rs = SelectFormaPago_CtaCte.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboFormaCtaCte(rs.getInt("idFormaPago_CtaCte"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFormaPago_CtaCte != null) {
                    SelectFormaPago_CtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// CARGAR CBO Cheque /////
    void cargarformacheque() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboformacheque.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectFormaPago_Cheque = null;
        String sSQL = "SELECT * FROM formasdepago_cheque";
        try {
            SelectFormaPago_Cheque = cn.createStatement();
            ResultSet rs = SelectFormaPago_Cheque.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboFormaCheque(rs.getInt("idFormaPago_Cheque"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFormaPago_Cheque != null) {
                    SelectFormaPago_Cheque.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ////////////////////////////////////////
    ///// CARGAR CBO TarjetaDebito /////
    void cargartarjetadebito() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotarjetadebito.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTarjetaDebito = null;
        String sSQL = "SELECT * FROM tarjetadebito";
        try {
            SelectTarjetaDebito = cn.createStatement();
            ResultSet rs = SelectTarjetaDebito.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTarjetaDebito(rs.getInt("idTarjetaDebito"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaDebito != null) {
                    SelectTarjetaDebito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    void cargarformadepago() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTarjetaDebito = null;
        String sSQL = "SELECT * FROM formasdepago";
        try {
            SelectTarjetaDebito = cn.createStatement();
            ResultSet rs = SelectTarjetaDebito.executeQuery(sSQL);
            int i = 0;
            while (rs.next()) {
                System.out.println("idFormasDePago " + rs.getInt("idFormasDePago") + " nombre " + rs.getString("nombre") + " descuento " + rs.getDouble("descuento") + " signo " + rs.getString("signo"));
                forma[i] = new cboFormasdepago(rs.getInt("idFormasDePago"), rs.getString("nombre"), rs.getDouble("descuento"), rs.getString("signo"));
                i++;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaDebito != null) {
                    SelectTarjetaDebito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    void agrega_producto() {

        Cantidad.bc = 1;
        bcant = 1;
        int bandera = 0;
        DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
        if (tablabuscar.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
        } else {
            double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());

            if (bcant != 0) {
                double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                double cant = Double.valueOf(bcant);
                double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                fnRedondear redondear = new fnRedondear();
                Object nuevo[] = {
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                    tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                    bcant,
                    Double.valueOf(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant))};
                /////Sacao el codigo de la tabla buscar
                String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                /////Recorro la tabla pedidos
                int filaspedidos = tablapedidos.getRowCount();
                int i = 0;
                if (filaspedidos != 0) {
                    while (filaspedidos > i) {
                        /////Sacao el codigo tabla pedido
                        String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                        if (codigobuscar.equals(codigopedido)) {
                            bandera = 1;
                            break;
                        }
                        i++;
                    }
                    if (bandera == 0) {
                        temp.addRow(nuevo);
                        /////Resto la cantidad al stock de la tabla buscar
                        System.out.println("Formularios.Main.agrega_producto()");
                        tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                    } else {
                        /////Recooro la tabla pedidos
                        i = 0;
                        while (filaspedidos > i) {

                            String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                            /////Sumo cantidad en la tabla pedidos si el cod son iguales
                            if (codigobuscar.equals(codigopedido2)) {
                                double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                System.out.println("Formularios.Main.agrega_producto()");
                                tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                break;
                            }
                            i++;
                        }
                    }
                } else {
                    temp.addRow(nuevo);
                    System.out.println("Formularios.Main.agrega_producto()");
                    /////Resto la cantidad al stock de la tabla buscar
                    tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                }
                cargatotales();
                /////////////////////////////////////////////////////////////
                tamañotablapedido();
                /////////////////////////////////////////////////////////////
                txtbuscar.setText("");
                txtbuscar.requestFocus();

            } else {
                JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
            }

        }
    }

    ///// CARGAR TABLA PEDIDO /////
    void dobleclickagregaproducto() {
        tablabuscar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Cantidad.bc = 0;
                    new Cantidad(null, true).setVisible(true);
                    int bandera = 0;
                    DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
                    if (tablabuscar.getSelectedRow() == -1) {
                        JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
                    } else {
                        double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());

                        if (bcant != 0) {
                            double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                            double cant = Double.valueOf(bcant);
                            double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                            double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                            fnRedondear redondear = new fnRedondear();
                            Object nuevo[] = {
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                                tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                                bcant,
                                Double.valueOf(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant))};
                            /////Sacao el codigo de la tabla buscar
                            String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                            /////Recorro la tabla pedidos
                            int filaspedidos = tablapedidos.getRowCount();
                            int i = 0;
                            if (filaspedidos != 0) {
                                while (filaspedidos > i) {
                                    /////Sacao el codigo tabla pedido
                                    String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                    if (codigobuscar.equals(codigopedido)) {
                                        bandera = 1;
                                        break;
                                    }
                                    i++;
                                }
                                if (bandera == 0) {
                                    temp.addRow(nuevo);
                                    System.out.println(".mouseClicked()");
                                    /////Resto la cantidad al stock de la tabla buscar
                                    tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                } else {
                                    /////Recooro la tabla pedidos
                                    i = 0;
                                    while (filaspedidos > i) {

                                        String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                        /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                        if (codigobuscar.equals(codigopedido2)) {
                                            double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                            precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                            bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                            iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                            tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                            tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                            System.out.println(".mouseClicked()");
                                            tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                            break;
                                        }
                                        i++;
                                    }
                                }
                            } else {
                                temp.addRow(nuevo);
                                /////Resto la cantidad al stock de la tabla buscar
                                System.out.println(".mouseClicked()");
                                tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                            }
                            cargatotales();
                            /////////////////////////////////////////////////////////////
                            tamañotablapedido();
                            /////////////////////////////////////////////////////////////
                            txtbuscar.setText("");
                            txtbuscar.requestFocus();

                        } else {
                            JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
                        }

                    }
                }
            }
        }
        );
    }
    ////////////////////////////////////////

    ///// CARGA TOTALES /////
    void cargatotales() {
        //Suma los iportes del pedido 
        double total = 0.00, sumatoria = 0.0;
        int totalRow = tablapedidos.getRowCount();
        totalRow -= 1;
        descuentoporcentaje[0] = 0;
        interesporcentaje[0] = 0;
        descuentobase[0] = 0;
        interesbase[0] = 0;
        fnRedondear redondear = new fnRedondear();
        String signo = "";

        signo = forma[indexformapago].getSignoFormasdepago();
        //System.out.println("signo " + signo + " indexformapago " + indexformapago);

        //Ninguna de los anteriores
        if (indexformapago == 0) {
            // System.out.println("indexformapago == 0");
            descuentoporcentaje[0] = forma[indexformapago].getdescuentoFormasdepago();
            //System.out.println("descuento " + descuentoporcentaje[0]);

            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            formas_detalle[0] = 0;
            idTipoDePago = 0;

        }

        //Forma CtaCte
        if (indexformapago == 1) {
            //System.out.println("indexformapago == 1");
            if (signo.equals("NEGATIVO")) {

                cboFormaCtaCte formactacte = (cboFormaCtaCte) cboformactacte.getSelectedItem();
                formas_detalle[0] = formactacte.getidFormaPago_CtaCte();
                idTipoDePago = cboformactacte.getSelectedIndex();
                descuentoporcentaje[0] = formactacte.getdescuentoFormaPago_CtaCte();
                //System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboFormaCtaCte formactacte = (cboFormaCtaCte) cboformactacte.getSelectedItem();
                formas_detalle[0] = formactacte.getidFormaPago_CtaCte();
                idTipoDePago = cboformactacte.getSelectedIndex();
                // System.out.println("fornas_detalle[0] " + fornas_detalle[0]);
                interesporcentaje[0] = formactacte.getdescuentoFormaPago_CtaCte();
                // System.out.println("interesporcentaje[0] " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }

        }

        //Tarjeta Credito
        if (indexformapago == 2) {
            //System.out.println("indexformapago == 2");
            if (signo.equals("NEGATIVO")) {
                cboTarjetaCredito tarjetacredito = (cboTarjetaCredito) cbotarjetacredito.getSelectedItem();
                formas_detalle[0] = tarjetacredito.getidTarjetaCredito();
                idTipoDePago = cbotarjetacredito.getSelectedIndex();
                descuentoporcentaje[0] = tarjetacredito.getdescuentoTarjetaCredito();
                //System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboTarjetaCredito tarjetacredito = (cboTarjetaCredito) cbotarjetacredito.getSelectedItem();
                formas_detalle[0] = tarjetacredito.getidTarjetaCredito();
                idTipoDePago = cbotarjetacredito.getSelectedIndex();
                //System.out.println("fornas_detalle[0] " + fornas_detalle[0]);
                interesporcentaje[0] = tarjetacredito.getdescuentoTarjetaCredito();
                //System.out.println("interesporcentaje[0] " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }
        }

        //Tarjeta de Debito
        if (indexformapago == 3) {
            //System.out.println("indexformapago == 3");
            if (signo.equals("NEGATIVO")) {
                cboTarjetaDebito tarjetadebito = (cboTarjetaDebito) cbotarjetadebito.getSelectedItem();
                formas_detalle[0] = tarjetadebito.getidTarjetaDebito();
                idTipoDePago = cbotarjetadebito.getSelectedIndex();
                descuentoporcentaje[0] = tarjetadebito.getdescuentoTarjetaDebito();
                // System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboTarjetaDebito tarjetadebito = (cboTarjetaDebito) cbotarjetadebito.getSelectedItem();
                formas_detalle[0] = tarjetadebito.getidTarjetaDebito();
                idTipoDePago = cbotarjetadebito.getSelectedIndex();
                interesporcentaje[0] = tarjetadebito.getdescuentoTarjetaDebito();
                //System.out.println("interesporcentaje " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }

        }

        //Cheque
        if (indexformapago == 4) {
            // System.out.println("indexformapago == 4");
            if (signo.equals("NEGATIVO")) {
                cboFormaCheque cheque = (cboFormaCheque) cboformacheque.getSelectedItem();
                formas_detalle[0] = cheque.getidFormaPago_Cheque();
                idTipoDePago = cboformacheque.getSelectedIndex();
                descuentoporcentaje[0] = cheque.getdescuentoFormaPago_Cheque();
                //  System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboFormaCheque cheque = (cboFormaCheque) cboformacheque.getSelectedItem();
                formas_detalle[0] = cheque.getidFormaPago_Cheque();
                idTipoDePago = cboformacheque.getSelectedIndex();
                interesporcentaje[0] = cheque.getdescuentoFormaPago_Cheque();
                //  System.out.println("interesporcentaje " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }
        }

        //interes manual
        if (indexformapago == 5) {
            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            //descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            descuentobase[0] = Double.valueOf(txtdescuento.getText());
            formas_detalle[0] = 0;
            idTipoDePago = 0;

        }
        if (indexformapago == 6) {
            interesporcentaje[0] = forma[indexformapago].getdescuentoFormasdepago();

            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            formas_detalle[0] = 0;
            idTipoDePago = 0;

        }
        //Recorrer para el descuento
        for (int i = 0; i <= (totalRow); i++) {
            double precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
            double cant = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
            double bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
            double iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
            double desc = descuentoporcentaje[0];
            double interes = interesporcentaje[0];
            tablapedidos.setValueAt(desc, i, 8);
            tablapedidos.setValueAt(interes, i, 10);
            tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * (1 + interes / 100) * cant)), i, 9);
            //tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * cant)), i, 9);

        }

        for (int i = 0; i <= (totalRow); i++) {
            sumatoria = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 9).toString()));
            total = total + sumatoria;
        }

        txtsubtotal.setText(String.valueOf(redondear.dosDigitos(total)));
        txtdescuentoPorcentaje.setText(String.valueOf(descuentoporcentaje[0]));
        txtinteresPorcentaje.setText(String.valueOf(interesporcentaje[0]));
        txtinteres.setText(String.valueOf(interesbase[0]));
        txtdescuento.setText(String.valueOf(descuentobase[0]));
        txttotal.setText(String.valueOf(redondear.dosDigitos(total - descuentobase[0])));
    }
    ////////////////////////////////////////

    ///// FUNCION AJUSTE TABLA PEDIDOS /////
    void tamañotablapedido() {
        tablapedidos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(6).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(70);
        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(170);
        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(40);

        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(100);
        tablapedidos.getColumnModel().getColumn(9).setCellRenderer(new fnEditarCeldas());

        tablapedidos.getColumnModel().getColumn(8).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(8).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
    }
    ////////////////////////////////////////

    ///// FUNCION BORRAR TABLA /////
    void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }
    ////////////////////////////////////////

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        btnaceptar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        tipoventa = new javax.swing.JLabel();
        btncliente = new javax.swing.JButton();
        txtnombreCliente = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtnumero = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        txtfecha = new javax.swing.JTextField();
        txtresposabilidadCliente = new javax.swing.JTextField();
        txtdocCliente = new javax.swing.JTextField();
        txttipodocCliente = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablapedidos = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        cbotarjetacredito = new javax.swing.JComboBox<>();
        cbotarjetadebito = new javax.swing.JComboBox<>();
        cboformactacte = new javax.swing.JComboBox<>();
        cboformacheque = new javax.swing.JComboBox<>();
        radioContado = new javax.swing.JRadioButton();
        radioCuentaCorriente = new javax.swing.JRadioButton();
        radioCredito = new javax.swing.JRadioButton();
        radioDebito = new javax.swing.JRadioButton();
        radioCheque = new javax.swing.JRadioButton();
        radioOtros = new javax.swing.JRadioButton();
        txttotal = new javax.swing.JTextField();
        txtinteres = new javax.swing.JTextField();
        txtdescuento = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtsubtotal = new javax.swing.JTextField();
        txtinteresPorcentaje = new javax.swing.JTextField();
        txtdescuentoPorcentaje = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablabuscar = new javax.swing.JTable();
        btnsalir = new javax.swing.JButton();
        txtbuscar = new javax.swing.JTextField();
        cbodeposito = new javax.swing.JComboBox();
        buscar = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuDatos = new javax.swing.JMenu();
        mncategoria = new javax.swing.JMenuItem();
        mnmarcas = new javax.swing.JMenuItem();
        mnproveedor = new javax.swing.JMenuItem();
        mnproducto = new javax.swing.JMenuItem();
        mnstock = new javax.swing.JMenuItem();
        mnclientes = new javax.swing.JMenuItem();
        mnivacompra = new javax.swing.JMenuItem();
        mnegresos = new javax.swing.JMenuItem();
        mnpuntodeventa = new javax.swing.JMenuItem();
        mnusuarios = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuVentas = new javax.swing.JMenu();
        mnpedidos = new javax.swing.JMenuItem();
        mnpresupuestos = new javax.swing.JMenuItem();
        mnbonificacion = new javax.swing.JMenuItem();
        mnformasdepago = new javax.swing.JMenuItem();
        mnctacte = new javax.swing.JMenuItem();
        mncaja = new javax.swing.JMenuItem();
        mnfacturacion = new javax.swing.JMenuItem();
        jMenuConsultas = new javax.swing.JMenu();
        RankingClientes = new javax.swing.JMenuItem();
        RankingProductos = new javax.swing.JMenuItem();
        OrdenesdeCompra = new javax.swing.JMenuItem();
        ProveedoresyProductos = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuAfip = new javax.swing.JMenu();
        mntipofacturacion = new javax.swing.JMenuItem();
        jMenuWebServices = new javax.swing.JMenu();
        jMenuFECAEAConsultar = new javax.swing.JMenuItem();
        jMenuFECompUltimoAutorizado = new javax.swing.JMenuItem();
        jMenuFEDummy = new javax.swing.JMenuItem();
        jMenuFEParamGetCotizacion = new javax.swing.JMenuItem();
        jMenuFEParamGetPtosVenta = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposCbte = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposConcepto = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposDoc = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposIva = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposMonedas = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposOpcional = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposPaises = new javax.swing.JMenuItem();
        jMenuFEParamGetTiposTributos = new javax.swing.JMenuItem();
        jMenuHasar2G1 = new javax.swing.JMenu();
        mniniciar = new javax.swing.JMenuItem();
        mnparar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pedidos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnaceptar.setMnemonic('e');
        btnaceptar.setText("Aceptar");
        btnaceptar.setToolTipText("[v]");
        btnaceptar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setMnemonic('l');
        btncancelar.setText("Cancelar");
        btncancelar.setToolTipText("[b]");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        tipoventa.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        tipoventa.setForeground(new java.awt.Color(255, 0, 51));
        tipoventa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tipoventa.setText("Pedido");
        tipoventa.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tipoventa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tipoventaMouseClicked(evt);
            }
        });

        btncliente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Login.png"))); // NOI18N
        btncliente.setText("Cliente");
        btncliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclienteActionPerformed(evt);
            }
        });

        txtnombreCliente.setEditable(false);
        txtnombreCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombreCliente.setOpaque(false);
        txtnombreCliente.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("N°:");

        txtnumero.setEditable(false);
        try {
            txtnumero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtnumero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtnumero.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Fecha:");

        txtfecha.setEditable(false);
        txtfecha.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtfecha.setOpaque(false);

        txtresposabilidadCliente.setEditable(false);
        txtresposabilidadCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtresposabilidadCliente.setOpaque(false);
        txtresposabilidadCliente.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        txtdocCliente.setEditable(false);
        txtdocCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdocCliente.setOpaque(false);
        txtdocCliente.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        txttipodocCliente.setEditable(false);
        txttipodocCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttipodocCliente.setOpaque(false);
        txttipodocCliente.setSelectedTextColor(new java.awt.Color(240, 240, 240));

        tablapedidos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tablapedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdProducto", "Codigo", "Nombre", "PVenta", "Bon%", "idIVA", "IVA%", "Cantidad", "Desc%", "Total", "Interes"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablapedidos.setToolTipText("tabla de ventas");
        tablapedidos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tablapedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablapedidosMouseClicked(evt);
            }
        });
        tablapedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapedidosKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablapedidos);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Formas de Pago", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        cbotarjetacredito.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbotarjetacredito.setForeground(new java.awt.Color(0, 102, 204));
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotarjetacreditoActionPerformed(evt);
            }
        });

        cbotarjetadebito.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbotarjetadebito.setForeground(new java.awt.Color(0, 102, 204));
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotarjetadebitoActionPerformed(evt);
            }
        });

        cboformactacte.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboformactacte.setForeground(new java.awt.Color(0, 102, 204));
        cboformactacte.setEnabled(false);
        cboformactacte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboformactacteActionPerformed(evt);
            }
        });

        cboformacheque.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboformacheque.setForeground(new java.awt.Color(0, 102, 204));
        cboformacheque.setEnabled(false);
        cboformacheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboformachequeActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioContado);
        radioContado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioContado.setSelected(true);
        radioContado.setText("Contado");
        radioContado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioContadoActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioCuentaCorriente);
        radioCuentaCorriente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioCuentaCorriente.setText("Cuenta corriente");
        radioCuentaCorriente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCuentaCorrienteActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioCredito);
        radioCredito.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioCredito.setText("Tarjeta de crédito");
        radioCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCreditoActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioDebito);
        radioDebito.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioDebito.setText("Tarjeta de débito");
        radioDebito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioDebitoActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioCheque);
        radioCheque.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioCheque.setText("Cheque");
        radioCheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioChequeActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioOtros);
        radioOtros.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        radioOtros.setText("Otros");
        radioOtros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioOtrosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboformactacte, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbotarjetacredito, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbotarjetadebito, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboformacheque, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioContado)
                            .addComponent(radioCuentaCorriente)
                            .addComponent(radioCredito)
                            .addComponent(radioDebito)
                            .addComponent(radioCheque)
                            .addComponent(radioOtros))
                        .addGap(0, 80, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(radioContado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioCuentaCorriente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboformactacte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioCredito)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbotarjetacredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioDebito)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbotarjetadebito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioCheque)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboformacheque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(radioOtros)
                .addGap(33, 33, 33))
        );

        txttotal.setEditable(false);
        txttotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txttotal.setForeground(new java.awt.Color(0, 102, 204));
        txttotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txttotal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txttotal.setOpaque(false);

        txtinteres.setEditable(false);
        txtinteres.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtinteres.setForeground(new java.awt.Color(255, 102, 0));
        txtinteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtinteres.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Interes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txtinteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtinteresActionPerformed(evt);
            }
        });

        txtdescuento.setEditable(false);
        txtdescuento.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtdescuento.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtdescuento.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Descuento", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txtdescuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescuentoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("%");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 102, 0));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("%");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 204));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("$");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("$");

        txtsubtotal.setEditable(false);
        txtsubtotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtsubtotal.setForeground(new java.awt.Color(0, 153, 153));
        txtsubtotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtsubtotal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SubTotal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txtsubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsubtotalActionPerformed(evt);
            }
        });

        txtinteresPorcentaje.setEditable(false);
        txtinteresPorcentaje.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtinteresPorcentaje.setForeground(new java.awt.Color(255, 102, 0));
        txtinteresPorcentaje.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtinteresPorcentaje.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txtinteresPorcentaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtinteresPorcentajeActionPerformed(evt);
            }
        });

        txtdescuentoPorcentaje.setEditable(false);
        txtdescuentoPorcentaje.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtdescuentoPorcentaje.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtdescuentoPorcentaje.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        txtdescuentoPorcentaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescuentoPorcentajeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(btncliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtresposabilidadCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttipodocCliente)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtnombreCliente))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtinteresPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtdescuentoPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtinteres, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtdescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tipoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 5, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, jLabel3, jLabel6, jLabel7});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtdescuento, txtinteres, txtsubtotal, txttotal});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtdescuentoPorcentaje, txtinteresPorcentaje});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtnombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtresposabilidadCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btncliente, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttipodocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtsubtotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(60, 60, 60)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtinteres, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                                        .addComponent(txtinteresPorcentaje)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btncancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnaceptar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tipoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtdescuentoPorcentaje))
                                    .addComponent(txtdescuento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel6, txtinteres, txtinteresPorcentaje});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel2, txtdescuento, txtdescuentoPorcentaje});

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablabuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tablabuscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablabuscar.setToolTipText("Buscar Productos");
        tablabuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablabuscarKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablabuscar);

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setToolTipText("Buscar Producto");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        cbodeposito.setToolTipText("Filtrar por");
        cbodeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbodepositoActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setText("Usuario:");
        jLabel4.setEnabled(false);

        txtusuario.setEditable(false);
        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtusuario.setBorder(null);
        txtusuario.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(buscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cbodeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtusuario)
                                .addGap(18, 18, 18)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 10, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(buscar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbodeposito, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.Alignment.LEADING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnsalir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtusuario, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jMenuDatos.setMnemonic('D');
        jMenuDatos.setText("  Datos ");

        mncategoria.setText("Categorias ");
        mncategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mncategoriaActionPerformed(evt);
            }
        });
        jMenuDatos.add(mncategoria);

        mnmarcas.setText("Marcas");
        mnmarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnmarcasActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnmarcas);

        mnproveedor.setText("Proveedores ");
        mnproveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnproveedorActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnproveedor);

        mnproducto.setText("Productos ");
        mnproducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnproductoActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnproducto);

        mnstock.setText("Stock");
        mnstock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnstockActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnstock);

        mnclientes.setText("Clientes");
        mnclientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnclientesActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnclientes);

        mnivacompra.setText("Iva Compra");
        mnivacompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnivacompraActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnivacompra);

        mnegresos.setText("Egresos");
        mnegresos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnegresosActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnegresos);

        mnpuntodeventa.setText("Puntos de Ventas");
        mnpuntodeventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnpuntodeventaActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnpuntodeventa);

        mnusuarios.setText("Usuarios");
        mnusuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnusuariosActionPerformed(evt);
            }
        });
        jMenuDatos.add(mnusuarios);

        jMenuItem1.setText("Actualizar Precios");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenuDatos.add(jMenuItem1);

        jMenuBar1.add(jMenuDatos);

        jMenuVentas.setMnemonic('D');
        jMenuVentas.setText("Ventas");

        mnpedidos.setText("Pedidos");
        mnpedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnpedidosActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnpedidos);

        mnpresupuestos.setText("Presupuestos");
        mnpresupuestos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnpresupuestosActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnpresupuestos);

        mnbonificacion.setText("Bonificaciones");
        mnbonificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnbonificacionActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnbonificacion);

        mnformasdepago.setText("Formas de Pago");
        mnformasdepago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnformasdepagoActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnformasdepago);

        mnctacte.setText("Cuentas Corrientes");
        mnctacte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnctacteActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnctacte);

        mncaja.setText("Caja");
        mncaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mncajaActionPerformed(evt);
            }
        });
        jMenuVentas.add(mncaja);

        mnfacturacion.setText("Facturacion");
        mnfacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnfacturacionActionPerformed(evt);
            }
        });
        jMenuVentas.add(mnfacturacion);

        jMenuBar1.add(jMenuVentas);

        jMenuConsultas.setText("Consultas");

        RankingClientes.setText("Ranking Clientes");
        RankingClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RankingClientesActionPerformed(evt);
            }
        });
        jMenuConsultas.add(RankingClientes);

        RankingProductos.setText("Ranking Productos");
        RankingProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RankingProductosActionPerformed(evt);
            }
        });
        jMenuConsultas.add(RankingProductos);

        OrdenesdeCompra.setText("Ordenes de Compra");
        OrdenesdeCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrdenesdeCompraActionPerformed(evt);
            }
        });
        jMenuConsultas.add(OrdenesdeCompra);

        ProveedoresyProductos.setText("Proveedores y Productos");
        ProveedoresyProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProveedoresyProductosActionPerformed(evt);
            }
        });
        jMenuConsultas.add(ProveedoresyProductos);

        jMenuItem3.setText("Ingresos y Egresos");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenuConsultas.add(jMenuItem3);

        jMenuBar1.add(jMenuConsultas);

        jMenuAfip.setMnemonic('D');
        jMenuAfip.setText("Afip");
        jMenuAfip.setToolTipText("");

        mntipofacturacion.setText("Configurar Facturacion");
        mntipofacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mntipofacturacionActionPerformed(evt);
            }
        });
        jMenuAfip.add(mntipofacturacion);

        jMenuWebServices.setText("Web Services");

        jMenuFECAEAConsultar.setText("FECAESolicitar");
        jMenuFECAEAConsultar.setToolTipText("Consultar CAEA emitidos.");
        jMenuFECAEAConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFECAEAConsultarActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFECAEAConsultar);

        jMenuFECompUltimoAutorizado.setText("FECompUltimoAutorizado");
        jMenuFECompUltimoAutorizado.setToolTipText("Retorna el ultimo comprobante autorizado para el tipo de comprobante / cuit / punto de venta ingresado / Tipo de Emisión");
        jMenuFECompUltimoAutorizado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFECompUltimoAutorizadoActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFECompUltimoAutorizado);

        jMenuFEDummy.setText("FEDummy");
        jMenuFEDummy.setToolTipText("Metodo dummy para verificacion de funcionamiento");
        jMenuFEDummy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEDummyActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEDummy);

        jMenuFEParamGetCotizacion.setText("FEParamGetCotizacion");
        jMenuFEParamGetCotizacion.setToolTipText("Recupera la cotizacion de la moneda consultada y su fecha");
        jMenuFEParamGetCotizacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetCotizacionActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetCotizacion);

        jMenuFEParamGetPtosVenta.setText("FEPtosVenta");
        jMenuFEParamGetPtosVenta.setToolTipText("Recupera el listado de puntos de venta registrados y su estado");
        jMenuFEParamGetPtosVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetPtosVentaActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetPtosVenta);

        jMenuFEParamGetTiposCbte.setText("FETiposCbte");
        jMenuFEParamGetTiposCbte.setToolTipText("Recupera el listado de Tipos de Comprobantes utilizables en servicio de autorización.");
        jMenuFEParamGetTiposCbte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposCbteActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposCbte);

        jMenuFEParamGetTiposConcepto.setText("FEParamGetTiposConcepto ");
        jMenuFEParamGetTiposConcepto.setToolTipText("Recupera el listado de identificadores para el campo Concepto.");
        jMenuFEParamGetTiposConcepto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposConceptoActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposConcepto);

        jMenuFEParamGetTiposDoc.setText("FEParamGetTiposDoc ");
        jMenuFEParamGetTiposDoc.setToolTipText("Recupera el listado de Tipos de Documentos utilizables en servicio de autorización.");
        jMenuFEParamGetTiposDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposDocActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposDoc);

        jMenuFEParamGetTiposIva.setText("FEParamGetTiposIva");
        jMenuFEParamGetTiposIva.setToolTipText("Recupera el listado de Tipos de Iva utilizables en servicio de autorización.");
        jMenuFEParamGetTiposIva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposIvaActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposIva);

        jMenuFEParamGetTiposMonedas.setText("FEParamGetTiposMonedas");
        jMenuFEParamGetTiposMonedas.setToolTipText("Recupera el listado de monedas utilizables en servicio de autorización");
        jMenuFEParamGetTiposMonedas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposMonedasActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposMonedas);

        jMenuFEParamGetTiposOpcional.setText("FEParamGetTiposOpcional");
        jMenuFEParamGetTiposOpcional.setToolTipText("Recupera el listado de identificadores para los campos Opcionales");
        jMenuFEParamGetTiposOpcional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposOpcionalActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposOpcional);

        jMenuFEParamGetTiposPaises.setText("FEParamGetTiposPaises");
        jMenuFEParamGetTiposPaises.setToolTipText("Recupera el listado de los diferente paises que pueden ser utilizados en el servicio de autorizacion");
        jMenuFEParamGetTiposPaises.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposPaisesActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposPaises);

        jMenuFEParamGetTiposTributos.setText("FEParamGetTiposTributos");
        jMenuFEParamGetTiposTributos.setToolTipText("Recupera el listado de los diferente tributos que pueden ser utilizados en el servicio de autorizacion");
        jMenuFEParamGetTiposTributos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFEParamGetTiposTributosActionPerformed(evt);
            }
        });
        jMenuWebServices.add(jMenuFEParamGetTiposTributos);

        jMenuAfip.add(jMenuWebServices);

        jMenuHasar2G1.setText("Auto");

        mniniciar.setText("Iniciar");
        mniniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniniciarActionPerformed(evt);
            }
        });
        jMenuHasar2G1.add(mniniciar);

        mnparar.setText("Parar");
        mnparar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnpararActionPerformed(evt);
            }
        });
        jMenuHasar2G1.add(mnparar);

        jMenuAfip.add(jMenuHasar2G1);

        jMenuBar1.add(jMenuAfip);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnclientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnclientesActionPerformed
        new Clientes(this, true).setVisible(true);
    }//GEN-LAST:event_mnclientesActionPerformed

    private void mnstockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnstockActionPerformed
        new Stock(this, true).setVisible(true);
        cargartablabuscar("");
    }//GEN-LAST:event_mnstockActionPerformed

    private void mnproductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnproductoActionPerformed
        new Productos(this, true).setVisible(true);
        cargartablabuscar("");
    }//GEN-LAST:event_mnproductoActionPerformed

    private void mnproveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnproveedorActionPerformed
        new Proveedores(this, true).setVisible(true);
    }//GEN-LAST:event_mnproveedorActionPerformed

    private void mncategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mncategoriaActionPerformed
        new Categorias(this, true).setVisible(true);
    }//GEN-LAST:event_mncategoriaActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        //No se Puede Hacer un Pedido si la Caja esta Cerrada
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        if (idCaja == 0) {
            JOptionPane.showMessageDialog(null, "Debe Realizar la Apertura de Caja");
        } else {
            if (txttotal.getText().equals("") || txtsubtotal.getText().equals("")
                    || txtnombreCliente.getText().equals("") || tablapedidos.getRowCount() == 0) {
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos obligatorios...");
            } else {
                /////   Carga Variables
                fnCargarFecha f = new fnCargarFecha();

                total = txttotal.getText();
                String[][] datos = new String[tablapedidos.getRowCount()][11];
                for (int i = 0; i < tablapedidos.getRowCount(); i++) {
                    datos[i][0] = tablapedidos.getValueAt(i, 0).toString();
                    datos[i][1] = tablapedidos.getValueAt(i, 1).toString();
                    datos[i][2] = tablapedidos.getValueAt(i, 2).toString();
                    datos[i][3] = tablapedidos.getValueAt(i, 3).toString();
                    datos[i][4] = tablapedidos.getValueAt(i, 4).toString();
                    datos[i][5] = tablapedidos.getValueAt(i, 5).toString();
                    datos[i][6] = tablapedidos.getValueAt(i, 6).toString();
                    datos[i][7] = tablapedidos.getValueAt(i, 7).toString();
                    datos[i][8] = tablapedidos.getValueAt(i, 8).toString();
                    datos[i][9] = tablapedidos.getValueAt(i, 9).toString();
                    datos[i][10] = tablapedidos.getValueAt(i, 10).toString();
                }

                /////   Pregunto Si es una Pedido o un Presupuesto o un Modificar Pedido
                if ("Pedido".equals(tipoventa.getText())) {
                    new Descripcion_Pedido(this, true).setVisible(true);
                    new MainImprimir(this, true).setVisible(true);

                    //Pregunto si no Cancelo el Pedido
                    if (MainImprimir.cancelar == 1) {
                        //////////////////////////////////
                        //Pregunto si el es CTA CTE (id=2)
                        if (forma[indexformapago].getidFormasdepago() == 2) {
                            ClasePedidos pedido = new ClasePedidos();
                            ClaseCuentaCorriente cc = new ClaseCuentaCorriente();
                            int Idctacte = pedido.ComprueboCatCte(IdCliente, Double.valueOf(txttotal.getText()));
                            //Si la cta cte esta OK
                            if (Idctacte != 0) {
                                //Si Facturar 
                                if (MainImprimir.facturar == 1) {
                                    ///Deberia elegir pto de venta Pto de Venta=1 por defecto
                                    int Idpedidos = 0;
                                    if (ModificarPedido == 0) {
                                        Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                        pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                        idPtodeVenta = 1;
                                        pedido.FacturarPedido(IdCliente, Idpedidos, idPtodeVenta, id_usuario);
                                    } else {
                                         JOptionPane.showMessageDialog(null, "No puede modificar un pedido Facturado");
                                    }

                                } else {
                                    //Si MainImprimir 
                                    if (MainImprimir.imprimir == 1) {
                                        if (ModificarPedido == 0) {
                                            int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                            pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                            pedido.ImprimirPedido(Idpedidos);
                                        } else {
                                            pedido.ModificarPedido(idpedido, datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);
                                            cc.actualizacionDetalleCuentaCorriente(Idctacte, idpedido, Double.valueOf(txttotal.getText()));
                                            pedido.ImprimirPedido(idpedido);
                                        }
                                    }
                                    //No imprimir no Facturar
                                    if (MainImprimir.imprimir == 0) {
                                        if (ModificarPedido == 0) {
                                            int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                            pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                        } else {
                                            pedido.ModificarPedido(idpedido, datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);
                                            cc.actualizacionDetalleCuentaCorriente(Idctacte, idpedido, Double.valueOf(txttotal.getText()));
                                        }
                                    }
                                }
                                borrartabla();
                                cargarnumero();
                                txtfecha.setText(f.cargarfecha());
                                cboformactacte.setEnabled(false);
                                cbotarjetacredito.setEnabled(false);
                                cbotarjetacredito.setSelectedIndex(0);
                                cbotarjetadebito.setEnabled(false);
                                cbotarjetadebito.setSelectedIndex(0);
                                cboformacheque.setEnabled(false);
                                cboformacheque.setSelectedIndex(0);
                                txtdescuento.setEditable(false);
                                radioContado.setSelected(true);
                                txtinteres.setText("0.0");
                                txtdescuento.setText("0.0");
                                txtinteresPorcentaje.setText("0.0");
                                txtdescuentoPorcentaje.setText("0.0");
                                indexformapago = 0;
                                ConvertirPresupuesto = 0;
                                AgregarPresupuesto=0;
                                ModificarPedido=0;
                                descripcion = "";
                                idpedido=0;
                                cargatotales();
                                cargartablabuscar("");
                                cargarconsumidorfinal();
                                btnaceptar.setText("Aceptar");
                                btncancelar.transferFocus();
                            }
                            ///////////////////////////////////////
                            //No es CTA CTE
                        } else {

                            /*
                            if (ModificarPedido == 0) {
                                            int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);                                            
                                        } else {
                                            pedido.ModificarPedido(idpedido, datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);                                            
                                        }
                             */
                            //Si Facturar 
                            if (MainImprimir.facturar == 1) {
                                ///Deberia elegir pto de venta Pto de Venta=1 por defecto
                                ClasePedidos pedido = new ClasePedidos();
                                if (ModificarPedido == 0) {
                                    int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                    idPtodeVenta = 1;
                                    pedido.FacturarPedido(IdCliente, Idpedidos, idPtodeVenta, id_usuario);
                                } else {
                                    JOptionPane.showMessageDialog(null, "No puede modificar un pedido Facturado");
                                }
                            } else {
                                //Si MainImprimir 
                                if (MainImprimir.imprimir == 1) {
                                    ClasePedidos pedido = new ClasePedidos();
                                    if (ModificarPedido == 0) {
                                        int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                        pedido.ImprimirPedido(Idpedidos);
                                    } else {
                                        pedido.ModificarPedido(idpedido, datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);
                                        pedido.ImprimirPedido(idpedido);
                                    }
                                }
                                //No imprimir no Facturar
                                if (MainImprimir.imprimir == 0) {
                                    ClasePedidos pedido = new ClasePedidos();
                                    if (ModificarPedido == 0) {
                                        pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                                    } else {
                                        pedido.ModificarPedido(idpedido, datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);
                                    }
                                }
                            }

                            System.out.println("borrar tabla_pedido");
                            borrartabla();
                            cargarnumero();
                            txtfecha.setText(f.cargarfecha());
                            cboformactacte.setEnabled(false);
                            cbotarjetacredito.setEnabled(false);
                            cbotarjetacredito.setSelectedIndex(0);
                            cbotarjetadebito.setEnabled(false);
                            cbotarjetadebito.setSelectedIndex(0);
                            cboformacheque.setEnabled(false);
                            cboformacheque.setSelectedIndex(0);
                            txtdescuento.setEditable(false);
                            radioContado.setSelected(true);
                            txtinteres.setText("0.0");
                            txtdescuento.setText("0.0");
                            txtinteresPorcentaje.setText("0.0");
                            txtdescuentoPorcentaje.setText("0.0");
                            indexformapago = 0;
                            AgregarPresupuesto=0;
                            idpedido=0;
                            ModificarPedido=0;
                            ConvertirPresupuesto = 0;
                            descripcion = "";
                            cargatotales();
                            System.out.println("carga tabla buscar");
                            cargartablabuscar("");
                            cargarconsumidorfinal();
                            btnaceptar.setText("Aceptar");
                            btncancelar.transferFocus();
                            

                        }
                    }
                }
                if ("Presupuesto".equals(tipoventa.getText())) {
                    new Descripcion_Pedido(this, true).setVisible(true);
                    ClasePresupuestos presupuesto = new ClasePresupuestos();
                    int Idpresupuesto = presupuesto.AgregarPresupuesto(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), interesbase[0], interesporcentaje[0], descripcion, id_usuario, idTipoDePago);
                    //int Idpedidos = pedido.AgregarPedido(datos, f.cargarfecha(), f.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion, id_usuario);
                    presupuesto.ImprimirPresupuesto(Idpresupuesto);
                    borrartabla();
                    cargarnumero();
                    txtfecha.setText(f.cargarfecha());
                    cboformactacte.setEnabled(false);
                    cbotarjetacredito.setEnabled(false);
                    cbotarjetacredito.setSelectedIndex(0);
                    cbotarjetadebito.setEnabled(false);
                    cbotarjetadebito.setSelectedIndex(0);
                    cboformacheque.setEnabled(false);
                    cboformacheque.setSelectedIndex(0);
                    txtdescuento.setEditable(false);
                    radioContado.setSelected(true);
                    txtinteres.setText("0.0");
                    indexformapago = 0;
                    descripcion = "";
                    idpedido=0;
                    ModificarPedido=0;
                    AgregarPresupuesto=0;
                    ConvertirPresupuesto = 0;
                    txtdescuento.setText("0.0");
                    txtinteresPorcentaje.setText("0.0");
                    txtdescuentoPorcentaje.setText("0.0");
                    cargatotales();
                    cargartablabuscar("");
                    cargarconsumidorfinal();
                    tipoventa.setText("Pedido");
                    btnaceptar.setText("Aceptar");
                    btncancelar.transferFocus();
                }
            }
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        borrartabla();
        cargarnumero();
        fnCargarFecha f = new fnCargarFecha();
        txtfecha.setText(f.cargarfecha());
        cargarformadepago();
        cboformactacte.setEnabled(false);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(false);
        radioContado.setSelected(true);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        indexformapago = 0;
        idpedido=0;
        ConvertirPresupuesto = 0;
        txtdescuentoPorcentaje.setText("0.0");
        cargatotales();
        btnaceptar.setText("Aceptar");
        cargartablabuscar("");
        cargarconsumidorfinal();
        btncancelar.transferFocus();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclienteActionPerformed
        new ClientesElegir(this, true).setVisible(true);
        txtnombreCliente.setText(nombreCliente);
        txtresposabilidadCliente.setText(ResposabilidadCliente);
        txttipodocCliente.setText(TipodocCliente);
        txtdocCliente.setText(DocCliente);
        radioCuentaCorriente.setSelected(true);
        indexformapago=1;
        cargatotales();
        cargarnumero();
    }//GEN-LAST:event_btnclienteActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            bcant = 0;

            if (tablabuscar.getRowCount() == 1) {
                tablabuscar.setRowSelectionInterval(0, 0);
                System.out.println("entra");
                agrega_producto();
            } else {
                cbodeposito.transferFocus();
                evt.consume();
                tablabuscar.setRowSelectionInterval(0, 0);
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {
            btnaceptar.doClick();
            txtbuscar.setText("");
        }
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablabuscar.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void cbodepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbodepositoActionPerformed
        cboDeposito dp = (cboDeposito) cbodeposito.getSelectedItem();
        int codigo = dp.getidDeposito();
        filtrartablabuscardep(codigo);
        IdDeposito = codigo;
        borrartabla();
        cargarnumero();
        fnCargarFecha f = new fnCargarFecha();
        txtfecha.setText(f.cargarfecha());
        cargatotales();
        txtbuscar.requestFocus();
    }//GEN-LAST:event_cbodepositoActionPerformed

    private void tablabuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Cantidad.bc = 0;
            new Cantidad(null, true).setVisible(true);
            int bandera = 0;
            DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
            if (tablabuscar.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
            } else {
                double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());
                if (bcant != 0) {
                    double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                    double cant = Double.valueOf(bcant);
                    double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                    double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                    fnRedondear redondear = new fnRedondear();
                    Object nuevo[] = {
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                        bcant,
                        Double.valueOf(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant))};
                    /////Sacao el codigo de la tabla buscar
                    String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                    /////Recorro la tabla pedidos
                    int filaspedidos = tablapedidos.getRowCount();
                    int i = 0;
                    if (filaspedidos != 0) {
                        while (filaspedidos > i) {
                            /////Sacao el codigo tabla pedido
                            String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                            if (codigobuscar.equals(codigopedido)) {
                                bandera = 1;
                                break;
                            }
                            i++;
                        }
                        if (bandera == 0) {
                            temp.addRow(nuevo);
                            System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                            /////Resto la cantidad al stock de la tabla buscar
                            tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                        } else {
                            /////Recooro la tabla pedidos
                            i = 0;
                            while (filaspedidos > i) {
                                String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                if (codigobuscar.equals(codigopedido2)) {
                                    double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                    precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                    bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                    iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                    tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                    tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                    System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                                    tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                    break;
                                }
                                i++;
                            }
                        }
                    } else {
                        temp.addRow(nuevo);
                        System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                        /////Resto la cantidad al stock de la tabla buscar
                        tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                    }
                    cargatotales();
                    /////////////////////////////////////////////////////////////
                    tamañotablapedido();
                    /////////////////////////////////////////////////////////////
                    txtbuscar.setText("");
                    txtbuscar.requestFocus();

                } else {
                    JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
                }

            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            int fila = tablapedidos.getRowCount();
            if (fila != 0) {
                bcant = 0;
                tablabuscar.transferFocus();
                evt.consume();
                tablapedidos.setRowSelectionInterval(0, 0);
                txtbuscar.setText("");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_B) {
            txtbuscar.requestFocus();
            txtbuscar.setText("");
        }
    }//GEN-LAST:event_tablabuscarKeyPressed

    private void jMenuFECAEAConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFECAEAConsultarActionPerformed
        new AfipWS_FECAESolicitar(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFECAEAConsultarActionPerformed

    private void jMenuFECompUltimoAutorizadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFECompUltimoAutorizadoActionPerformed
        // new FormulariosAfip.FECompUltimoAutorizado(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFECompUltimoAutorizadoActionPerformed

    private void jMenuFEDummyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEDummyActionPerformed
        new AfipWS_FEDummy(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEDummyActionPerformed

    private void jMenuFEParamGetCotizacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetCotizacionActionPerformed
        // new FormulariosAfip.FEParamGetCotizacion(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetCotizacionActionPerformed

    private void jMenuFEParamGetPtosVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetPtosVentaActionPerformed
        new AfipWS_FEPtoVentas(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetPtosVentaActionPerformed

    private void jMenuFEParamGetTiposCbteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposCbteActionPerformed
        new AfipWS_FETiposCbte(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposCbteActionPerformed

    private void jMenuFEParamGetTiposConceptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposConceptoActionPerformed
        // new FormulariosAfip.FEParamGetTiposConcepto(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposConceptoActionPerformed

    private void jMenuFEParamGetTiposDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposDocActionPerformed
        // new FormulariosAfip.FEParamGetTiposDoc(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposDocActionPerformed

    private void jMenuFEParamGetTiposIvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposIvaActionPerformed
        // new FormulariosAfip.FEParamGetTiposIva(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposIvaActionPerformed

    private void jMenuFEParamGetTiposMonedasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposMonedasActionPerformed
        // new FormulariosAfip.FEParamGetTiposMonedas(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposMonedasActionPerformed

    private void jMenuFEParamGetTiposOpcionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposOpcionalActionPerformed
        // new FormulariosAfip.FEParamGetTiposOpcional(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposOpcionalActionPerformed

    private void jMenuFEParamGetTiposPaisesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposPaisesActionPerformed
        // new FormulariosAfip.FEParamGetTiposPaises(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposPaisesActionPerformed

    private void jMenuFEParamGetTiposTributosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFEParamGetTiposTributosActionPerformed
        // new FormulariosAfip.FEParamGetTiposTributos(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuFEParamGetTiposTributosActionPerformed

    private void tipoventaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tipoventaMouseClicked
        if (tipoventa.getText().equals("Presupuesto")) {
            tipoventa.setText("Pedido");
            cargarnumero();
        } else {
            tipoventa.setText("Presupuesto");
            cargarnumero();
        }
    }//GEN-LAST:event_tipoventaMouseClicked

    void cargarFormaDePago(int idForma, int idTipo) {
        if (idForma == 1) {
            radioContado.setSelected(true);
            cboformactacte.setEnabled(false);
            cboformactacte.setSelectedIndex(0);
            cbotarjetacredito.setEnabled(false);
            cbotarjetacredito.setSelectedIndex(0);
            cbotarjetadebito.setEnabled(false);
            cbotarjetadebito.setSelectedIndex(0);
            cboformacheque.setEnabled(false);
            cboformacheque.setSelectedIndex(0);
            txtdescuento.setEditable(false);
            indexformapago = 0;
        }
        if (idForma == 2) {
            radioCuentaCorriente.setSelected(true);
            cboformactacte.setEnabled(true);
            cboformactacte.setSelectedIndex(idTipo);
            cbotarjetacredito.setEnabled(false);
            cbotarjetacredito.setSelectedIndex(0);
            cbotarjetadebito.setEnabled(false);
            cbotarjetadebito.setSelectedIndex(0);
            cboformacheque.setEnabled(false);
            cboformacheque.setSelectedIndex(0);
            txtdescuento.setEditable(false);
            txtinteres.setText("0.0");
            txtdescuento.setText("0.0");
            txtinteresPorcentaje.setText("0.0");
            txtdescuentoPorcentaje.setText("0.0");
            indexformapago = 1;

        }
        if (idForma == 3) {
            radioCredito.setSelected(true);
            cboformactacte.setEnabled(false);
            cboformactacte.setSelectedIndex(0);
            cbotarjetacredito.setEnabled(true);
            cbotarjetacredito.setSelectedIndex(idTipo);
            cbotarjetadebito.setEnabled(false);
            cbotarjetadebito.setSelectedIndex(0);
            cboformacheque.setEnabled(false);
            cboformacheque.setSelectedIndex(0);
            txtdescuento.setEditable(false);
            txtinteres.setText("0.0");
            txtdescuento.setText("0.0");
            txtinteresPorcentaje.setText("0.0");
            txtdescuentoPorcentaje.setText("0.0");
            indexformapago = 2;
        }
        if (idForma == 4) {
            radioDebito.setSelected(true);
            cboformactacte.setEnabled(false);
            cboformactacte.setSelectedIndex(0);
            cbotarjetacredito.setEnabled(false);
            cbotarjetacredito.setSelectedIndex(0);
            cbotarjetadebito.setEnabled(true);
            cbotarjetadebito.setSelectedIndex(idTipo);
            cboformacheque.setEnabled(false);
            cboformacheque.setSelectedIndex(0);
            txtdescuento.setEditable(false);
            txtinteres.setText("0.0");
            txtdescuento.setText("0.0");
            txtinteresPorcentaje.setText("0.0");
            txtdescuentoPorcentaje.setText("0.0");
            indexformapago = 3;
        }

        if (idForma == 5) {
            radioCheque.setSelected(true);
            cboformactacte.setEnabled(false);
            cboformactacte.setSelectedIndex(0);
            cbotarjetacredito.setEnabled(false);
            cbotarjetacredito.setSelectedIndex(0);
            cbotarjetadebito.setEnabled(false);
            cbotarjetadebito.setSelectedIndex(0);
            cboformacheque.setEnabled(true);
            cboformacheque.setSelectedIndex(idTipo);
            txtdescuento.setEditable(false);
            txtinteres.setText("0.0");
            txtdescuento.setText("0.0");
            txtinteresPorcentaje.setText("0.0");
            txtdescuentoPorcentaje.setText("0.0");
            indexformapago = 4;
        }

        if (idForma == 6) {
            radioOtros.setSelected(true);
            cboformactacte.setEnabled(false);
            cboformactacte.setSelectedIndex(0);
            cbotarjetacredito.setEnabled(false);
            cbotarjetacredito.setSelectedIndex(0);
            cbotarjetadebito.setEnabled(false);
            cbotarjetadebito.setSelectedIndex(0);
            cboformacheque.setEnabled(false);
            cboformacheque.setSelectedIndex(0);
            txtdescuento.setEditable(true);
            txtdescuentoPorcentaje.setEditable(true);
            txtinteres.setEditable(false);
            indexformapago = 5;
        }
    }

    private void mnpedidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnpedidosActionPerformed
        new Pedidos(this, true).setVisible(true);

        //Modificar pedidosfds
        if (ModificarPedido == 1) {
            borrartabla();
            ConexionMySQL cc = new ConexionMySQL();
            Connection cn = cc.Conectar();
            Statement SelectPedidoDetalle = null;
            Statement SelectPedidos = null;
            String sqlPedidodetalle = "SELECT * FROM vista_pedidos_detalle WHERE idPedidos = " + idpedido;
            model2 = (DefaultTableModel) tablapedidos.getModel();
            String sqlPedidos = "SELECT * FROM vista_pedidos WHERE idPedidos = " + idpedido;
            fnRedondear redondear = new fnRedondear();
            try {
                SelectPedidoDetalle = cn.createStatement();
                ResultSet rsPedidosDetalle = SelectPedidoDetalle.executeQuery(sqlPedidodetalle);
                while (rsPedidosDetalle.next()) {
                    Object nuevo[] = {
                        rsPedidosDetalle.getString(3),
                        rsPedidosDetalle.getString(2),
                        rsPedidosDetalle.getString(4),
                        rsPedidosDetalle.getString(9),
                        rsPedidosDetalle.getString(5),
                        rsPedidosDetalle.getString(6),
                        rsPedidosDetalle.getString(7),
                        rsPedidosDetalle.getString(8),
                        Double.valueOf(redondear.dosDigitos(redondear.dosDigitos(rsPedidosDetalle.getDouble(9) * (1 - (rsPedidosDetalle.getDouble(5) / 100)) * (1 + (rsPedidosDetalle.getDouble(7) / 100))) * rsPedidosDetalle.getDouble(8)))};
                    model2.addRow(nuevo);
                }
                tablapedidos.setModel(model2);
                tablapedidos.setAutoCreateRowSorter(true);
                tamañotablapedido();
                txtbuscar.setText("");
                txtbuscar.requestFocus();
                tipoventa.setText("Pedido");
                fnCompletar c = new fnCompletar();
                txtnumero.setText(c.completarString(String.valueOf(idpedido), 8));
                ////Cliente
                SelectPedidos = cn.createStatement();
                ResultSet rsPedido = SelectPedidos.executeQuery(sqlPedidos);
                while (rsPedido.next()) {
                    IdCliente = rsPedido.getInt(10);
                    nombreCliente = rsPedido.getString(4);
                    ResposabilidadCliente = rsPedido.getString(14);
                    TipodocCliente = rsPedido.getString(15);
                    DocCliente = rsPedido.getString(16);
                    idFormaDePago = rsPedido.getInt(17);
                    idTipoDePago = rsPedido.getInt(18);
                }
                cargarFormaDePago(idFormaDePago, idTipoDePago);
                txtnombreCliente.setText(nombreCliente);
                txtresposabilidadCliente.setText(ResposabilidadCliente);
                txttipodocCliente.setText(TipodocCliente);
                txtdocCliente.setText(DocCliente);

                /////////
                cargatotales();
                btnaceptar.setText("Modificar");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (SelectPedidos != null) {
                        SelectPedidos.close();
                    }
                    if (SelectPedidoDetalle != null) {
                        SelectPedidoDetalle.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }

    }//GEN-LAST:event_mnpedidosActionPerformed

    private void mnpresupuestosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnpresupuestosActionPerformed
        new Presupuestos(this, true).setVisible(true);

        //CONVERTIR PRESUPUESTO
        if (ConvertirPresupuesto == 1) {
            borrartabla();
            ConexionMySQL cc = new ConexionMySQL();
            Connection cn = cc.Conectar();
            Statement SelectPresupuestosdetalle = null;
            Statement SelectPresupuestos = null;
            String sqlPresupuestosdetalle = "SELECT * FROM vista_presupuestos_convertir_detalle WHERE idPresupuestos = " + idPresupuesto;
            model2 = (DefaultTableModel) tablapedidos.getModel();
            String sqlPresupuestos = "SELECT * FROM vista_presupuestos_convertir WHERE idPresupuestos = " + idPresupuesto;
            fnRedondear redondear = new fnRedondear();
            try {
                SelectPresupuestosdetalle = cn.createStatement();
                ResultSet rsPresupuestosdetalle = SelectPresupuestosdetalle.executeQuery(sqlPresupuestosdetalle);
                while (rsPresupuestosdetalle.next()) {
                    Object nuevo[] = {
                        rsPresupuestosdetalle.getString(1),
                        rsPresupuestosdetalle.getString(2),
                        rsPresupuestosdetalle.getString(3),
                        rsPresupuestosdetalle.getString(4),
                        rsPresupuestosdetalle.getString(5),
                        rsPresupuestosdetalle.getString(6),
                        rsPresupuestosdetalle.getString(7),
                        rsPresupuestosdetalle.getString(8),
                        Double.valueOf(redondear.dosDigitos(redondear.dosDigitos(rsPresupuestosdetalle.getDouble(4) * (1 - (rsPresupuestosdetalle.getDouble(5) / 100)) * (1 + (rsPresupuestosdetalle.getDouble(7) / 100))) * rsPresupuestosdetalle.getDouble(8)))};
                    model2.addRow(nuevo);
                }
                tablapedidos.setModel(model2);
                tablapedidos.setAutoCreateRowSorter(true);
                tamañotablapedido();
                txtbuscar.setText("");
                txtbuscar.requestFocus();
                tipoventa.setText("Pedido");
                cargarnumero();

                ////Cliente
                SelectPresupuestos = cn.createStatement();
                ResultSet rsPresupuestos = SelectPresupuestos.executeQuery(sqlPresupuestos);
                while (rsPresupuestos.next()) {
                    IdCliente = rsPresupuestos.getInt(1);
                    nombreCliente = rsPresupuestos.getString(2);
                    ResposabilidadCliente = rsPresupuestos.getString(3);
                    TipodocCliente = rsPresupuestos.getString(4);
                    DocCliente = rsPresupuestos.getString(5);
                    idFormaDePago = rsPresupuestos.getInt(6);
                    idTipoDePago = rsPresupuestos.getInt(10);
                    /*DefaultComboBoxModel value = new DefaultComboBoxModel();
                    cboforma.setModel(value);
                    value.setSelectedItem(new cboFormasdepago(rsPresupuestos.getInt(6), rsPresupuestos.getString(7), rsPresupuestos.getDouble(8)));*/

                }
                cargarFormaDePago(idFormaDePago, idTipoDePago);
                txtnombreCliente.setText(nombreCliente);
                txtresposabilidadCliente.setText(ResposabilidadCliente);
                txttipodocCliente.setText(TipodocCliente);
                txtdocCliente.setText(DocCliente);
                /////////
                cargatotales();                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (SelectPresupuestos != null) {
                        SelectPresupuestos.close();
                    }
                    if (SelectPresupuestosdetalle != null) {
                        SelectPresupuestosdetalle.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
        //Agregar Presupuesto
        if (AgregarPresupuesto == 1) {
            ConvertirPresupuesto = 0;
            borrartabla();
            cargarformadepago();
            cargatotales();
            txtbuscar.setText("");
            txtbuscar.requestFocus();
            tipoventa.setText("Presupuesto");
            cargarnumero();
            AgregarPresupuesto = 0;
        }
    }//GEN-LAST:event_mnpresupuestosActionPerformed

    private void mnformasdepagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnformasdepagoActionPerformed
        new FormasdePagos(this, true).setVisible(true);
        cargarformadepago();
    }//GEN-LAST:event_mnformasdepagoActionPerformed

    private void mnmarcasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnmarcasActionPerformed
        new Marcas(this, true).setVisible(true);
    }//GEN-LAST:event_mnmarcasActionPerformed

    private void mnctacteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnctacteActionPerformed
        new CuentasCorrientes(this, true).setVisible(true);
    }//GEN-LAST:event_mnctacteActionPerformed

    private void mnivacompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnivacompraActionPerformed
        new IvaCompra(this, true).setVisible(true);
    }//GEN-LAST:event_mnivacompraActionPerformed

    private void mncajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mncajaActionPerformed
        new Caja(this, true).setVisible(true);
    }//GEN-LAST:event_mncajaActionPerformed

    private void mnegresosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnegresosActionPerformed
        new Egresos(this, true).setVisible(true);
    }//GEN-LAST:event_mnegresosActionPerformed

    private void mnusuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnusuariosActionPerformed
        new Usuarios(this, true).setVisible(true);
    }//GEN-LAST:event_mnusuariosActionPerformed

    private void mnfacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnfacturacionActionPerformed
        new Facturacion(this, true).setVisible(true);
    }//GEN-LAST:event_mnfacturacionActionPerformed

    private void mntipofacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mntipofacturacionActionPerformed
        new AfipTipoFacturacion(this, true).setVisible(true);
        //Ver que tipo de facturacion usa para habilitar el menu
        if (AfipTipoFacturacion.descripcion.equals("WSFE")) {
            jMenuWebServices.setVisible(true);
        }


    }//GEN-LAST:event_mntipofacturacionActionPerformed

    private void mnpuntodeventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnpuntodeventaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnpuntodeventaActionPerformed

    private void ProveedoresyProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProveedoresyProductosActionPerformed
        new ProveedoresyProductos(this, true).setVisible(true);
    }//GEN-LAST:event_ProveedoresyProductosActionPerformed

    private void OrdenesdeCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrdenesdeCompraActionPerformed
        new ConsultasOrdenesCompra(this, true).setVisible(true);
    }//GEN-LAST:event_OrdenesdeCompraActionPerformed

    private void RankingProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RankingProductosActionPerformed
        new ConsultasRankingProductos(this, true).setVisible(true);
    }//GEN-LAST:event_RankingProductosActionPerformed

    private void RankingClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RankingClientesActionPerformed
        new ConsultasRankingClientes(this, true).setVisible(true);
    }//GEN-LAST:event_RankingClientesActionPerformed

    private void tablapedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapedidosKeyPressed
        /////Borrar una fila de la tabla pedidos
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int filaspedidos = tablapedidos.getRowCount();
            if (tablapedidos.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                if (filaspedidos != 0) {
                    double cantidadpedidos = Double.valueOf(tablapedidos.getValueAt((tablapedidos.getSelectedRow()), 7).toString());
                    String codigopedidos = tablapedidos.getValueAt(tablapedidos.getSelectedRow(), 0).toString();
                    /////Recorro la tabla buscar
                    int filasbuscar = tablabuscar.getRowCount();
                    if (filasbuscar != 0) {
                        int i = 0;
                        while (filasbuscar > i) {
                            String codigobuscar = tablabuscar.getValueAt(i, 0).toString();
                            if (codigopedidos.equals(codigobuscar)) {
                                double stock = Double.valueOf(tablabuscar.getValueAt(i, 7).toString());
                                /////Sumo en la tabla buscar la cantidad y stock
                                System.out.println("Formularios.Main.tablapedidosKeyPressed()");
                                tablabuscar.setValueAt((stock + cantidadpedidos), i, 7);
                                break;
                            }
                            i++;
                        }
                    }
                    model3 = (DefaultTableModel) tablapedidos.getModel();
                    model3.removeRow(tablapedidos.getSelectedRow());
                    model3 = null;
                } else {
                    JOptionPane.showMessageDialog(null, "No hay ningun producto en la lista...");
                }
            }
            cargatotales();
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cargatotales();
            tablapedidos.transferFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablapedidos.transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablapedidosKeyPressed

    private void txtdescuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescuentoActionPerformed
        cargatotales();
    }//GEN-LAST:event_txtdescuentoActionPerformed

    private void cbotarjetacreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotarjetacreditoActionPerformed
        ///  indexformapago = 2;
        cargatotales();
    }//GEN-LAST:event_cbotarjetacreditoActionPerformed

    private void cbotarjetadebitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotarjetadebitoActionPerformed
        // indexformapago = 3;
        cargatotales();
    }//GEN-LAST:event_cbotarjetadebitoActionPerformed

    private void cboformactacteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboformactacteActionPerformed
        //  indexformapago = 1;
        cargatotales();
    }//GEN-LAST:event_cboformactacteActionPerformed

    private void cboformachequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboformachequeActionPerformed
//        if(){cboformacheque.getSelectedIndex()}
        /// indexformapago = 4;
        cargatotales();
    }//GEN-LAST:event_cboformachequeActionPerformed

    private void radioContadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioContadoActionPerformed
        cboformactacte.setEnabled(false);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(false);
        indexformapago = 0;
        cargatotales();
    }//GEN-LAST:event_radioContadoActionPerformed

    private void radioCuentaCorrienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCuentaCorrienteActionPerformed
        cboformactacte.setEnabled(true);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(false);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setText("0.0");
        indexformapago = 1;
        cargatotales();
    }//GEN-LAST:event_radioCuentaCorrienteActionPerformed

    private void radioCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCreditoActionPerformed
        cboformactacte.setEnabled(false);
        cboformactacte.setSelectedIndex(0);
        cbotarjetacredito.setEnabled(true);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(false);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setText("0.0");
        indexformapago = 2;
        cargatotales();
    }//GEN-LAST:event_radioCreditoActionPerformed

    private void radioDebitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioDebitoActionPerformed
        cboformactacte.setEnabled(false);
        cboformactacte.setSelectedIndex(0);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(true);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(false);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setText("0.0");
        indexformapago = 3;
        cargatotales();
    }//GEN-LAST:event_radioDebitoActionPerformed

    private void radioChequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioChequeActionPerformed
        cboformactacte.setEnabled(false);
        cboformactacte.setSelectedIndex(0);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(true);
        txtdescuento.setEditable(false);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setText("0.0");
        indexformapago = 4;
        cargatotales();
    }//GEN-LAST:event_radioChequeActionPerformed

    private void radioOtrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioOtrosActionPerformed
        cboformactacte.setEnabled(false);
        cboformactacte.setSelectedIndex(0);
        cbotarjetacredito.setEnabled(false);
        cbotarjetacredito.setSelectedIndex(0);
        cbotarjetadebito.setEnabled(false);
        cbotarjetadebito.setSelectedIndex(0);
        cboformacheque.setEnabled(false);
        cboformacheque.setSelectedIndex(0);
        txtdescuento.setEditable(true);
        txtdescuentoPorcentaje.setEditable(true);
        txtinteres.setEditable(false);
        indexformapago = 5;
        cargatotales();
    }//GEN-LAST:event_radioOtrosActionPerformed

    private void txtinteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtinteresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtinteresActionPerformed

    private void txtsubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsubtotalActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new IngresosEgresos(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void txtinteresPorcentajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtinteresPorcentajeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtinteresPorcentajeActionPerformed

    private void txtdescuentoPorcentajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescuentoPorcentajeActionPerformed
        double subtotal = Double.valueOf(txtsubtotal.getText());
        double porcentaje = Double.valueOf(txtdescuentoPorcentaje.getText());
        double descuento = 0.0;

        descuento = ((porcentaje * subtotal) / 100);
        fnRedondear r = new fnRedondear();
        txtdescuento.setText(Double.toString(r.dosDigitos(descuento)));
        cargatotales();
    }//GEN-LAST:event_txtdescuentoPorcentajeActionPerformed

    private void mnbonificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnbonificacionActionPerformed
        new Bonificacion(this, true).setVisible(true);
    }//GEN-LAST:event_mnbonificacionActionPerformed

    private void mniniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniniciarActionPerformed

    }//GEN-LAST:event_mniniciarActionPerformed

    private void mnpararActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnpararActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnpararActionPerformed

    private void tablapedidosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablapedidosMouseClicked
        //aca el doble clic precio venta
    }//GEN-LAST:event_tablapedidosMouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        new ActualizarPrecios2(this, true).setVisible(true);    
         cargartablabuscar("");
    }//GEN-LAST:event_jMenuItem1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem OrdenesdeCompra;
    private javax.swing.JMenuItem ProveedoresyProductos;
    private javax.swing.JMenuItem RankingClientes;
    private javax.swing.JMenuItem RankingProductos;
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btncliente;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbodeposito;
    private javax.swing.JComboBox<String> cboformacheque;
    private javax.swing.JComboBox<String> cboformactacte;
    private javax.swing.JComboBox<String> cbotarjetacredito;
    private javax.swing.JComboBox<String> cbotarjetadebito;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenuAfip;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuConsultas;
    private javax.swing.JMenu jMenuDatos;
    private javax.swing.JMenuItem jMenuFECAEAConsultar;
    private javax.swing.JMenuItem jMenuFECompUltimoAutorizado;
    private javax.swing.JMenuItem jMenuFEDummy;
    private javax.swing.JMenuItem jMenuFEParamGetCotizacion;
    private javax.swing.JMenuItem jMenuFEParamGetPtosVenta;
    private javax.swing.JMenuItem jMenuFEParamGetTiposCbte;
    private javax.swing.JMenuItem jMenuFEParamGetTiposConcepto;
    private javax.swing.JMenuItem jMenuFEParamGetTiposDoc;
    private javax.swing.JMenuItem jMenuFEParamGetTiposIva;
    private javax.swing.JMenuItem jMenuFEParamGetTiposMonedas;
    private javax.swing.JMenuItem jMenuFEParamGetTiposOpcional;
    private javax.swing.JMenuItem jMenuFEParamGetTiposPaises;
    private javax.swing.JMenuItem jMenuFEParamGetTiposTributos;
    private javax.swing.JMenu jMenuHasar2G1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenu jMenuVentas;
    private javax.swing.JMenu jMenuWebServices;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem mnbonificacion;
    private javax.swing.JMenuItem mncaja;
    private javax.swing.JMenuItem mncategoria;
    private javax.swing.JMenuItem mnclientes;
    private javax.swing.JMenuItem mnctacte;
    private javax.swing.JMenuItem mnegresos;
    private javax.swing.JMenuItem mnfacturacion;
    private javax.swing.JMenuItem mnformasdepago;
    private javax.swing.JMenuItem mniniciar;
    private javax.swing.JMenuItem mnivacompra;
    private javax.swing.JMenuItem mnmarcas;
    private javax.swing.JMenuItem mnparar;
    private javax.swing.JMenuItem mnpedidos;
    private javax.swing.JMenuItem mnpresupuestos;
    private javax.swing.JMenuItem mnproducto;
    private javax.swing.JMenuItem mnproveedor;
    private javax.swing.JMenuItem mnpuntodeventa;
    private javax.swing.JMenuItem mnstock;
    private javax.swing.JMenuItem mntipofacturacion;
    private javax.swing.JMenuItem mnusuarios;
    private javax.swing.JRadioButton radioCheque;
    private javax.swing.JRadioButton radioContado;
    private javax.swing.JRadioButton radioCredito;
    private javax.swing.JRadioButton radioCuentaCorriente;
    private javax.swing.JRadioButton radioDebito;
    private javax.swing.JRadioButton radioOtros;
    private javax.swing.JTable tablabuscar;
    private javax.swing.JTable tablapedidos;
    private javax.swing.JLabel tipoventa;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtdescuento;
    private javax.swing.JTextField txtdescuentoPorcentaje;
    private javax.swing.JTextField txtdocCliente;
    private javax.swing.JTextField txtfecha;
    private javax.swing.JTextField txtinteres;
    private javax.swing.JTextField txtinteresPorcentaje;
    private javax.swing.JTextField txtnombreCliente;
    private javax.swing.JFormattedTextField txtnumero;
    private javax.swing.JTextField txtresposabilidadCliente;
    private javax.swing.JTextField txtsubtotal;
    private javax.swing.JTextField txttipodocCliente;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
