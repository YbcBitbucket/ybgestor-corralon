package Formularios;

import Clases.ClaseUsuarios;
import Clases.fnEscape;
import javax.swing.JOptionPane;

public class UsuariosAgrega extends javax.swing.JDialog {

    public UsuariosAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        fnEscape.funcionescape(this);
    }

    String cargarDatos (){
        String[] Datos = new String[15];
        if(jCheckBoxDatosCat.isSelected()){Datos[0]="1";}
        if(jCheckBoxDatosMarcas.isSelected()){Datos[1]="1";}
        if(jCheckBoxDatosProv.isSelected()){Datos[2]="1";}
        if(jCheckBoxDatosProd.isSelected()){Datos[3]="1";}
        if(jCheckBoxDatosStock.isSelected()){Datos[4]="1";}
        if(jCheckBoxDatosClientes.isSelected()){Datos[5]="1";}
        if(jCheckBoxDatosIvaCom.isSelected()){Datos[6]="1";}
        if(jCheckBoxDatosEgresos.isSelected()){Datos[7]="1";}
        if(jCheckBoxDatosUsuarios.isSelected()){Datos[8]="1";}
        
        if(Datos[0] == null){Datos[0]="0";}
        String acceso_datos = Datos[0];
        
        for(int i = 1; i<Datos.length; i++) {
            if(Datos[i] == null){
                Datos[i]="0";
            }
            acceso_datos = acceso_datos + Datos[i];
        }
        return acceso_datos;
    }
    
    String cargarVentas (){
        String[] Ventas = new String[15];
        if(jCheckBoxVentasPedidos.isSelected()){Ventas[0]="1";}
        if(jCheckBoxVentasPre.isSelected()){Ventas[1]="1";}
        if(jCheckBoxVentasBon.isSelected()){Ventas[2]="1";}
        if(jCheckBoxVentasFormadePago.isSelected()){Ventas[3]="1";}
        if(jCheckBoxVentasCtaCte.isSelected()){Ventas[4]="1";}
        if(jCheckBoxVentasCaja.isSelected()){Ventas[5]="1";}
        
        if(Ventas[0] == null){Ventas[0]="0";}
        String acceso_ventas = Ventas[0];
        
        for(int i = 1; i<Ventas.length; i++) {
            if(Ventas[i] == null){
                Ventas[i]="0";
            }
            acceso_ventas = acceso_ventas + Ventas[i];
        }
        return acceso_ventas;
    }
    
    String cargarConsultas (){
        String[] Consultas = new String[15];
        if(jCheckBoxConsultasRCli.isSelected()){Consultas[0]="1";}
        if(jCheckBoxConsultasRPro.isSelected()){Consultas[1]="1";}
        if(jCheckBoxConsultasOrd.isSelected()){Consultas[2]="1";}
        
        if(Consultas[0] == null){Consultas[0]="0";}
        String acceso_Consultas = Consultas[0];
        
        for(int i = 1; i<Consultas.length; i++) {
            if(Consultas[i] == null){
                Consultas[i]="0";
            }
            acceso_Consultas = acceso_Consultas + Consultas[i];
        }
        return acceso_Consultas;
    }
    
    String cargarAfip (){
        String[] Afip = new String[15];
        if(jCheckBoxAfipConfigurar.isSelected()){Afip[0]="1";}
        if(jCheckBoxAfipWebServices.isSelected()){Afip[1]="1";}
        if(jCheckBoxAfipHasar.isSelected()){Afip[2]="1";}
        if(jCheckBoxAfipHasar2G.isSelected()){Afip[3]="1";}
        
        if(Afip[0] == null){Afip[0]="0";}
        String acceso_Afip = Afip[0];
        
        for(int i = 1; i<Afip.length; i++) {
            if(Afip[i] == null){
                Afip[i]="0";
            }
            acceso_Afip = acceso_Afip + Afip[i];
        }
        return acceso_Afip;
    }
    
    void limpiarjCheckBox(){
        jCheckBoxDatosCat.setSelected(true);
        jCheckBoxDatosMarcas.setSelected(true);
        jCheckBoxDatosProv.setSelected(true);
        jCheckBoxDatosProd.setSelected(true);
        jCheckBoxDatosStock.setSelected(true);
        jCheckBoxDatosClientes.setSelected(true);
        jCheckBoxDatosIvaCom.setSelected(true);
        jCheckBoxDatosEgresos.setSelected(true);
        jCheckBoxDatosUsuarios.setSelected(true);
        jCheckBoxVentasPedidos.setSelected(true);
        jCheckBoxVentasPre.setSelected(true);
        jCheckBoxVentasBon.setSelected(true);
        jCheckBoxVentasFormadePago.setSelected(true);
        jCheckBoxVentasCtaCte.setSelected(true);
        jCheckBoxVentasCaja.setSelected(true);
        jCheckBoxConsultasRCli.setSelected(true);
        jCheckBoxConsultasRPro.setSelected(true);
        jCheckBoxConsultasOrd.setSelected(true);
        jCheckBoxAfipConfigurar.setSelected(true);
        jCheckBoxAfipWebServices.setSelected(true);
        jCheckBoxAfipHasar.setSelected(true);
        jCheckBoxAfipHasar2G.setSelected(true);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtusuario = new javax.swing.JTextField();
        txtcontraseña = new javax.swing.JTextField();
        btnsalir = new javax.swing.JButton();
        btnagregar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jCheckBoxDatosCat = new javax.swing.JCheckBox();
        jCheckBoxDatosMarcas = new javax.swing.JCheckBox();
        jCheckBoxDatosProv = new javax.swing.JCheckBox();
        jCheckBoxDatosProd = new javax.swing.JCheckBox();
        jCheckBoxDatosStock = new javax.swing.JCheckBox();
        jCheckBoxDatosClientes = new javax.swing.JCheckBox();
        jCheckBoxDatosIvaCom = new javax.swing.JCheckBox();
        jCheckBoxDatosEgresos = new javax.swing.JCheckBox();
        jCheckBoxDatosUsuarios = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jCheckBoxVentasPedidos = new javax.swing.JCheckBox();
        jCheckBoxVentasPre = new javax.swing.JCheckBox();
        jCheckBoxVentasBon = new javax.swing.JCheckBox();
        jCheckBoxVentasFormadePago = new javax.swing.JCheckBox();
        jCheckBoxVentasCtaCte = new javax.swing.JCheckBox();
        jCheckBoxVentasCaja = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        jCheckBoxAfipWebServices = new javax.swing.JCheckBox();
        jCheckBoxAfipHasar = new javax.swing.JCheckBox();
        jCheckBoxAfipHasar2G = new javax.swing.JCheckBox();
        jCheckBoxAfipConfigurar = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jCheckBoxConsultasRCli = new javax.swing.JCheckBox();
        jCheckBoxConsultasRPro = new javax.swing.JCheckBox();
        jCheckBoxConsultasOrd = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setText("Apellidos:");

        jLabel2.setText("Nombres:");

        jLabel3.setText("Nombre de Usuario:");

        jLabel4.setText("Contraseña:");

        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtapellidoKeyReleased(evt);
            }
        });

        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtapellido, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(5, 5, 5))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 16, Short.MAX_VALUE))
        );

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setNextFocusableComponent(txtapellido);
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setMnemonic('a');
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxDatosCat.setSelected(true);
        jCheckBoxDatosCat.setText("Categorias");

        jCheckBoxDatosMarcas.setSelected(true);
        jCheckBoxDatosMarcas.setText("Marcas");

        jCheckBoxDatosProv.setSelected(true);
        jCheckBoxDatosProv.setText("Proveedores");

        jCheckBoxDatosProd.setSelected(true);
        jCheckBoxDatosProd.setText("Productos");

        jCheckBoxDatosStock.setSelected(true);
        jCheckBoxDatosStock.setText("Stock");

        jCheckBoxDatosClientes.setSelected(true);
        jCheckBoxDatosClientes.setText("Clientes");
        jCheckBoxDatosClientes.setNextFocusableComponent(btnagregar);

        jCheckBoxDatosIvaCom.setSelected(true);
        jCheckBoxDatosIvaCom.setText("Iva Compra");
        jCheckBoxDatosIvaCom.setNextFocusableComponent(btnagregar);

        jCheckBoxDatosEgresos.setSelected(true);
        jCheckBoxDatosEgresos.setText("Egresos");
        jCheckBoxDatosEgresos.setNextFocusableComponent(btnagregar);

        jCheckBoxDatosUsuarios.setSelected(true);
        jCheckBoxDatosUsuarios.setText("Usuarios");
        jCheckBoxDatosUsuarios.setNextFocusableComponent(btnagregar);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBoxDatosEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxDatosMarcas, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxDatosProd, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxDatosStock)
                    .addComponent(jCheckBoxDatosClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jCheckBoxDatosIvaCom, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jCheckBoxDatosProv, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jCheckBoxDatosCat)
                    .addComponent(jCheckBoxDatosUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jCheckBoxDatosCat)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxDatosMarcas)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxDatosProv)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosProd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosStock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosClientes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosIvaCom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosEgresos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxDatosUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ventas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxVentasPedidos.setSelected(true);
        jCheckBoxVentasPedidos.setText("Pedidos");

        jCheckBoxVentasPre.setSelected(true);
        jCheckBoxVentasPre.setText("Presupuestos");

        jCheckBoxVentasBon.setSelected(true);
        jCheckBoxVentasBon.setText("Bonificacion");

        jCheckBoxVentasFormadePago.setSelected(true);
        jCheckBoxVentasFormadePago.setText("Formas de Pago");

        jCheckBoxVentasCtaCte.setSelected(true);
        jCheckBoxVentasCtaCte.setText("Cuentas Corrientes");

        jCheckBoxVentasCaja.setSelected(true);
        jCheckBoxVentasCaja.setText("Caja");
        jCheckBoxVentasCaja.setNextFocusableComponent(btnagregar);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBoxVentasCtaCte)
                    .addComponent(jCheckBoxVentasCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxVentasBon)
                    .addComponent(jCheckBoxVentasPedidos)
                    .addComponent(jCheckBoxVentasPre)
                    .addComponent(jCheckBoxVentasFormadePago))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jCheckBoxVentasPedidos)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxVentasPre)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxVentasBon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxVentasFormadePago)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxVentasCtaCte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxVentasCaja)
                .addGap(78, 78, 78))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Afip", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxAfipWebServices.setSelected(true);
        jCheckBoxAfipWebServices.setText("Web Services");

        jCheckBoxAfipHasar.setSelected(true);
        jCheckBoxAfipHasar.setText("Hasar");

        jCheckBoxAfipHasar2G.setSelected(true);
        jCheckBoxAfipHasar2G.setText("Hasar 2G");

        jCheckBoxAfipConfigurar.setSelected(true);
        jCheckBoxAfipConfigurar.setText("Configurar Facturacion");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBoxAfipWebServices)
                    .addComponent(jCheckBoxAfipHasar)
                    .addComponent(jCheckBoxAfipHasar2G)
                    .addComponent(jCheckBoxAfipConfigurar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBoxAfipConfigurar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxAfipWebServices)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxAfipHasar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxAfipHasar2G)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Consultas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxConsultasRCli.setSelected(true);
        jCheckBoxConsultasRCli.setText("Ranking Clientes");

        jCheckBoxConsultasRPro.setSelected(true);
        jCheckBoxConsultasRPro.setText("Ranking Productos");

        jCheckBoxConsultasOrd.setSelected(true);
        jCheckBoxConsultasOrd.setText("Ordenes de Compra");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBoxConsultasOrd)
                    .addComponent(jCheckBoxConsultasRCli)
                    .addComponent(jCheckBoxConsultasRPro))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jCheckBoxConsultasRCli)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxConsultasRPro)
                .addGap(3, 3, 3)
                .addComponent(jCheckBoxConsultasOrd)
                .addGap(156, 156, 156))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtapellidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyReleased
        String texto = txtapellido.getText().toUpperCase();
        txtapellido.setText(texto);
    }//GEN-LAST:event_txtapellidoKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        //Chequea q no haya datos vacios
        if (txtapellido.getText().equals("") || txtnombre.getText().equals("") || txtusuario.getText().equals("")
                || txtcontraseña.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "No ingreso todos los datos del producto");
        } else {
            ClaseUsuarios usuarios = new ClaseUsuarios();
            if (usuarios.AgregarUsuarios(txtapellido.getText(), txtnombre.getText(), txtusuario.getText(), txtcontraseña.getText(), cargarDatos(), cargarVentas(), cargarConsultas(), cargarAfip()) == 1) {
                //Limpio Campos
                txtapellido.setText("");
                txtnombre.setText("");
                txtusuario.setText("");
                txtcontraseña.setText("");
                limpiarjCheckBox();
                btnagregar.transferFocus();
            }
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        String texto = txtnombre.getText().toUpperCase();
        txtnombre.setText(texto);
    }//GEN-LAST:event_txtnombreKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JCheckBox jCheckBoxAfipConfigurar;
    private javax.swing.JCheckBox jCheckBoxAfipHasar;
    private javax.swing.JCheckBox jCheckBoxAfipHasar2G;
    private javax.swing.JCheckBox jCheckBoxAfipWebServices;
    private javax.swing.JCheckBox jCheckBoxConsultasOrd;
    private javax.swing.JCheckBox jCheckBoxConsultasRCli;
    private javax.swing.JCheckBox jCheckBoxConsultasRPro;
    private javax.swing.JCheckBox jCheckBoxDatosCat;
    private javax.swing.JCheckBox jCheckBoxDatosClientes;
    private javax.swing.JCheckBox jCheckBoxDatosEgresos;
    private javax.swing.JCheckBox jCheckBoxDatosIvaCom;
    private javax.swing.JCheckBox jCheckBoxDatosMarcas;
    private javax.swing.JCheckBox jCheckBoxDatosProd;
    private javax.swing.JCheckBox jCheckBoxDatosProv;
    private javax.swing.JCheckBox jCheckBoxDatosStock;
    private javax.swing.JCheckBox jCheckBoxDatosUsuarios;
    private javax.swing.JCheckBox jCheckBoxVentasBon;
    private javax.swing.JCheckBox jCheckBoxVentasCaja;
    private javax.swing.JCheckBox jCheckBoxVentasCtaCte;
    private javax.swing.JCheckBox jCheckBoxVentasFormadePago;
    private javax.swing.JCheckBox jCheckBoxVentasPedidos;
    private javax.swing.JCheckBox jCheckBoxVentasPre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtcontraseña;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
