package Formularios;

import Clases.ConexionMySQL;
import Clases.fnEscape;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.sql.Statement;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Marcas extends javax.swing.JDialog {

    int elim = 0;
    static DefaultListModel modeloLista = new DefaultListModel();  //Atributo de clase

    public Marcas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cargarlista("");
        initComponents();
        listamarcas.setModel(modeloLista);
        fnEscape.funcionescape(this);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    void cargarlista(String valor) {
        String Registros = "";
        String sql = "SELECT nombre FROM marcas WHERE nombre LIKE '%" + valor + "%'";
        modeloLista.clear();
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectMarcas = null;
        try {
            SelectMarcas = cn.createStatement();
            ResultSet rs = SelectMarcas.executeQuery(sql);
            while (rs.next()) {
                Registros = rs.getString("nombre");
                modeloLista.addElement(Registros);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarcas != null) {
                    SelectMarcas.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jPanel1 = new javax.swing.JPanel();
        txtnombre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        listamarcas = new javax.swing.JList();
        buscar = new javax.swing.JLabel();
        btnagregar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Caracter.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(10, 10, 10))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setNextFocusableComponent(listamarcas);
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        listamarcas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listamarcasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(listamarcas);

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(226, 226, 226)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectMarcas = null;
        PreparedStatement InsertarMarca = null;
        String nom;
        int agrega = 0;
        String sSQL = "";
        if ("".equals(txtnombre.getText())) {
            JOptionPane.showMessageDialog(null, "No ingreso ningun dato");
        } else {
            nom = txtnombre.getText();
            String sql = "SELECT nombre FROM marcas";
            try {
                SelectMarcas = cn.createStatement();
                ResultSet rsr = SelectMarcas.executeQuery(sql);
                int contador = 0; //inicio
                while (rsr.next()) {
                    contador++;
                }
                if (contador != 0) {
                    rsr.beforeFirst();
                    while (rsr.next()) {
                        if (nom.equals(rsr.getString("nombre"))) {
                            JOptionPane.showMessageDialog(null, "La marca ya esta en la base de datos");
                            agrega = 0;
                            break;
                        } else {
                            agrega = 1;
                        }
                    }
                } else {
                    agrega = 1;
                }
                if (agrega == 1) {
                    sSQL = "INSERT INTO marcas(nombre)"
                            + "VALUES(?)";
                    try {
                        InsertarMarca = cn.prepareStatement(sSQL);
                        InsertarMarca.setString(1, nom);
                        int n = InsertarMarca.executeUpdate();
                        if (n > 0) {
                            JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                        }
                        cargarlista("");
                        txtnombre.setText("");
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
                cargarlista("");
                txtnombre.setText("");
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, e);
            } finally {
                try {
                    if (SelectMarcas != null) {
                        SelectMarcas.close();
                    }
                    if (InsertarMarca != null) {
                        InsertarMarca.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        String texto = txtnombre.getText().toUpperCase();
        txtnombre.setText(texto);
        cargarlista(txtnombre.getText());
    }//GEN-LAST:event_txtnombreKeyReleased

    private void listamarcasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listamarcasKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (elim == 0) {
                int filasel;
                filasel = listamarcas.getSelectedIndex();
                if (filasel == -1) {
                    JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                } else {
                    int opcion = JOptionPane.showConfirmDialog(this, "Desea eliminar la marca?", "Mensaje", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        ConexionMySQL mysql = new ConexionMySQL();
                        Connection cn = mysql.Conectar();
                        Statement SelectIdMarca = null;
                        Statement SelectIdMarcaProducto = null;
                        PreparedStatement DeleteMarca = null;
                        try {
                           //////////////////consulta productos//////////////////////////////
                            int idMarcas = 0;
                            String nombre = listamarcas.getSelectedValue().toString();
                            int eliminaP = 0;
                            String sqlid = "SELECT idMarcas FROM marcas WHERE nombre='" + nombre + "'";
                            JOptionPane.showMessageDialog(null, nombre);
                            SelectIdMarca = cn.createStatement();
                            ResultSet rss = SelectIdMarca.executeQuery(sqlid);
                            while (rss.next()) {
                                idMarcas = rss.getInt("idMarcas");
                            }
                            String sql = "SELECT idMarcas FROM productos WHERE idMarcas=" + idMarcas + "";
                            SelectIdMarcaProducto = cn.createStatement();
                            ResultSet rs = SelectIdMarcaProducto.executeQuery(sql);
                            if (rs.next()) {
                                JOptionPane.showMessageDialog(null, "La marca no puede ser eliminada." + "\n" + "Pertenece a un producto existente." + "\n");
                                eliminaP = 0;

                            } else {
                                eliminaP = 1;
                            }
                            /////////////////elimina/////////////////////////////////////////////////////////
                            if (eliminaP == 1) {

                                DeleteMarca = cn.prepareStatement("DELETE FROM marcas WHERE idMarcas ='" + idMarcas + "'");
                                DeleteMarca.execute();
                                JOptionPane.showMessageDialog(null, "La marca fué eliminada.");
                                cargarlista("");
                                txtnombre.setText("");
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        } finally {
                            try {
                                if (SelectIdMarca != null) {
                                    SelectIdMarca.close();
                                }
                                if (SelectIdMarcaProducto != null) {
                                    SelectIdMarcaProducto.close();
                                }
                                if (DeleteMarca != null) {
                                    DeleteMarca.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                    }
                }
            } else {
                this.dispose();
            }
        }
    }//GEN-LAST:event_listamarcasKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargarlista(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtbuscar.transferFocus();
        }
    }//GEN-LAST:event_txtbuscarKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList listamarcas;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
