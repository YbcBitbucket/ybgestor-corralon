package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnExportar;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Stock extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String idProductos = "";

    public Stock(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardatosdepositos();
        cargarDatosCategoria();
        cargartabla("");
        dobleclick();

    }

    //////////////////////FUNCION CARGAR DEPOSITO //////////////////////
    void cargardatosdepositos() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDeposito = null;
        sSQL = "SELECT nombre FROM deposito";
        cbodeposito.removeAllItems();
        cbodeposito.addItem("Depositos");
        try {
            SelectDeposito = cn.createStatement();
            ResultSet rs = SelectDeposito.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbodeposito.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDeposito != null) {
                    SelectDeposito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR DEPOSITO //////////////////////
    void cargarDatosCategoria() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDeposito = null;
        sSQL = "SELECT nombre FROM categorias";
        cbocategoria.removeAllItems();
        cbocategoria.addItem("Categorias");
        try {
            SelectDeposito = cn.createStatement();
            ResultSet rs = SelectDeposito.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbocategoria.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDeposito != null) {
                    SelectDeposito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"Id", "Codigo", "Producto", "Cantidad", "Deposito", "Categoria"};
        String[] Registros = new String[6];
        String sql = "SELECT * FROM vista_stock "
                + "WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + valor + "%' ";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                model.addRow(Registros);
            }
            tablastock.setModel(model);
            tablastock.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablastock.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablastock.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(0).setMaxWidth(0);
            tablastock.getColumnModel().getColumn(0).setMinWidth(0);
            tablastock.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablastock.getColumnModel().getColumn(1).setPreferredWidth(80);
            tablastock.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablastock.getColumnModel().getColumn(3).setPreferredWidth(60);
            tablastock.getColumnModel().getColumn(5).setMaxWidth(0);
            tablastock.getColumnModel().getColumn(5).setMinWidth(0);
            tablastock.getColumnModel().getColumn(5).setPreferredWidth(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION FILTRAR DEPOSITO//////////////////////
    void filtrardeposito(String valor) {
        String[] Titulo = {"Id", "Codigo", "Producto", "Cantidad", "Deposito"};
        String[] Registros = new String[5];
        String sql = "SELECT * FROM vista_stock "
                + "WHERE deposito='" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                model.addRow(Registros);

            }
            tablastock.setModel(model);
            tablastock.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablastock.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablastock.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(0).setMaxWidth(0);
            tablastock.getColumnModel().getColumn(0).setMinWidth(0);
            tablastock.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablastock.getColumnModel().getColumn(1).setPreferredWidth(80);
            tablastock.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablastock.getColumnModel().getColumn(3).setPreferredWidth(60);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

//////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablastock.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablastock.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idProductos = tablastock.getValueAt(tablastock.getSelectedRow(), 0).toString();
                        new StockDetalle(null, true).setVisible(true);
                        cargartabla("");
                    }
                }
            }
        });
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablastock = new javax.swing.JTable();
        cbodeposito = new javax.swing.JComboBox();
        buscar = new javax.swing.JLabel();
        cbocategoria = new javax.swing.JComboBox();
        btndetalle = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnagregar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();
        btndeposito = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablastock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablastockKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablastock);

        cbodeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbodepositoActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        cbocategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(cbodeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbocategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar)
                    .addComponent(cbodeposito)
                    .addComponent(cbocategoria))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btndetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Documento.png"))); // NOI18N
        btndetalle.setText("Detalle");
        btndetalle.setMaximumSize(new java.awt.Dimension(120, 50));
        btndetalle.setMinimumSize(new java.awt.Dimension(120, 50));
        btndetalle.setPreferredSize(new java.awt.Dimension(120, 50));
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setMnemonic('a');
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setMnemonic('e');
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btndeposito.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndeposito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btndeposito.setText("Deposito");
        btndeposito.setMaximumSize(new java.awt.Dimension(120, 50));
        btndeposito.setMinimumSize(new java.awt.Dimension(120, 50));
        btndeposito.setPreferredSize(new java.awt.Dimension(120, 50));
        btndeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndepositoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btndeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btndeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        new StockAgrega(null, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnagregarActionPerformed

    private void tablastockKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablastockKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablastockKeyPressed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablastock.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablastock);
            nom.add("Tabla Stock");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablastock.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idProductos = tablastock.getValueAt(tablastock.getSelectedRow(), 0).toString();
            new StockDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btndepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndepositoActionPerformed
        new Depositos(null, true).setVisible(true);
    }//GEN-LAST:event_btndepositoActionPerformed

    private void cbodepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbodepositoActionPerformed
        if (!cbodeposito.getSelectedItem().toString().equals("Depositos")) {
            filtrardeposito(cbodeposito.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cbodepositoActionPerformed

    private void cbocategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriaActionPerformed
        if (!cbocategoria.getSelectedItem().toString().equals("Categorias")) {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*" + cbocategoria.getSelectedItem().toString() + ".*"));
            tablastock.setRowSorter(sorter);
        } else {
            TableRowSorter sorter = new TableRowSorter(model);
            sorter.setRowFilter(RowFilter.regexFilter(".*.*"));
            tablastock.setRowSorter(sorter);
        }
    }//GEN-LAST:event_cbocategoriaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btndeposito;
    private javax.swing.JButton btndetalle;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cbocategoria;
    private javax.swing.JComboBox cbodeposito;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablastock;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
