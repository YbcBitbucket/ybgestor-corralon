package Formularios;

import Clases.ConexionMySQL;
import Clases.fnEscape;
import Clases.fnesNumerico;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FormasdePagosCheque extends javax.swing.JDialog {

    DefaultTableModel model;
    int mod = 0;

    public FormasdePagosCheque(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        dobleclick();
        cargartabla("");
        fnEscape.funcionescape(this);
        this.setResizable(false);
        
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    void dobleclick() {
        tablaforma.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel;
                    filasel = tablaforma.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        cargardatos(tablaforma.getValueAt(tablaforma.getSelectedRow(), 0).toString());
                        mod = 1;
                    }
                }
            }
        });
    }

    void cargardatos(String id) {
        String sSQL = "";
        ConexionMySQL MySQL = new ConexionMySQL();
        Connection cn = MySQL.Conectar();
        Statement SelectFormadePago = null;
        sSQL = "SELECT * FROM formasdepago_cheque WHERE idFormaPago_Cheque=" + id;
        try {
            SelectFormadePago = cn.createStatement();
            ResultSet rs = SelectFormadePago.executeQuery(sSQL);
            rs.next();
            txtnombre.setText(rs.getString("descripcion"));
            txtdescuento.setText(rs.getString("descuento"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (SelectFormadePago != null) {
                    SelectFormadePago.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargartabla(String valor) {
        String[] Titulo = {"Nº", "Nombre", "Descuento"};
        String[] Registros = new String[3];

        String sql = "SELECT * FROM formasdepago_cheque WHERE descripcion LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL MySQL = new ConexionMySQL();
        Connection cn = MySQL.Conectar();
        Statement SelectFormadePago = null;
        try {
            SelectFormadePago = cn.createStatement();
            ResultSet rs = SelectFormadePago.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("idFormaPago_Cheque");
                Registros[1] = rs.getString("descripcion");
                Registros[2] = rs.getString("descuento");
                model.addRow(Registros);
            }
            tablaforma.setModel(model);
            tablaforma.setAutoCreateRowSorter(true);
            tablaforma.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaforma.getColumnModel().getColumn(0).setMinWidth(0);
            tablaforma.getColumnModel().getColumn(0).setPreferredWidth(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (SelectFormadePago != null) {
                    SelectFormadePago.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtnombre = new javax.swing.JTextField();
        txtdescuento = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btncancelar = new javax.swing.JButton();
        btnmodificar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaforma = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        buscar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cheque");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setNextFocusableComponent(txtdescuento);

        txtdescuento.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdescuento.setForeground(new java.awt.Color(0, 102, 204));
        txtdescuento.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtdescuento.setNextFocusableComponent(btnmodificar);
        txtdescuento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdescuentoKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Descuento");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("%");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtdescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addComponent(txtdescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))))
        );

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setMnemonic('c');
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setNextFocusableComponent(txtnombre);
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnmodificar.setMnemonic('m');
        btnmodificar.setText("Aceptar");
        btnmodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setNextFocusableComponent(btncancelar);
        btnmodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaforma.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaforma.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaforma.setNextFocusableComponent(txtnombre);
        tablaforma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaformaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaforma);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.setNextFocusableComponent(tablaforma);
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 48, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        if (mod == 0) {
            this.dispose();
        } else {
            txtdescuento.setText("");
            txtnombre.setText("");
            mod = 0;
        }
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        ConexionMySQL MySQL = new ConexionMySQL();
        Connection cn = MySQL.Conectar();
        String nom;
        Double descuento;
        int agrega = 0;
        String sSQL = "";
        String SQL = "";
        if ("".equals(txtnombre.getText()) || "".equals(txtdescuento.getText())) {
            JOptionPane.showMessageDialog(null, "No ingreso ningun dato");
        } else {
            nom = txtnombre.getText();
            descuento = Double.valueOf(txtdescuento.getText());

            if (mod == 0) {
                String sql = "SELECT descripcion FROM formasdepago_cheque";
                try {
                    Statement stt = cn.createStatement();
                    ResultSet rsr = stt.executeQuery(sql);
                    int contador = 0; //inicio
                    contador = rsr.getRow();
                    if (contador != 0) {
                        while (rsr.next()) {
                            if (nom.equals(rsr.getString("descripcion"))) {
                                JOptionPane.showMessageDialog(null, "El cheque ya esta en la base de datos");
                                agrega = 0;
                                break;
                            } else {
                                agrega = 1;
                            }
                        }
                    } else {
                        agrega = 1;
                    }
                    if (agrega == 1) {
                        sSQL = "INSERT INTO formasdepago_cheque(descripcion,descuento)"
                                + "VALUES(?,?)";
                        try {
                            PreparedStatement pst = cn.prepareStatement(sSQL);
                            pst.setString(1, nom);
                            pst.setDouble(2, descuento);
                            int n = pst.executeUpdate();
                            if (n > 0) {
                                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                                mod = 0;
                            }
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                    cargartabla("");
                    txtnombre.setText("");
                    txtdescuento.setText("");
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            } else {
                try {
                    int id = Integer.valueOf(tablaforma.getValueAt(tablaforma.getSelectedRow(), 0).toString());
                    SQL = "UPDATE formasdepago_cheque SET  "
                            + "descuento=? "
                            + "WHERE idFormaPago_Cheque=" + id;
                    PreparedStatement pstt = cn.prepareStatement(SQL);
                    pstt.setDouble(1, descuento);
                    int n = pstt.executeUpdate();
                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
                        cargartabla("");
                        txtdescuento.setText("");
                        txtnombre.setText("");
                        mod = 0;
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    JOptionPane.showMessageDialog(null, e);
                }
            }
        }
        
        
        
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void tablaformaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaformaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            evt.consume();
            tablaforma.transferFocus();
        }
    }//GEN-LAST:event_tablaformaKeyPressed

    private void txtdescuentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdescuentoKeyReleased
        fnesNumerico Clase = new fnesNumerico();
        if (!Clase.isNumeric(txtdescuento.getText())) {
            txtdescuento.setText("");
        }
    }//GEN-LAST:event_txtdescuentoKeyReleased

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed

        cargartabla(txtbuscar.getText());
        
    }//GEN-LAST:event_txtbuscarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JLabel buscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaforma;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtdescuento;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
