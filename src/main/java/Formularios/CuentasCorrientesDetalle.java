package Formularios;

import Clases.ClaseCuentaCorriente;
import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnEscape;
import Clases.fnExportar;
import Clases.fnRedondear;
import Clases.fnReversa;
import static Formularios.CuentasCorrientes.id_cliente;
import static Formularios.CuentasCorrientes.cliente;
import static Formularios.PedidosDetalle.idPedidos;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.*;
import javax.swing.JOptionPane;

import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CuentasCorrientesDetalle extends javax.swing.JDialog {

    DefaultTableModel model;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatoSimple = new SimpleDateFormat("dd-MM-yyyy");
    String fecha;

    public CuentasCorrientesDetalle(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        txtCliente.setText(cliente);
        cargartablaCliente(id_cliente);
        cargartotales();
        dobleclick();
    }

    void dobleclick() {
        tablactacte.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablactacte.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        if (tablactacte.getValueAt(tablactacte.getSelectedRow(), 5) != null) {
                            idPedidos = tablactacte.getValueAt(tablactacte.getSelectedRow(), 5).toString();
                            new PedidosDetalle(null, true).setVisible(true);
                        }
                    }
                }

            }
        });
    }

    void cargartablaCliente(int Valor) {
        String[] Titulo = {"Descripcion", "Debe", "Haber", "Fecha", "Detalle", "Nro Pedido"};
        String[] Registros = new String[6];
        String sql = "SELECT * FROM vista_ctacte_detalle WHERE idClientes = " + Valor;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                model.addRow(Registros);
            }
            tablactacte.setModel(model);
            tablactacte.setAutoCreateRowSorter(true);
            tablactacte.getColumnModel().getColumn(0).setPreferredWidth(170);
            tablactacte.getColumnModel().getColumn(1).setPreferredWidth(150);
            tablactacte.getColumnModel().getColumn(2).setPreferredWidth(150);
            tablactacte.getColumnModel().getColumn(3).setPreferredWidth(75);
            tablactacte.getColumnModel().getColumn(4).setPreferredWidth(230);
            tablactacte.getColumnModel().getColumn(4).setPreferredWidth(150);
            fnAlinear alinear = new fnAlinear();
            tablactacte.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearIzquierda());
            tablactacte.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
            tablactacte.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearCentro());
            tablactacte.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            tablactacte.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablactacte.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearCentro());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargartotales() {
        double totaldebe = 0.00, sumatoria, totalhaber = 0.00, sumatoria2;
        //AQUI SE SUMAN LOS VALORES DE LA COLUMNAS
        int totalRow = tablactacte.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {

            String debe = tablactacte.getValueAt(i, 1).toString();
            String haber = tablactacte.getValueAt(i, 2).toString();

            sumatoria = Double.valueOf(debe).doubleValue();
            sumatoria2 = Double.valueOf(haber).doubleValue();

            totaldebe = totaldebe + sumatoria;
            totalhaber = totalhaber + sumatoria2;

        }
        fnRedondear redondear = new fnRedondear();
        txtSaldo.setText(String.valueOf(redondear.dosDigitos(totalhaber - totaldebe)));

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnsalir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablactacte = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        fechafinal = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        fechainicial = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        btnfiltrar = new javax.swing.JButton();
        txtSaldo = new javax.swing.JTextField();
        btnexportar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        tablactacte.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablactacte);

        jLabel1.setText("Cliente:");

        txtCliente.setEditable(false);
        txtCliente.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("SALDO:     $");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        fechafinal.setDateFormatString("dd-MM-yyyy");
        fechafinal.setMaximumSize(new java.awt.Dimension(120, 50));
        fechafinal.setMinimumSize(new java.awt.Dimension(120, 50));
        fechafinal.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel3.setText("Desde:");

        fechainicial.setDateFormatString("dd-MM-yyyy");
        fechainicial.setMaximumSize(new java.awt.Dimension(120, 50));
        fechainicial.setMinimumSize(new java.awt.Dimension(120, 50));
        fechainicial.setPreferredSize(new java.awt.Dimension(120, 50));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel4.setText("Hasta:");

        btnfiltrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnfiltrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnfiltrar.setMnemonic('f');
        btnfiltrar.setText("Filtrar");
        btnfiltrar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnfiltrar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnfiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnfiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnfiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        txtSaldo.setEditable(false);
        txtSaldo.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtSaldo.setForeground(new java.awt.Color(0, 102, 102));

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setMnemonic('s');
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btnImprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Imprimir.png"))); // NOI18N
        btnImprimir.setMnemonic('s');
        btnImprimir.setText("Imprimir");
        btnImprimir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnImprimir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnImprimir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(512, 512, 512)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(txtSaldo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        this.dispose();

    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnfiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfiltrarActionPerformed
        String[] Titulo = {"Descripcion", "Debe", "Haber", "Fecha", "Detalle", "Nro Pedido"};
        String[] Registros = new String[6];
        String sql = "SELECT * FROM vista_ctacte_detalle WHERE idClientes = " + id_cliente;
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Date fechinicial = fechainicial.getDate();
        Statement Selectpedidos = null;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = fechafinal.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    if ((fecha.compareTo(ffinal) >= 0) && (fecha.compareTo(finicial) >= 0)) {
                        model = new DefaultTableModel(null, Titulo) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        try {
                            Selectpedidos = cn.createStatement();
                            ResultSet rs = Selectpedidos.executeQuery(sql);
                            while (rs.next()) {
                                fnReversa r = new fnReversa();
                                if ((r.reverse(rs.getString("fecha")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fecha")).compareTo(finicial) >= 0)) {
                                    Registros[0] = rs.getString(1);
                                    Registros[1] = rs.getString(2);
                                    Registros[2] = rs.getString(3);
                                    Registros[3] = rs.getString(4);
                                    Registros[4] = rs.getString(5);
                                    Registros[5] = rs.getString(6);

                                    model.addRow(Registros);
                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (Selectpedidos != null) {
                                    Selectpedidos.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        tablactacte.setModel(model);
                        tablactacte.setAutoCreateRowSorter(true);

                        tablactacte.getColumnModel().getColumn(1).setPreferredWidth(200);
                        fnAlinear alinear = new fnAlinear();
                        tablactacte.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearIzquierda());
                        tablactacte.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
                        tablactacte.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearCentro());
                        tablactacte.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
                        tablactacte.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
                        tablactacte.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearCentro());

                        cargartotales();
                    } else {
                        JOptionPane.showMessageDialog(null, "Se seleccionó fuera de rango de la fecha actual ");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                fechainicial.setDate(null);
                fechafinal.setDate(null);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ingreso fecha inicial");
            fechainicial.setDate(null);
            fechafinal.setDate(null);
        }


    }//GEN-LAST:event_btnfiltrarActionPerformed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablactacte.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablactacte);
            nom.add("Tabla Cta Cte");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        ClaseCuentaCorriente cc = new ClaseCuentaCorriente();        
        Date fechinicial = fechainicial.getDate();
        if (fechinicial != null) {
            String finicial = formato.format(fechinicial.getTime());
            Date fechfinal = fechafinal.getDate();
            if (fechfinal != null) {
                String ffinal = formato.format(fechfinal.getTime());
                if (finicial.compareTo(ffinal) <= 0) {
                    cc.ImprimirEstadoFecha(id_cliente, finicial, ffinal);
                } else {
                    JOptionPane.showMessageDialog(null, "La fecha inicial es mayor que la final");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No se ingreso fecha final");
                fechainicial.setDate(null);
                fechafinal.setDate(null);
            }
        } else {
            cc.ImprimirEstado(id_cliente);
        }       
    }//GEN-LAST:event_btnImprimirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnfiltrar;
    private javax.swing.JButton btnsalir;
    private com.toedter.calendar.JDateChooser fechafinal;
    private com.toedter.calendar.JDateChooser fechainicial;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablactacte;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtSaldo;
    // End of variables declaration//GEN-END:variables
}
