package Clases;

public class cboProveedor {
    
  
    private int idProveedores;
    private String razonsocial;
    
    public cboProveedor(){}
    
    public cboProveedor(int idProveedores, String razonsocial){
        this.idProveedores = idProveedores;
        this.razonsocial = razonsocial;
    }

    public int getidProveedores() {
        return idProveedores;
    }

    public void setidProveedores(int idProveedores) {
        this.idProveedores = idProveedores;
    }

    public String getrazonsocial() {
        return razonsocial;
    }

    public void setdrazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }
    
    public String toString(){
        return this.razonsocial;
    }
    
}
