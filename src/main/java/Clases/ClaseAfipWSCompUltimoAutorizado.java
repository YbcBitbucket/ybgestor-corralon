package Clases;

import WSFE_HOMOLOGACION.FERecuperaLastCbteResponse;
import WebServices.ServidorWsaa;
import WebServices.ServidorWsfev;
import WebServices.ServidorWsfevHomologacion;


public class ClaseAfipWSCompUltimoAutorizado {
    
    public String CompUltimoAutorizado(int PtoVta, int CbteTipo) {
        String ComprobanteNro = null;
        ClaseAfipEstado Afip = new ClaseAfipEstado();
        //Autorizacion
        ServidorWsaa autorizacion = new ServidorWsaa();
        if (Afip.Estado().equals("PRODUCCION")) {
            WSFE.FERecuperaLastCbteResponse Ultimo = ServidorWsfev.feCompUltimoAutorizado(autorizacion.ObtenerAutorizacion(), PtoVta, CbteTipo);
            ComprobanteNro = String.valueOf(Ultimo.getCbteNro());
        }
        
        if (Afip.Estado().equals("HOMOLOGACION")) {
            FERecuperaLastCbteResponse Ultimo = ServidorWsfevHomologacion.feParamGetUltimoAutorizado(autorizacion.ObtenerAutorizacionHomologacion(), PtoVta, CbteTipo);
            ComprobanteNro = String.valueOf(Ultimo.getCbteNro());
            
        }
        return ComprobanteNro;
    }

}
