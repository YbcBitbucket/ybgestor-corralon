package Clases;

import static Formularios.Login.id_usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ClasePedidos {

    public ClasePedidos() {
    }

    public int AgregarPedido(String[][] datos, String Fecha, String Hora, Double Total, Double Descuento, int IdCliente, int IdDeposito, int FormadePago, Double DescuentoBase, Double DescuentoPorcentaje, Double Subtotal, int idCaja, Double Interes, Double InteresPorcentaje, String descripcion, int id_usuario, int idTipo) {
        int IdPedidos = 0;
        /////   Conexion Mysql
        System.out.println("String[][] datos" + datos);
        System.out.println("String Fecha " + Fecha);
        System.out.println("String Hora " + Hora);
        System.out.println("Double Total " + Total);
        System.out.println("Double Descuento " + Descuento);
        System.out.println("int IdCliente " + IdCliente);
        System.out.println("int IdDeposito" + IdDeposito);
        System.out.println("int FormadePago" + FormadePago);
        System.out.println("Double DescuentoBase " + DescuentoBase);
        System.out.println("Double DescuentoPorcentaje " + DescuentoPorcentaje);
        System.out.println("Double Subtotal " + Subtotal);
        System.out.println("int idCaja " + idCaja);
        System.out.println("Double interes " + Interes);
        System.out.println("Double InteresPorcentaje " + InteresPorcentaje);
        System.out.println("Double idTipo " + idTipo);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertPedido = null;
        Statement UltimoIdPedido = null;
        PreparedStatement InsertStock = null;
        Statement UltimoIdStock = null;
        PreparedStatement InsertDetallePedido = null;
        PreparedStatement Insertformadepago = null;
        try {
            String SQLInsertPedido = "INSERT INTO pedidos (total, fecha, hora, idClientes, subtotal, idCaja,observacion,id_usuario) "
                    + "VALUES (?,?,?,?,?,?,?,?)";

            cn.setAutoCommit(false); //transaction block start
            //Inserto Pedidos
            InsertPedido = cn.prepareStatement(SQLInsertPedido);
            InsertPedido.setDouble(1, Total);
            InsertPedido.setString(2, Fecha);
            InsertPedido.setString(3, Hora);
            InsertPedido.setInt(4, IdCliente);
            InsertPedido.setDouble(5, Subtotal);
            InsertPedido.setInt(6, idCaja);
            InsertPedido.setString(7, descripcion);
            InsertPedido.setInt(8, id_usuario);
            InsertPedido.executeUpdate();
            //Busco Ultimo ID Pedidos
            UltimoIdPedido = cn.createStatement();
            ResultSet rs = UltimoIdPedido.executeQuery("SELECT MAX(idPedidos) AS idPedidos FROM pedidos");
            rs.next();
            IdPedidos = rs.getInt("idPedidos");
            //Cargo Stock y Detalle Pedido
            String SQLInsertStock = "INSERT INTO stock (idProductos, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion,id_usuario) "
                    + "VALUES (?,?,?,?,?,?,?,?,?)";
            String SQLInsertDetallePedido = "INSERT INTO detalledepedidos (idPedidos, idStock, cantidad) "
                    + "VALUES (?,?,?)";
            InsertStock = cn.prepareStatement(SQLInsertStock);
            InsertDetallePedido = cn.prepareStatement(SQLInsertDetallePedido);

            for (int i = 0; i < datos.length; i++) {
                //Inserto Stock
                InsertStock.setInt(1, Integer.valueOf(datos[i][0]));
                InsertStock.setString(2, datos[i][3]);
                InsertStock.setDouble(3, Double.valueOf(datos[i][4]));
                InsertStock.setDouble(4, -Double.valueOf(datos[i][7]));
                InsertStock.setString(5, Fecha);
                InsertStock.setInt(6, IdDeposito);
                InsertStock.setInt(7, Integer.valueOf(datos[i][5]));
                InsertStock.setString(8, "PEDIDO Nro " + IdPedidos);
                InsertStock.setInt(9, id_usuario);
                InsertStock.executeUpdate();
                // Ultimo idStock
                UltimoIdStock = cn.createStatement();
                ResultSet rsStock = UltimoIdStock.executeQuery("SELECT MAX(idStock) AS idStock FROM stock");
                rsStock.next();
                int idStock = rsStock.getInt("idStock");
                //Insero Detalle Pedido
                InsertDetallePedido.setInt(1, IdPedidos);
                InsertDetallePedido.setDouble(2, idStock);
                InsertDetallePedido.setDouble(3, Double.valueOf(datos[i][7]));
                InsertDetallePedido.executeUpdate();
            }
            //FormadePago
            String SQLInsertFormadePago = "INSERT INTO pedidos_tienen_formasdepago (idFormasDePago, idPedidos, descuento, descuentobase, descuentoporcentaje, interes,interesporcentaje,idTipo) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            Insertformadepago = cn.prepareStatement(SQLInsertFormadePago);
            Insertformadepago.setInt(1, FormadePago);
            Insertformadepago.setInt(2, IdPedidos);
            Insertformadepago.setDouble(3, Descuento);
            Insertformadepago.setDouble(4, DescuentoBase);
            Insertformadepago.setDouble(5, DescuentoPorcentaje);
            Insertformadepago.setDouble(6, Interes);
            Insertformadepago.setDouble(7, InteresPorcentaje);
            Insertformadepago.setInt(8, idTipo);
            Insertformadepago.executeUpdate();
            /////////
            cn.commit(); //transaction block end

            JOptionPane.showMessageDialog(null, "El pedido se realizo con exito...");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (InsertPedido != null) {
                    InsertPedido.close();
                }
                if (UltimoIdPedido != null) {
                    UltimoIdPedido.close();
                }
                if (InsertStock != null) {
                    InsertStock.close();
                }
                if (UltimoIdStock != null) {
                    UltimoIdStock.close();
                }
                if (InsertDetallePedido != null) {
                    InsertDetallePedido.close();
                }
                if (Insertformadepago != null) {
                    Insertformadepago.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return IdPedidos;
    }

    public int AgregarPedidoSolo(String[][] datos, String Fecha, String Hora, Double Total, Double Descuento, int IdCliente, int IdDeposito, int FormadePago, Double DescuentoBase, Double DescuentoPorcentaje, Double Subtotal, int idCaja, Double Interes, Double InteresPorcentaje) {
        int IdPedidos = 0;
        /////   Conexion Mysql
        System.out.println("String[][] datos" + datos);
        System.out.println("String Fecha " + Fecha);
        System.out.println("String Hora " + Hora);
        System.out.println("Double Total " + Total);
        System.out.println("Double Descuento " + Descuento);
        System.out.println("int IdCliente " + IdCliente);
        System.out.println("int IdDeposito" + IdDeposito);
        System.out.println("int FormadePago" + FormadePago);
        System.out.println("Double DescuentoBase " + DescuentoBase);
        System.out.println("Double DescuentoPorcentaje " + DescuentoPorcentaje);
        System.out.println("Double Subtotal " + Subtotal);
        System.out.println("int idCaja " + idCaja);
        System.out.println("Double interes " + Interes);
        System.out.println("Double InteresPorcentaje " + InteresPorcentaje);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertPedido = null;
        Statement UltimoIdPedido = null;
        PreparedStatement InsertStock = null;
        Statement UltimoIdStock = null;
        PreparedStatement InsertDetallePedido = null;
        try {
            String SQLInsertPedido = "INSERT INTO pedidos (total, fecha, hora, idClientes, subtotal, idCaja) "
                    + "VALUES (?,?,?,?,?,?)";

            cn.setAutoCommit(false); //transaction block start
            //Inserto Pedidos
            InsertPedido = cn.prepareStatement(SQLInsertPedido);
            InsertPedido.setDouble(1, Total);
            InsertPedido.setString(2, Fecha);
            InsertPedido.setString(3, Hora);
            InsertPedido.setInt(4, IdCliente);
            InsertPedido.setDouble(5, Subtotal);
            InsertPedido.setInt(6, idCaja);
            InsertPedido.executeUpdate();
            //Busco Ultimo ID Pedidos
            UltimoIdPedido = cn.createStatement();
            ResultSet rs = UltimoIdPedido.executeQuery("SELECT MAX(idPedidos) AS idPedidos FROM pedidos");
            rs.next();
            IdPedidos = rs.getInt("idPedidos");
            //Cargo Stock y Detalle Pedido
            String SQLInsertStock = "INSERT INTO stock (idProductos, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            String SQLInsertDetallePedido = "INSERT INTO detalledepedidos (idPedidos, idStock, cantidad) "
                    + "VALUES (?,?,?)";
            InsertStock = cn.prepareStatement(SQLInsertStock);
            InsertDetallePedido = cn.prepareStatement(SQLInsertDetallePedido);

            for (int i = 0; i < datos.length; i++) {
                //Inserto Stock
                InsertStock.setInt(1, Integer.valueOf(datos[i][0]));
                InsertStock.setString(2, datos[i][3]);
                InsertStock.setDouble(3, Double.valueOf(datos[i][4]));
                InsertStock.setDouble(4, -Double.valueOf(datos[i][7]));
                InsertStock.setString(5, Fecha);
                InsertStock.setInt(6, IdDeposito);
                InsertStock.setInt(7, Integer.valueOf(datos[i][5]));
                InsertStock.setString(8, "PEDIDO Nro " + IdPedidos);
                InsertStock.executeUpdate();
                // Ultimo idStock
                UltimoIdStock = cn.createStatement();
                ResultSet rsStock = UltimoIdStock.executeQuery("SELECT MAX(idStock) AS idStock FROM stock");
                rsStock.next();
                int idStock = rsStock.getInt("idStock");
                //Insero Detalle Pedido
                InsertDetallePedido.setInt(1, IdPedidos);
                InsertDetallePedido.setDouble(2, idStock);
                InsertDetallePedido.setDouble(3, Double.valueOf(datos[i][7]));
                InsertDetallePedido.executeUpdate();
            }
            /////////
            cn.commit(); //transaction block end

            JOptionPane.showMessageDialog(null, "El pedido se realizo con exito...");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (InsertPedido != null) {
                    InsertPedido.close();
                }
                if (UltimoIdPedido != null) {
                    UltimoIdPedido.close();
                }
                if (InsertStock != null) {
                    InsertStock.close();
                }
                if (UltimoIdStock != null) {
                    UltimoIdStock.close();
                }
                if (InsertDetallePedido != null) {
                    InsertDetallePedido.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return IdPedidos;
    }

    public int ModificarPedido(int IdPedidos, String[][] datos, String Fecha, String Hora, Double Total, Double Descuento, int IdCliente, int IdDeposito, int FormadePago, Double DescuentoBase, Double DescuentoPorcentaje, Double Subtotal, int idCaja, Double Interes, Double InteresPorcentaje, String descripcion, int id_usuario) {

        /////   Conexion Mysql
        System.out.println("String[][] datos" + datos);
        System.out.println("String Fecha " + Fecha);
        System.out.println("String Hora " + Hora);
        System.out.println("Double Total " + Total);
        System.out.println("Double Descuento " + Descuento);
        System.out.println("int IdCliente " + IdCliente);
        System.out.println("int IdDeposito" + IdDeposito);
        System.out.println("int FormadePago" + FormadePago);
        System.out.println("Double DescuentoBase " + DescuentoBase);
        System.out.println("Double DescuentoPorcentaje " + DescuentoPorcentaje);
        System.out.println("Double Subtotal " + Subtotal);
        System.out.println("int idCaja " + idCaja);
        System.out.println("Double interes " + Interes);
        System.out.println("Double InteresPorcentaje " + InteresPorcentaje);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarPedidos = null;
        Statement UltimoIdPedido = null;
        PreparedStatement InsertStock = null;
        Statement UltimoIdStock = null;
        PreparedStatement InsertDetallePedido = null;
        PreparedStatement Updateformadepago = null;
        try {
            String SQLUpdatePedido = "UPDATE pedidos set total=?, fecha=?, hora=?, idClientes=?, subtotal=?, idCaja=?,observacion=?,id_usuario=? "
                    + "where idpedidos=" + IdPedidos;

            cn.setAutoCommit(false); //transaction block start
            //Inserto Pedidos
            ModificarPedidos = cn.prepareStatement(SQLUpdatePedido);
            ModificarPedidos.setDouble(1, Total);
            ModificarPedidos.setString(2, Fecha);
            ModificarPedidos.setString(3, Hora);
            ModificarPedidos.setInt(4, IdCliente);
            ModificarPedidos.setDouble(5, Subtotal);
            ModificarPedidos.setInt(6, idCaja);
            ModificarPedidos.setString(7, descripcion);
            ModificarPedidos.setInt(8, id_usuario);
            ModificarPedidos.executeUpdate();
            //Borro productos de la tabla detalle pedido
            try {
                PreparedStatement pst3 = cn.prepareStatement("DELETE FROM detalledepedidos WHERE idPedidos =" + IdPedidos);
                pst3.executeUpdate();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos.(e317)");
            }
            //Borro productos de la tabla stock ID Pedidos
            try {
                PreparedStatement pst3 = cn.prepareStatement("DELETE FROM stock WHERE descripcion = 'PEDIDO Nro " + IdPedidos+"'");
                pst3.executeUpdate();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e + "/n Error en la base de datos.(e324)");
            }
            
            //Cargo Stock y Detalle Pedido
            String SQLInsertStock = "INSERT INTO stock (idProductos, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion,id_usuario) "
                    + "VALUES (?,?,?,?,?,?,?,?,?)";
            String SQLInsertDetallePedido = "INSERT INTO detalledepedidos (idPedidos, idStock, cantidad) "
                    + "VALUES (?,?,?)";
            InsertStock = cn.prepareStatement(SQLInsertStock);
            InsertDetallePedido = cn.prepareStatement(SQLInsertDetallePedido);

            for (int i = 0; i < datos.length; i++) {
                //Inserto Stock
                InsertStock.setInt(1, Integer.valueOf(datos[i][0]));
                InsertStock.setString(2, datos[i][3]);
                InsertStock.setDouble(3, Double.valueOf(datos[i][4]));
                InsertStock.setDouble(4, -Double.valueOf(datos[i][7]));
                InsertStock.setString(5, Fecha);
                InsertStock.setInt(6, IdDeposito);
                InsertStock.setInt(7, Integer.valueOf(datos[i][5]));
                InsertStock.setString(8, "PEDIDO Nro " + IdPedidos);
                InsertStock.setInt(9, id_usuario);
                InsertStock.executeUpdate();
                // Ultimo idStock
                UltimoIdStock = cn.createStatement();
                ResultSet rsStock = UltimoIdStock.executeQuery("SELECT MAX(idStock) AS idStock FROM stock");
                rsStock.next();
                int idStock = rsStock.getInt("idStock");
                //Insero Detalle Pedido
                InsertDetallePedido.setInt(1, IdPedidos);
                InsertDetallePedido.setDouble(2, idStock);
                InsertDetallePedido.setDouble(3, Double.valueOf(datos[i][7]));
                InsertDetallePedido.executeUpdate();
            }
            //FormadePago
//            String SQLUpdateFormadePago = "INSERT INTO pedidos_tienen_formasdepago (idFormasDePago, idPedidos, descuento, descuentobase, descuentoporcentaje, interes,interesporcentaje) "
//                    + "VALUES (?,?,?,?,?,?,?)";

            String SQLUpdatePedidoFormadePago = "UPDATE pedidos_tienen_formasdepago set idFormasDePago=?, descuento=?, descuentobase=?, descuentoporcentaje=?, interes=?, interesporcentaje=? "
                    + "where idpedidos=" + IdPedidos;
            
            Updateformadepago = cn.prepareStatement(SQLUpdatePedidoFormadePago);
            Updateformadepago.setInt(1, FormadePago);
            Updateformadepago.setDouble(2, Descuento);
            Updateformadepago.setDouble(3, DescuentoBase);
            Updateformadepago.setDouble(4, DescuentoPorcentaje);
            Updateformadepago.setDouble(5, Interes);
            Updateformadepago.setDouble(6, InteresPorcentaje);
            Updateformadepago.executeUpdate();
            /////////

            cn.commit(); //transaction block end

            JOptionPane.showMessageDialog(null, "El pedido se modificó con exito...");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e+ "/n Error en la base de datos.(e379)");
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex + "/n Error en la base de datos.(e383)");
            }
        } finally {
            try {
                if (ModificarPedidos != null) {
                    ModificarPedidos.close();
                }
                if (UltimoIdPedido != null) {
                    UltimoIdPedido.close();
                }
                if (InsertStock != null) {
                    InsertStock.close();
                }
                if (UltimoIdStock != null) {
                    UltimoIdStock.close();
                }
                if (InsertDetallePedido != null) {
                    InsertDetallePedido.close();
                }
                if (Updateformadepago != null) {
                    Updateformadepago.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex+ "/n Error en la base de datos.(e409)");
            }
        }
        return IdPedidos;
    }

    public void AgregaFormaDePago(int IdPedidos, Double Descuento, int FormadePago, Double DescuentoBase, Double DescuentoPorcentaje, Double Interes, Double InteresPorcentaje, Double Subtotal) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement Insertformadepago = null;
        //FormadePago
        try {
            String SQLInsertFormadePago = "INSERT INTO pedidos_tienen_formasdepago (idFormasDePago, idPedidos, descuento, descuentobase, descuentoporcentaje, interes,interesporcentaje,subtotal) "
                    + "VALUES (?,?,?,?,?,?,?,?)";
            Insertformadepago = cn.prepareStatement(SQLInsertFormadePago);
            Insertformadepago.setInt(1, FormadePago);
            Insertformadepago.setInt(2, IdPedidos);
            Insertformadepago.setDouble(3, Descuento);
            Insertformadepago.setDouble(4, DescuentoBase);
            Insertformadepago.setDouble(5, DescuentoPorcentaje);
            Insertformadepago.setDouble(6, Interes);
            Insertformadepago.setDouble(7, InteresPorcentaje);
            Insertformadepago.setDouble(8, Subtotal);
            Insertformadepago.executeUpdate();
            /////////
            cn.commit(); //transaction block end

            JOptionPane.showMessageDialog(null, "El pedido se realizo con exito...");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (Insertformadepago != null) {
                    Insertformadepago.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public boolean FacturarPedido(int IdCliente, int IdPedidos, int idPuntosdeVentas, int id_usuarios) {

        boolean facturacion = false;
        String facturacion_usuario = "SELECT * FROM vista_facturacion_usuario";
        String facturacion_cliente = "SELECT * FROM vista_facturacion_cliente WHERE idClientes=" + IdCliente;
        String pedidos_detalle = "SELECT * FROM vista_pedidos_detalle WHERE idPedidos=" + IdPedidos;

        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectfacturacion_usuario = null;
        Statement Selectfacturacion_cliente = null;
        Statement Selectpedidos_detalle = null;
        Statement ContarPedido = null;
        PreparedStatement Insertfacturacion = null;
        PreparedStatement Updatepedidos = null;

        //Datos Usuario
        int Usuario_idTiporesponsable = 0;
        String Usuario_direccionip = "";

        //Datos Cliente
        int Cli_idTiporesponsable = 0;
        String Cli_AfipTipo = "";
        String Cli_RazonSocial = "";
        String Cli_Documento = "";
        int Cli_tipodocWSFE = 0;
        int Cli_tipodocHASAR = 0;
        int Cli_tiporesponsableWSFE = 0;
        int Cli_tiporesponsableHASAR = 0;
        String Cli_Domicilio = "";

        try {
            Selectfacturacion_usuario = cn.createStatement();
            ResultSet rs_usuario = Selectfacturacion_usuario.executeQuery(facturacion_usuario);
            rs_usuario.next();
            Usuario_idTiporesponsable = rs_usuario.getInt("idTiporesponsable");
            Usuario_direccionip = rs_usuario.getString("direccionip");

            Selectfacturacion_cliente = cn.createStatement();
            ResultSet rs_cliente = Selectfacturacion_cliente.executeQuery(facturacion_cliente);
            rs_cliente.next();
            Cli_idTiporesponsable = rs_cliente.getInt("idTiporesponsable");
            Cli_AfipTipo = rs_cliente.getString("AfipTipo");
            Cli_RazonSocial = rs_cliente.getString("RazonSocial");
            Cli_Documento = rs_cliente.getString("Documento");
            Cli_tipodocWSFE = rs_cliente.getInt("tipodocWSFE");
            Cli_tipodocHASAR = rs_cliente.getInt("tipodocHASAR");
            Cli_tiporesponsableWSFE = rs_cliente.getInt("tiporesponsableWSFE");
            Cli_tiporesponsableHASAR = rs_cliente.getInt("tiporesponsableHASAR");
            Cli_Domicilio = rs_cliente.getString("Domicilio");

            /////Monotributo == 6 - FACTUA O TIQUE C
            if (Usuario_idTiporesponsable == 6) {

                if (Cli_AfipTipo.equals("WSFE")) {
                    //Usar datos WSFE - Factura C
                }

            }

            /////Responsable Inscripto == 1 - FACTUA O TIQUE A B
            if (Usuario_idTiporesponsable == 1) {

                ///Cliente RI
                if (Cli_idTiporesponsable == 1) {
                    if (Cli_AfipTipo.equals("WSFE")) {
                        //Usar datos WSFE - Factura A

                    }

                } ///Cliente Consumidor Final o Otro
                else {
                    if (Cli_AfipTipo.equals("WSFE")) {
                        //Usar datos WSFE - Factura B

                    }

                }
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
        }
        return facturacion;
    }

    public void ImprimirPedido(int IdPedidos) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;

        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Pedido", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reporte_Venta.jasper"));
            Map parametros = new HashMap();
            parametros.put("id_pedido", String.valueOf(IdPedidos));
            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

            //JasperPrintManager.printReport(jPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "El pedido no se pudo imprimir");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    public void AnularPedido(int IdPedidos) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDetallePedido = null;
        Statement SelectStock = null;
        PreparedStatement InsertStock = null;
        PreparedStatement Updatepedidos = null;
        fnCargarFecha fecha = new fnCargarFecha();
        try {
            cn.setAutoCommit(false); //transaction block start
            //Selecciones detalle Pedidos el idProducto y el idDeposito
            String SelectDetallePedidoSQL = "SELECT idProductos, idDepositos, cantidad FROM vista_pedidos_detalle WHERE idPedidos=" + IdPedidos;
            SelectDetallePedido = cn.createStatement();
            ResultSet rsSelectDetallePedido = SelectDetallePedido.executeQuery(SelectDetallePedidoSQL);

            while (rsSelectDetallePedido.next()) {
                //Selecciono los datos del stock
                String SelectStockSQL = "SELECT stock.bonificacion, stock.precioVenta, stock.idTipoiva "
                        + "FROM stock WHERE stock.idProductos =" + rsSelectDetallePedido.getInt(1) + " "
                        + "AND stock.idDepositos =" + rsSelectDetallePedido.getInt(2) + " ORDER BY stock.idStock DESC";
                SelectStock = cn.createStatement();
                ResultSet rsSelectStock = SelectStock.executeQuery(SelectStockSQL);
                rsSelectStock.next();

                //Insertamos Stock
                String InsertStockSQL = "INSERT INTO stock (idProductos, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion,id_usuario) "
                        + "VALUES (?,?,?,?,?,?,?,?,?)";
                InsertStock = cn.prepareStatement(InsertStockSQL);
                InsertStock.setInt(1, rsSelectDetallePedido.getInt(1));
                InsertStock.setDouble(2, rsSelectStock.getDouble(2));
                InsertStock.setDouble(3, rsSelectStock.getDouble(1));
                InsertStock.setDouble(4, rsSelectDetallePedido.getDouble(3));
                InsertStock.setString(5, fecha.cargarfecha());
                InsertStock.setInt(6, rsSelectDetallePedido.getInt(2));
                InsertStock.setInt(7, rsSelectStock.getInt(3));
                InsertStock.setString(8, "ANULACION DE PEDIDO Nro " + IdPedidos);
                InsertStock.setInt(9, id_usuario);
                InsertStock.executeUpdate();

            }

            String SQLUpdatePedidos = "UPDATE pedidos SET estado=? WHERE idPedidos=" + IdPedidos;
            Updatepedidos = cn.prepareStatement(SQLUpdatePedidos);
            Updatepedidos.setString(1, "ANULADO");
            Updatepedidos.executeUpdate();

            cn.commit(); //transaction block end

            JOptionPane.showMessageDialog(null, "El pedido se ANULO con exito...");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (SelectDetallePedido != null) {
                    SelectDetallePedido.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (InsertStock != null) {
                    InsertStock.close();
                }
                if (Updatepedidos != null) {
                    Updatepedidos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public int ComprueboCatCte(int IdCliente, double total) {
        int idCtaCte = 0;

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCtaCte = null;
        try {
            String SelectCtaCteSQL = "SELECT estado, margen, saldo, idCuentaCorriente FROM vista_ctactes WHERE IdClientes=" + IdCliente;
            SelectCtaCte = cn.createStatement();
            ResultSet rsSelectCtaCte = SelectCtaCte.executeQuery(SelectCtaCteSQL);
            //Si Tiene Cta Cte
            if (rsSelectCtaCte.next()) {
                //En que estado se encuentra
                if (rsSelectCtaCte.getString(1).equals("HABILITADO")) {
                    //Si posee Margen Para la compra (Saldo*-1 + Venta < Margen)
                    if ((total + (rsSelectCtaCte.getDouble(3) * -1)) <= rsSelectCtaCte.getDouble(2)) {
                        idCtaCte = rsSelectCtaCte.getInt(4);

                    } else {
                        JOptionPane.showMessageDialog(null, "EL CLIENTE NO POSEE MARGEN");
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "EL CLIENTE SE ENCUENTRA DESHABILITADO");
                }
            } else {
                JOptionPane.showMessageDialog(null, "EL CLIENTE NO POSEE CUENTA CORRIENTE");
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (SelectCtaCte != null) {
                    SelectCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        return idCtaCte;
    }

    public void CompraCtaCte(int IdCtacte, int IdPedidos, double total) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertCtaCte = null;
        fnCargarFecha fecha = new fnCargarFecha();
        try {
            cn.setAutoCommit(false); //transaction block start

            //Insertamos Stock
            String InsertStockSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, idPedidos, descripcion, debe, fecha) "
                    + "VALUES (?,?,?,?,?)";
            InsertCtaCte = cn.prepareStatement(InsertStockSQL);
            InsertCtaCte.setInt(1, IdCtacte);
            InsertCtaCte.setInt(2, IdPedidos);
            InsertCtaCte.setString(3, "COMPRA CON CTA CTE");
            InsertCtaCte.setDouble(4, total);
            InsertCtaCte.setString(5, fecha.cargarfecha());

            InsertCtaCte.executeUpdate();

            cn.commit(); //transaction block end

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (InsertCtaCte != null) {
                    InsertCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

}
