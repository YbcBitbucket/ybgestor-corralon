package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ClaseStock {

    public ClaseStock() {
    }

    public int AgregarStock(double cantidad, String fechastock, String pcompra, String pventa, int idProducto, int idDeposito, int idTipoiva, int idProveedor, String nComprobante) {
        int stock=0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarStock = null;
        try {
            String sSQL = "INSERT INTO stock (cantidad, fecha, precioCompra, precioVenta, "
                    + "idProductos, idDepositos, idTipoiva, descripcion, idProveedores, numerocbte) VALUES(?,?,?,?,?,?,?,?,?,?)";
            AgregarStock = cn.prepareStatement(sSQL);
            AgregarStock.setDouble(1, cantidad);
            AgregarStock.setString(2, fechastock);
            AgregarStock.setString(3, pcompra);
            AgregarStock.setString(4, pventa);
            AgregarStock.setInt(5, idProducto);
            AgregarStock.setInt(6, idDeposito);
            AgregarStock.setInt(7, idTipoiva);
            AgregarStock.setString(8, "COMPRA DE PRODUCTO");
            AgregarStock.setInt(9, idProveedor);
            AgregarStock.setString(10, nComprobante);
            int n = AgregarStock.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            stock = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (AgregarStock != null) {
                    AgregarStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return stock;
    }

    
    
    public void ModificarBonificacion(int idProducto,String pcompra, String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva, int idProveedor, String pventa2) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            PreparedStatement ModificarBonificacion = null;
            
            try {
                String sSQL = "INSERT INTO stock (idProductos, precioCompra, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion, idProveedores, precioVenta2) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                ModificarBonificacion = cn.prepareStatement(sSQL);
                ModificarBonificacion.setInt(1, idProducto);
                ModificarBonificacion.setString(2, pcompra);
                ModificarBonificacion.setString(3, pventa);
                ModificarBonificacion.setString(4, bonificacion);
                ModificarBonificacion.setDouble(5, 0);
                ModificarBonificacion.setString(6, fechabonificacion);
                ModificarBonificacion.setInt(7, idDeposito);
                ModificarBonificacion.setInt(8, idTipoiva);
                ModificarBonificacion.setString(9, "MODIFICACION DE BONIFICACION");
                ModificarBonificacion.setInt(10, idProveedor);
                ModificarBonificacion.setString(11, pventa2);
                int n = ModificarBonificacion.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (ModificarBonificacion != null) {
                        ModificarBonificacion.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    public void ActualizacionPrecio(int idProducto, String pcompra,String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            PreparedStatement ActualizacionPrecio = null;
            
            try {
                String sSQL = "INSERT INTO stock (idProductos, precioCompra,precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion) "
                    + "VALUES (?,?,?,?,?,?,?,?,?)";
                ActualizacionPrecio = cn.prepareStatement(sSQL);
                ActualizacionPrecio.setInt(1, idProducto);
                ActualizacionPrecio.setString(2, pcompra);
                ActualizacionPrecio.setString(3, pventa);
                ActualizacionPrecio.setString(4, bonificacion);
                ActualizacionPrecio.setDouble(5, 0);
                ActualizacionPrecio.setString(6, fechabonificacion);
                ActualizacionPrecio.setInt(7, idDeposito);
                ActualizacionPrecio.setInt(8, idTipoiva);
                ActualizacionPrecio.setString(9, "ACTUALIZACION PRECIO");
                
                int n = ActualizacionPrecio.executeUpdate();
                if (n > 0) {
                    System.out.println("Los Datos se modificaron exitosamente... " + idProducto);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (ActualizacionPrecio != null) {
                        ActualizacionPrecio.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    
    public int ActualizacionPrecio(int idProducto, String pcompra, String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva, int idprov) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizacionPrecio = null;
        int stock = 0;
        try {
            String sSQL = "INSERT INTO stock (idProductos, precioCompra,precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion,idProveedores) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
            ActualizacionPrecio = cn.prepareStatement(sSQL);
            ActualizacionPrecio.setInt(1, idProducto);
            ActualizacionPrecio.setString(2, pcompra);
            ActualizacionPrecio.setString(3, pventa);
            ActualizacionPrecio.setString(4, bonificacion);
            ActualizacionPrecio.setDouble(5, 0);
            ActualizacionPrecio.setString(6, fechabonificacion);
            ActualizacionPrecio.setInt(7, idDeposito);
            ActualizacionPrecio.setInt(8, idTipoiva);
            ActualizacionPrecio.setString(9, "ACTUALIZACION PRECIO");
            ActualizacionPrecio.setInt(10, idprov);
            int n = ActualizacionPrecio.executeUpdate();
            if (n > 0) {
                ///System.out.println("Los Datos se modificaron exitosamente... " + idProducto);
                stock = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            stock = 0;
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizacionPrecio != null) {
                    ActualizacionPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return stock;
    }
}
