package Clases;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class fnCargarFecha {
    
    public fnCargarFecha() {
    }
    
    public String cargarfecha() {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
    public String cargarfechaTipoDate() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
    
    public String cargarHora() {
        SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String hora = formato.format(currentDate);
        return hora;
    }
    
    public String cargarfechayhoraSQL() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
    
    public String cargarfechaafip() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
}
  
