package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseProveedores {

    public ClaseProveedores() {
    }

    public int AgregarProveedor(int idTipodoc, String documento, String razon_social, String localidad, String domicilio, int codigo_postal) {
        int proveedor = 0;

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProveedores = null;
        PreparedStatement AgregarProveedor = null;
        //Veo si Ivadocumento no esta cargado en p_cliente
        String sqlraz = "SELECT razonsocial FROM proveedores WHERE razonsocial='" + razon_social + "'";
        try {
            SelectProveedores = cn.createStatement();
            ResultSet rsraz = SelectProveedores.executeQuery(sqlraz);
            if (!rsraz.next()) {
                cn.setAutoCommit(false); //transaction block start
                //Inserto el producto
                String sSQL = "INSERT INTO proveedores(idTipodoc, documento, razonsocial, localidad, direccion, codigopostal) VALUES(?,?,?,?,?,?)";
                AgregarProveedor = cn.prepareStatement(sSQL);
                AgregarProveedor.setInt(1, idTipodoc);
                AgregarProveedor.setString(2, documento);
                AgregarProveedor.setString(3, razon_social);
                AgregarProveedor.setString(4, localidad);
                AgregarProveedor.setString(5, domicilio);
                AgregarProveedor.setInt(6, codigo_postal);
                int n = AgregarProveedor.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
                cn.commit(); //transaction block end
                proveedor = 1;
            } else {
                JOptionPane.showMessageDialog(null, "El Proveedor ya esta en la base de datos");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (AgregarProveedor != null) {
                    AgregarProveedor.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return proveedor;
    }

    public void ModificarProveedor(int idProveedor, int idTipodoc, String documento, String razon_social, String localidad, String domicilio, int codigo_postal) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        String sSQL = "UPDATE proveedores SET idTipodoc=?, documento=?, razonsocial=?, localidad=?, direccion=?, codigopostal=? WHERE idProveedores=" + idProveedor;
        PreparedStatement ModificarProveedores = null;
        try {
            ModificarProveedores = cn.prepareStatement(sSQL);
            ModificarProveedores.setInt(1, idTipodoc);
            ModificarProveedores.setString(2, documento);
            ModificarProveedores.setString(3, razon_social);
            ModificarProveedores.setString(4, localidad);
            ModificarProveedores.setString(5, domicilio);
            ModificarProveedores.setInt(6, codigo_postal);
            int n = ModificarProveedores.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarProveedores != null) {
                    ModificarProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}
