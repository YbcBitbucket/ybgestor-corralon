package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseClientes {

    public ClaseClientes() {
    }

    public int AgregarCliente(int idResponsableIVA, int idTipodoc, String documento, String nombre, String domicilio, String localidad, String provincia, int margen, String persona, String telefonocel, String telefonofijo, String mail, String obs) {
        int cliente = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectIvaDocumento = null;
        PreparedStatement AgregarClientes = null;
        //Veo si Ivadocumento no esta cargado en p_cliente
        String sqldoc = "SELECT IvaDocumento FROM clientes WHERE IvaDocumento='" + documento + "' AND nombre='"+nombre+"'";
        try {
            SelectIvaDocumento = cn.createStatement();
            ResultSet rsdoc = SelectIvaDocumento.executeQuery(sqldoc);
            if (!rsdoc.next()) {
                cn.setAutoCommit(false); //transaction block start
                //Inserto el cliente
                String sSQL = "INSERT INTO clientes(idTiporesponsable, idTipodoc,"
                        + " IvaDocumento, nombre, domicilio, localidad,"
                        + " provincia, margen, persona, telcel, telfijo, mail, observacion) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                AgregarClientes = cn.prepareStatement(sSQL);
                AgregarClientes.setInt(1, idResponsableIVA);
                AgregarClientes.setInt(2, idTipodoc);
                AgregarClientes.setString(3, documento);
                AgregarClientes.setString(4, nombre);
                AgregarClientes.setString(5, domicilio);
                AgregarClientes.setString(6, localidad);
                AgregarClientes.setString(7, provincia);
                AgregarClientes.setInt(8, margen);
                AgregarClientes.setString(9, persona);
                AgregarClientes.setString(10, telefonocel);
                AgregarClientes.setString(11, telefonofijo);
                AgregarClientes.setString(12, mail);
                AgregarClientes.setString(13, obs);
                int n = AgregarClientes.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
                cn.commit(); //transaction block end
                cliente = 1;
            } else {
                JOptionPane.showMessageDialog(null, "El Cliente ya esta en la base de datos");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (SelectIvaDocumento != null) {
                    SelectIvaDocumento.close();
                }
                if (AgregarClientes != null) {
                    AgregarClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return cliente;
    }

    public void ModificarCliente(int idClienete, int idResponsableIVA, int idTipodoc, String documento, String nombre, String domicilio, String localidad, String provincia, int margen, String persona, String telefonocel, String telefonofijo, String mail, String obs) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarClientes = null;
        
        String sSQL = "UPDATE clientes SET"
                + " idTiporesponsable=?, idTipodoc=?, IvaDocumento=?,"
                + " nombre=?, domicilio=?, localidad=?,"
                + " provincia=?, margen=?, persona=?,"
                + " telcel=?, telfijo=?, mail=?,"
                + " observacion=?"
                + " WHERE idClientes=" + idClienete;
        try {
            ModificarClientes = cn.prepareStatement(sSQL);
            ModificarClientes.setInt(1, idResponsableIVA);
            ModificarClientes.setInt(2, idTipodoc);
            ModificarClientes.setString(3, documento);
            ModificarClientes.setString(4, nombre);
            ModificarClientes.setString(5, domicilio);
            ModificarClientes.setString(6, localidad);
            ModificarClientes.setString(7, provincia);
            ModificarClientes.setInt(8, margen);
            ModificarClientes.setString(9, persona);
            ModificarClientes.setString(10, telefonocel);
            ModificarClientes.setString(11, telefonofijo);
            ModificarClientes.setString(12, mail);
            ModificarClientes.setString(13, obs);
            int n = ModificarClientes.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (ModificarClientes != null) {
                    ModificarClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}
