package Clases;

import Formularios.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ClaseFacturacion {

    public ClaseFacturacion() {
    }

    public int FacturarPedido(int IdCliente, int IdPedidos, int PuntosdeVentas) {
        int idFacturacion = 0;
        //Saco el tipo de Facturacion
        //////////////////////////////
        int idTiporesponsable = 0;
        String AfipTipo = "";
        String RazonSocial = "";
        String Documento = "";
        int tipodocWSFE = 0;
        int tiporesponsableWSFE = 0;
        String Domicilio = "";
        String sql = "SELECT * FROM vista_tipofacturacion WHERE idClientes=" + IdCliente;
        
        String sqlPedidos = "SELECT * FROM vista_pedidos_detalle WHERE idPedidos=" + IdPedidos;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectCliente = null;
        Statement SelectHasar = null;
        Statement SelectPedidos = null;
        PreparedStatement Insertfacturacion = null;
        PreparedStatement Updatepedidos = null;

        PreparedStatement AgregarFacturacion = null;
        PreparedStatement AgregarFacturacion_iva = null;
        Statement UltimoIdFacturacion = null;
        try {
            SelectCliente = cn.createStatement();
            ResultSet rs = SelectCliente.executeQuery(sql);
            while (rs.next()) {
                idTiporesponsable = rs.getInt("idTiporesponsable");
                AfipTipo = rs.getString("AfipTipo");
                RazonSocial = rs.getString("RazonSocial");
                Documento = rs.getString("Documento");
                tipodocWSFE = rs.getInt("tipodocWSFE");
                tiporesponsableWSFE = rs.getInt("tiporesponsableWSFE");
                Domicilio = rs.getString("Domicilio");
            }

            //////////////////
            //SI EL TIPO DE FACTURACION ES WEB SERVISES FACTURA ELECTRONICA
            if (AfipTipo.equals("WSFE")) {

                //lote de comprobantes de ingreso
                int CantReg = 1;

                int CbteTipo;
                if (tiporesponsableWSFE == 1) {
                    //Ticket Factura A
                    CbteTipo = 1;
                } else {
                    //Ticket Factura B
                    CbteTipo = 6;
                }
                fnCargarFecha fecha = new fnCargarFecha();
                String CbteFech = fecha.cargarfechaafip();
                String CbteHora = fecha.cargarHora();
                String Fecha = fecha.cargarfecha();
                Double ImpTotal = 0.0;
                Double ImpNeto = 0.0;
                Double ImpNetobase = 0.0;
                Double DescTotal = 0.0;
                Double ImpIVA = 0.0;
                Double SubTotal = 0.0;
                Double ImpTotConc = 0.0;
                Double ImpOpEx = 0.0;
                Double ImpTrib = 0.0;

                Double Iva270base = 0.0;
                Double Iva210base = 0.0;
                Double Iva105base = 0.0;
                Double Iva50base = 0.0;
                Double Iva25base = 0.0;
                Double Iva0base = 0.0;

                SelectPedidos = cn.createStatement();
                ResultSet rsPedidos = SelectPedidos.executeQuery(sqlPedidos);
                while (rsPedidos.next()) {

                    double precio = rsPedidos.getDouble(27);
                    double cant = rsPedidos.getDouble(8);
                    double bonif = rsPedidos.getDouble(5);
                    double desc = rsPedidos.getDouble(20);
                    SubTotal = rsPedidos.getDouble(22);
                    DescTotal = desc;
                    fnRedondear redondear = new fnRedondear();

                    //IVAbase= PrecioV * (1-bon/100) * Cant * (1+Descuento/100);
//                    if (rsPedidos.getDouble(7) == 27) {
//                        Iva270base = Iva270base + redondear.dosDigitos(precio );
//                    }
                    if (CbteTipo == 1) {
                        Iva210base = Iva210base + redondear.dosDigitos(precio );
                    }
//                    if (rsPedidos.getDouble(7) == 10.5) {
//                        Iva105base = Iva105base + redondear.dosDigitos(precio );
//                    }
//                    if (rsPedidos.getDouble(7) == 5) {
//                        Iva50base = Iva50base + redondear.dosDigitos(precio );
//                    }
//                    if (rsPedidos.getDouble(7) == 2.5) {
//                        Iva25base = Iva25base + redondear.dosDigitos(precio );
//                    }
//                    if (rsPedidos.getDouble(7) == 0) {
//                        Iva0base = Iva0base + redondear.dosDigitos(precio );
//                    }
                }
                System.out.println("Iva0base:" + Iva0base);

                int idIVA = 0;
//                if (Iva270base != 0.0) {
//                    idIVA = idIVA + 1;
//                    ImpNetobase = Iva270base + ImpNetobase;
//                }
                if (Iva210base != 0.0) {
                    idIVA = idIVA + 1;
                    ImpNetobase = Iva210base + ImpNetobase;
                }
//                if (Iva105base != 0.0) {
//                    idIVA = idIVA + 1;
//                    ImpNetobase = Iva105base + ImpNetobase;
//                }
//                if (Iva50base != 0.0) {
//                    idIVA = idIVA + 1;
//                    ImpNetobase = Iva50base + ImpNetobase;
//                }
//                if (Iva25base != 0.0) {
//                    idIVA = idIVA + 1;
//                    ImpNetobase = Iva25base + ImpNetobase;
//                }
//                if (Iva0base != 0.0) {
//                    idIVA = idIVA + 1;
//                    ImpNetobase = Iva0base + ImpNetobase;
//                }
                System.out.println("ImpNetobase:" + ImpNetobase);
                System.out.println("DescTotal:" + DescTotal);
                String[][] datosiva = new String[idIVA][3];

                int ban270 = 0, ban210 = 0, ban105 = 0, ban50 = 0, ban25 = 0, ban0 = 0;
                for (int i = 0; i < idIVA; i++) {
                    fnRedondear redondear = new fnRedondear();
                    if (Iva270base != 0.0 && ban270 == 0) {
                        datosiva[i][0] = "6";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva270base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva270base * 27 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva270base * 27 / 100);
                        ban270 = 1;

                    } else if (Iva210base != 0.0 && ban210 == 0) {
                        datosiva[i][0] = "5";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva210base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva210base * 21 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva210base * 21 / 100);
                        ban210 = 1;
                    } else if (Iva105base != 0.0 && ban105 == 0) {
                        datosiva[i][0] = "4";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva105base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva105base * 10.5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva105base * 10.5 / 100);
                        ban105 = 1;
                    } else if (Iva50base != 0.0 && ban50 == 0) {
                        datosiva[i][0] = "8";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva50base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva50base * 5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva50base * 5 / 100);
                        ban50 = 1;
                    } else if (Iva25base != 0.0 && ban25 == 0) {
                        datosiva[i][0] = "9";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva25base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva25base * 2.5 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva25base * 2.5 / 100);
                        ban25 = 1;
                    } else if (Iva0base != 0.0 && ban0 == 0) {
                        datosiva[i][0] = "3";
                        datosiva[i][1] = String.valueOf(redondear.dosDigitos(Iva0base));
                        datosiva[i][2] = String.valueOf(redondear.dosDigitos(Iva0base * 0 / 100));
                        ImpIVA = ImpIVA + redondear.dosDigitos(Iva0base * 0 / 100);
                        ban0 = 1;
                    }
                }

                fnRedondear redondear = new fnRedondear();
                ImpNeto = redondear.dosDigitos(ImpNetobase);
                ImpIVA = redondear.dosDigitos(ImpIVA);
                ImpTotal = redondear.dosDigitos(ImpNeto + ImpIVA);

                System.out.println("///////////////////////////////////////");
                System.out.println("ImpNetobase " + ImpNeto);
                System.out.println("ImpTotal " + ImpTotal);
                System.out.println("ImpIVA " + ImpIVA);

                ClaseAfipWSCompUltimoAutorizado ComprobanteNro = new ClaseAfipWSCompUltimoAutorizado();
                int CompNro = Integer.valueOf(ComprobanteNro.CompUltimoAutorizado(PuntosdeVentas, CbteTipo)) + 1;
                String CbteDesde = String.valueOf(CompNro);
                String CbteHasta = CbteDesde;

                ClaseAfipWSCae cae = new ClaseAfipWSCae();
                String[] CAE = cae.ObtenerCAE(CantReg, CbteTipo, PuntosdeVentas, tipodocWSFE, Documento, CbteDesde, CbteHasta,
                        CbteFech, ImpTotal, ImpTotConc, ImpNeto, ImpOpEx, ImpIVA, ImpTrib, datosiva);
                //TENGO CAE
                System.out.println("CbteFech " + CbteFech);
                if (!CAE[0].equals("RECHAZADO")) {

                    String SQL = "";

                    cn.setAutoCommit(false); //transaction block start

                    SQL = "INSERT INTO facturacion (idPedidos, cantReg, idTipoCbte, puntoVenta, idTiporesponsable, "
                            + "documento, cbteDesde, cbteHasta, cbteFech, impTotal, impTotConc, impNeto, impOpEx, "
                            + "impIVA, impTrib, AfipTipo, CAE, fechaCAE, id_usuario,horaCbte,totaldescuento, subtotalNeto,fechaCbte,totalVenta,totalIVA) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    AgregarFacturacion = cn.prepareStatement(SQL);

                    AgregarFacturacion.setInt(1, IdPedidos);
                    AgregarFacturacion.setInt(2, CantReg);
                    AgregarFacturacion.setInt(3, CbteTipo);
                    AgregarFacturacion.setInt(4, PuntosdeVentas);
                    AgregarFacturacion.setInt(5, idTiporesponsable);
                    AgregarFacturacion.setString(6, Documento);
                    AgregarFacturacion.setString(7, CbteDesde);
                    AgregarFacturacion.setString(8, CbteHasta);
                    AgregarFacturacion.setString(9, CbteFech);
                    AgregarFacturacion.setDouble(10, ImpTotal);
                    AgregarFacturacion.setDouble(11, ImpTotConc);
                    AgregarFacturacion.setDouble(12, ImpNeto);
                    AgregarFacturacion.setDouble(13, ImpOpEx);
                    AgregarFacturacion.setDouble(14, ImpIVA);
                    AgregarFacturacion.setDouble(15, ImpTrib);
                    AgregarFacturacion.setString(16, AfipTipo);
                    AgregarFacturacion.setString(17, CAE[0]);
                    AgregarFacturacion.setString(18, CAE[1]);
                    AgregarFacturacion.setInt(19, Login.id_usuario);
                    AgregarFacturacion.setString(20, CbteHora);
                    AgregarFacturacion.setDouble(21, DescTotal);
                    AgregarFacturacion.setDouble(22, SubTotal);
                    AgregarFacturacion.setString(23, Fecha);
                    AgregarFacturacion.setDouble(24, ImpNeto);
                    AgregarFacturacion.setDouble(25, ImpIVA);

                    AgregarFacturacion.executeUpdate();

                    //Busco Ultimo Factura
                    UltimoIdFacturacion = cn.createStatement();
                    ResultSet rsFac = UltimoIdFacturacion.executeQuery("SELECT MAX(idFacturacion) AS idFacturacion FROM facturacion");
                    rsFac.next();
                    idFacturacion = rsFac.getInt("idFacturacion");

                    for (int i = 0; i < datosiva.length; i++) {
                        //Inserto el cliente
                        String sSQL = "INSERT INTO facturacion_iva(idFacturacion, idTipoiva, baseImp, importe) VALUES(?,?,?,?)";
                        AgregarFacturacion_iva = cn.prepareStatement(sSQL);
                        AgregarFacturacion_iva.setInt(1, idFacturacion);
                        AgregarFacturacion_iva.setInt(2, Integer.valueOf(datosiva[i][0]));
                        AgregarFacturacion_iva.setDouble(3, Double.valueOf(datosiva[i][1]));
                        AgregarFacturacion_iva.setDouble(4, Double.valueOf(datosiva[i][2]));

                        AgregarFacturacion_iva.executeUpdate();
                    }

                    String SQLUpdatePedidos = "UPDATE pedidos SET estado=? WHERE idPedidos=" + IdPedidos;
                    Updatepedidos = cn.prepareStatement(SQLUpdatePedidos);
                    Updatepedidos.setString(1, "FACTURADO");
                    Updatepedidos.executeUpdate();

                    cn.commit(); //transaction block end
                    // JOptionPane.showMessageDialog(null, "La Facturacion se realizo con exito...");

                } else {
                    idFacturacion = 0;
                    JOptionPane.showMessageDialog(null, "El Pedido fue Rechazado por Afip");
                }

            }

//            //////////////////
//            //SI EL TIPO DE FACTURACION ES HASAR
//            if (AfipTipo.equals("HASAR")) {
//                //Datos de Configuracion
//                int transporte = 0;
//                int puerto = 0;
//                int baudios = 0;
//                String direccionip = "";
//                SelectHasar = cn.createStatement();
//                ResultSet rsHasar = SelectHasar.executeQuery(sql2);
//                rsHasar.last();
//                transporte = rsHasar.getInt("transporte");
//                puerto = rsHasar.getInt("puerto");
//                baudios = rsHasar.getInt("baudios");
//                direccionip = rsHasar.getString("direccionip");
//                //Datos para comprobante
//                int TipoCbte = 0;
//                String formadepago = "";
//                Double total = 0.0;
//                Double Descuento = 0.0;
//                Double DescuentoBase = 0.0;
//                //Datos Facturacion
//                Double VentaTotal = 0.0000;
//                Double IvaTotal = 0.0000;
//                int NumeroCbte = 0;
//                int idTipoCbte = 0;
//
//                SelectPedidos = cn.createStatement();
//                ResultSet rsPedidos = SelectPedidos.executeQuery(sqlPedidos);
//
//                ServiciosHasar hasar = new ServiciosHasar();
//                if (!"ERROR".equals(hasar.Comenzar(transporte, puerto, baudios, direccionip))) {
//                    hasar.TratarDeCancelarTodo();
//                    //Cliente IVA Responsable Inscripto
//                    if (idTiporesponsable == 1) {
//                        //Ticket Factura A
//                        TipoCbte = 65;
//                        idTipoCbte = 66;
//
//                        hasar.DatosCliente(RazonSocial, Documento, tipodocHASAR, tiporesponsableHASAR, Domicilio);
//                        hasar.AbrirComprobanteFiscal(TipoCbte);
//                        while (rsPedidos.next()) {
//                            //Para factura Precio Base y Impuesto Interno Fijo
//                            hasar.PrecioBase(true);
//                            hasar.ImpuestoInternoFijo(true);
//                            fnRedondear redondear = new fnRedondear();
//                            //Precio sin IVA * bonificacion
//                            Double PreciosinIVA = redondear.dosDigitos(rsPedidos.getDouble("precioVenta") * (1 - (rsPedidos.getDouble("bonificacion") / 100)));
//                            //Double PreciosinIVA = Redondear(rsPedidos.getDouble("precioVenta"));
//                            hasar.ImprimirItem(rsPedidos.getString("nombre"), rsPedidos.getDouble("cantidad"), PreciosinIVA, rsPedidos.getDouble("tipoiva"), 0);
//                            /*//bonificacion
//                             if(rsPedidos.getDouble("bonificacion")>0.00){
//                             hasar.DescuentoUltimoItem("Bonificacion", rsPedidos.getDouble("bonificacion"), true);
//                             }*/
//                            formadepago = rsPedidos.getString("formadepago");
//                            total = rsPedidos.getDouble("total");
//                            DescuentoBase = rsPedidos.getDouble("descuentobase");
//                        }
//                        hasar.DescuentoGeneral("Desc Forma de Pago", DescuentoBase, true);
//                        hasar.Subtotal(true);
//                        VentaTotal = Double.valueOf(hasar.Respuesta(4));
//                        IvaTotal = Double.valueOf(hasar.Respuesta(5));
//                        fnRedondear redondear = new fnRedondear();
//                        hasar.ImprimirPago(formadepago, redondear.dosDigitos(VentaTotal));
//                        hasar.CerrarComprobanteFiscal(1);
//                        NumeroCbte = hasar.UltimoDocumentoFiscalA();
//
//                    } else {
//                        //Ticket Factura B
//                        TipoCbte = 66;
//                        idTipoCbte = 67;
//                        hasar.AvanzarPapel(0, 3);
//                        hasar.DatosCliente(RazonSocial, Documento, tipodocHASAR, tiporesponsableHASAR, Domicilio);
//                        hasar.AbrirComprobanteFiscal(TipoCbte);
//                        while (rsPedidos.next()) {
//                            fnRedondear redondear = new fnRedondear();
//                            //Precio con IVA * bonificacion
//                            Double PrecioIVA = redondear.dosDigitos(rsPedidos.getDouble("precioVenta") * (1 - (rsPedidos.getDouble("bonificacion") / 100)) * (1 + (rsPedidos.getDouble("tipoiva") / 100)));
//                            //Double PrecioIVA = Redondear(rsPedidos.getDouble("precioVenta") * (1 + (rsPedidos.getDouble("tipoiva") / 100)));
//                            hasar.ImprimirItem(rsPedidos.getString("nombre"), rsPedidos.getDouble("cantidad"), PrecioIVA, rsPedidos.getDouble("tipoiva"), 0);
//                            //bonificacion
//                            //if(rsPedidos.getDouble("bonificacion")>0.00){
//                            //hasar.DescuentoUltimoItem("Bonificacion", rsPedidos.getDouble("bonificacion"), true);
//                            //}
//                            formadepago = rsPedidos.getString("formadepago");
//                            total = rsPedidos.getDouble("total");
//                            Descuento = rsPedidos.getDouble("descuento");
//                        }
//                        hasar.DescuentoGeneral("Descuento", Descuento, true);
//                        hasar.Subtotal(true);
//                        VentaTotal = Double.valueOf(hasar.Respuesta(4));
//                        IvaTotal = Double.valueOf(hasar.Respuesta(5));
//                        fnRedondear redondear = new fnRedondear();
//                        hasar.ImprimirPago(formadepago, redondear.dosDigitos(VentaTotal));
//                        hasar.CerrarComprobanteFiscal(1);
//                        NumeroCbte = hasar.UltimoDocumentoFiscalA();
//                    }
//                    ////Guardo en Facturacion
//                    String SQLInsertfacturacion = "INSERT INTO facturacion (idPedidos, idTipoCbte, numeroCbte, fechaCbte, idPuntosdeVentas, totalVenta, totalIVA, id_usuario) "
//                            + "VALUES (?,?,?,?,?,?,?,?)";
//                    Insertfacturacion = cn.prepareStatement(SQLInsertfacturacion);
//                    Insertfacturacion.setInt(1, IdPedidos);
//                    Insertfacturacion.setInt(2, idTipoCbte);
//                    Insertfacturacion.setInt(3, NumeroCbte);
//                    fnCargarFecha fecha = new fnCargarFecha();
//                    Insertfacturacion.setDate(4, fecha.cargarfechaSQL());
//                    Insertfacturacion.setInt(5, PuntosdeVentas);
//                    Insertfacturacion.setDouble(6, VentaTotal);
//                    Insertfacturacion.setDouble(7, IvaTotal);
//                    Insertfacturacion.setInt(8, Login.id_usuario);
//                    Insertfacturacion.executeUpdate();
//
//                    String SQLUpdatePedidos = "UPDATE pedidos SET estado=? WHERE idPedidos=" + IdPedidos;
//                    Updatepedidos = cn.prepareStatement(SQLUpdatePedidos);
//                    Updatepedidos.setString(1, "FACTURADO");
//                    Updatepedidos.executeUpdate();
//
//                    //JOptionPane.showMessageDialog(null, "La Facturacion se realizo con exito...");
//                } else {
//                    idFacturacion = 0;
//                }
//            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCliente != null) {
                    SelectCliente.close();
                }
                if (SelectHasar != null) {
                    SelectHasar.close();
                }
                if (SelectPedidos != null) {
                    SelectPedidos.close();
                }
                if (Insertfacturacion != null) {
                    Insertfacturacion.close();
                }
                if (Updatepedidos != null) {
                    Updatepedidos.close();
                }
                if (AgregarFacturacion != null) {
                    AgregarFacturacion.close();
                }
                if (AgregarFacturacion_iva != null) {
                    AgregarFacturacion_iva.close();
                }
                if (UltimoIdFacturacion != null) {
                    UltimoIdFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
        return idFacturacion;
    }

    public void ImprimirListadoMovimientos(String forma_pago, String estado, String fecha_inicio, String fecha_fin) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Listado de Movimientos", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reportes_Pedidos.jasper"));
            Map parametros = new HashMap();
            parametros.put("forma_pago", forma_pago);
            parametros.put("estado", estado);
            parametros.put("fecha_inicio", fecha_inicio);
            parametros.put("fecha_fin", fecha_fin);

            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "El pedido no se pudo imprimir");
        }
    }

    public String numeroVerificador(String codigo) {
        int par = 0;
        int impar = 0;
        int i = 1;
        int suma = 0;

        while (i <= codigo.length()) {
            if (i % 2 == 0) {

                par = par + Integer.valueOf(String.valueOf((codigo.charAt(i - 1))));

            } else {

                impar = impar + Integer.valueOf(String.valueOf((codigo.charAt(i - 1))));

            }
            i++;
        }
        suma = par + (impar * 3);

        int verify_number;

        if (10 - (suma % 10) == 10) {
            verify_number = 0;
        } else {
            verify_number = 10 - (suma % 10);
        }
        return String.valueOf(verify_number);
    }

    public void ImprimirFactura(int idFacturacion) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        String NumVerif = "", codigo_barra = "";
        String sql = "SELECT *, LPAD(puntoventa,5,0) as pventa, LPAD(idTipoCbte,3,0) as tipocbe  FROM facturacion WHERE idFacturacion=" + idFacturacion;
//        String sql = "SELECT * FROM facturacion WHERE idFacturacion=" + idFacturacion;

        Statement SelectFacturacion = null;
        try {
            SelectFacturacion = cn.createStatement();
            ResultSet rs = SelectFacturacion.executeQuery(sql);
            rs.next();
            codigo_barra = "20346059479" + rs.getString("tipocbe") + rs.getString("pventa") + rs.getString("cae") + rs.getString("fechacae");

            codigo_barra = codigo_barra + numeroVerificador(codigo_barra);
            ///FACTURA A
            if (rs.getInt("idTipoCbte") == 1) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/FacturaA.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    //reportparametermap1.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    //reportparametermap2.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);
                    //reportparametermap3.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);

                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    //JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////FACTURA B
            if (rs.getInt("idTipoCbte") == 6) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/FacturaB.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);

                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);

                        }

                    }
///                    JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ///NOTAS DE CREDITO A
            if (rs.getInt("idTipoCbte") == 3) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota Credito A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoA.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);
                    //reportparametermap1.put("Imagen", this.getClass().getResourceAsStream(imagen));

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);
                    //reportparametermap2.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);
                    //reportparametermap3.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////NOTAS DE CREDITO B
            if (rs.getInt("idTipoCbte") == 8) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota de Credito B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoB.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);

                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///  JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);

                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFacturacion != null) {
                    SelectFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
    }

    public int ImprimirTickectFactura(int idFacturacion) {
        int imprime = 0;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        String NumVerif = "", codigo_barra = "";
        String sql = "SELECT *, LPAD(puntoventa,5,0) as pventa, LPAD(idTipoCbte,3,0) as tipocbe  FROM facturacion WHERE idFacturacion=" + idFacturacion;
//        String sql = "SELECT * FROM facturacion WHERE idFacturacion=" + idFacturacion;

        Statement SelectFacturacion = null;
        try {
            SelectFacturacion = cn.createStatement();
            ResultSet rs = SelectFacturacion.executeQuery(sql);
            rs.next();
            codigo_barra = "20346059479" + rs.getString("tipocbe") + rs.getString("pventa") + rs.getString("cae") + rs.getString("fechacae");

            codigo_barra = codigo_barra + numeroVerificador(codigo_barra);
            ///FACTURA A
            if (rs.getInt("idTipoCbte") == 1) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ticket_A.jasper"));

                    //Map<String, Object> parametros = new HashMap<String, Object>();
                    Map reportparametermap1 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    //JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    // JasperViewer visualizador = new JasperViewer(jPrint, true);
                    //viewer.getContentPane().add(visualizador.getContentPane());
                    /// viewer.setVisible(true);
                    JasperPrintManager.printReport(jPrint, false);
                    imprime = 1;
                } catch (JRException ex) {
                    imprime = 0;
                    JOptionPane.showMessageDialog(null, ex);
                    // JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////FACTURA B
            if (rs.getInt("idTipoCbte") == 6) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Factura B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ticket_B.jasper"));
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    //JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    /* JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);*/
                    JasperPrintManager.printReport(jPrint, false);
                    imprime = 1;

                } catch (JRException ex) {
                    imprime = 0;
                    JOptionPane.showMessageDialog(null, ex);
                    //JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ///NOTAS DE CREDITO A
            if (rs.getInt("idTipoCbte") == 3) {
                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota Credito A", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);
                    //String imagen = "/Imagenes/Logoparafactura.png";
                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoA.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);
                    //reportparametermap1.put("Imagen", this.getClass().getResourceAsStream(imagen));

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);
                    //reportparametermap2.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);
                    //reportparametermap3.put("Imagen", this.getClass().getResourceAsStream(imagen));
                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);
                    imprime = 1;
                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    imprime = 0;
                    JOptionPane.showMessageDialog(null, ex);
                    ///JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
            ////NOTAS DE CREDITO B
            if (rs.getInt("idTipoCbte") == 8) {

                JasperReport reporte;
                try {
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Nota de Credito B", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
                    viewer.setSize(1024, 720);
                    viewer.setLocationRelativeTo(null);

                    reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/NotaCreditoB.jasper"));
                    List<Map> ParamList = new ArrayList<Map>();
                    //Map<String, Object> parametros = new HashMap<String, Object>();

                    Map reportparametermap1 = new HashMap();
                    Map reportparametermap2 = new HashMap();
                    Map reportparametermap3 = new HashMap();
                    //Map parametros = new HashMap();
                    reportparametermap1.put("idFacturacion", idFacturacion);
                    reportparametermap1.put("copias", "ORIGINAL");
                    reportparametermap1.put("CodigoBarra", codigo_barra);

                    reportparametermap2.put("idFacturacion", idFacturacion);
                    reportparametermap2.put("copias", "DUPLICADO");
                    reportparametermap2.put("CodigoBarra", codigo_barra);

                    reportparametermap3.put("idFacturacion", idFacturacion);
                    reportparametermap3.put("copias", "TRIPLICADO");
                    reportparametermap3.put("CodigoBarra", codigo_barra);

                    ParamList.add(reportparametermap2);
                    ParamList.add(reportparametermap3);

                    JasperPrint jPrint = JasperFillManager.fillReport(reporte, reportparametermap1, cn);

                    for (int i = 0; i < 2; i++) {
                        JasperPrint jasperPrint_next = JasperFillManager.fillReport(reporte, ParamList.get(i), cn);
                        List pages = jasperPrint_next.getPages();
                        for (int j = 0; j < pages.size(); j++) {
                            JRPrintPage object = (JRPrintPage) pages.get(j);
                            jPrint.addPage(object);
                        }
                    }
                    ///  JasperExportManager.exportReportToPdfFile(jPrint, "\\\\138.99.7.73\\pdf-facturacion\\PDF-FACTURAS-PROVEEDURIA\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    JasperExportManager.exportReportToPdfFile(jPrint, "c:\\PDF-FACTURAS\\" + rs.getInt("idTipoCbte") + "-" + rs.getInt("puntoVenta") + "-" + rs.getInt("cbteDesde") + ".pdf");
                    //JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
                    JasperViewer visualizador = new JasperViewer(jPrint, false);
                    viewer.getContentPane().add(visualizador.getContentPane());
                    viewer.setVisible(true);
                    imprime = 1;
                    //JasperPrintManager.printReport(jPrint, true);
                } catch (JRException ex) {
                    imprime = 0;
                    JOptionPane.showMessageDialog(null, ex);
                    //JOptionPane.showMessageDialog(null, "El Cbte no se pudo imprimir");

                }

            }
        } catch (SQLException ex) {
            imprime = 0;
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFacturacion != null) {
                    SelectFacturacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo facturar");
            }
        }
        return imprime;
    }

}
