package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ClaseCuentaCorriente {

    public ClaseCuentaCorriente() {
    }

    public void AgregarCtaCte(int IdCliente) {
        int IdCtaCte = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarCtaCte = null;
        Statement UltimoIdCtaCte = null;
        PreparedStatement AgregardetalleCtaCte = null;
        String sSQL = "";
        String SQL = "";
        fnCargarFecha f = new fnCargarFecha();
        if (IdCliente != 1) {
            try {
                cn.setAutoCommit(false); //transaction block start

                sSQL = "INSERT INTO cuentacorriente (fecha, estado, idClientes) VALUES(?,?,?)";
                AgregarCtaCte = cn.prepareStatement(sSQL);
                AgregarCtaCte.setString(1, f.cargarfecha());
                AgregarCtaCte.setString(2, "HABILITADO");
                AgregarCtaCte.setInt(3, IdCliente);
                AgregarCtaCte.executeUpdate();

                //Busco Ultimo ID CtaCte
                UltimoIdCtaCte = cn.createStatement();
                ResultSet rs = UltimoIdCtaCte.executeQuery("SELECT MAX(idCuentaCorriente) AS idCuentaCorriente FROM cuentacorriente");
                rs.next();
                IdCtaCte = rs.getInt("idCuentaCorriente");

                SQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, debe, haber, fecha) VALUES(?,?,?,?,?)";
                AgregardetalleCtaCte = cn.prepareStatement(SQL);
                AgregardetalleCtaCte.setInt(1, IdCtaCte);
                AgregardetalleCtaCte.setString(2, "INICIO DE ACTIVIDAD");
                AgregardetalleCtaCte.setDouble(3, 0.0);
                AgregardetalleCtaCte.setDouble(4, 0.0);
                AgregardetalleCtaCte.setString(5, f.cargarfecha());
                AgregardetalleCtaCte.executeUpdate();

                cn.commit(); //transaction block end

                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (AgregarCtaCte != null) {
                        AgregarCtaCte.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se puede crear una Cta Cte a un CONSUMIDOR FINAL");
        }
    }

    public void ModificarCatCte(int IdCliente, String estado) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarCtaCte = null;

        try {
            String sSQL = "UPDATE cuentacorriente SET"
                    + " estado=?"
                    + " WHERE idClientes=" + IdCliente;
            ModificarCtaCte = cn.prepareStatement(sSQL);
            ModificarCtaCte.setString(1, estado);

            int n = ModificarCtaCte.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarCtaCte != null) {
                    ModificarCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void PagoCatCte(int idCuentaCorriente, double haber, String detalle) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement PagoCtaCte = null;
        fnCargarFecha f = new fnCargarFecha();
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        try {
            String sSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, haber, fecha, detalle,id_caja) VALUES(?,?,?,?,?,?)";
            PagoCtaCte = cn.prepareStatement(sSQL);
            PagoCtaCte.setInt(1, idCuentaCorriente);
            PagoCtaCte.setString(2, "PAGO DE CTA CTE");
            PagoCtaCte.setDouble(3, haber);
            PagoCtaCte.setString(4, f.cargarfecha());
            PagoCtaCte.setString(5, detalle);
            PagoCtaCte.setInt(6, idCaja);
            int n = PagoCtaCte.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (PagoCtaCte != null) {
                    PagoCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void ImprimirEstado(int IdCliente) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        System.out.println("IdCliente_" + IdCliente);
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Cuenta Corriente", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reporte_CuentaCorriente.jasper"));
            Map parametros = new HashMap();
            parametros.put("id_cliente", String.valueOf(IdCliente));
            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

            //JasperPrintManager.printReport(jPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "La cuenta no se pudo imprimir");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    public void ImprimirEstadoFecha(int IdCliente, String fechaInicial, String fechaFinal) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        System.out.println("IdCliente_" + IdCliente);
        System.out.println("fechaInicial" + fechaInicial);
        System.out.println("fechaFinal" + fechaFinal);
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Cuenta Corriente", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);
            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reporte_CuentaCorriente_Fecha.jasper"));
            Map parametros = new HashMap();
            parametros.put("id_cliente", String.valueOf(IdCliente));
            parametros.put("fechainicial", fechaInicial);
            parametros.put("fechafinal", fechaFinal);
            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

            //JasperPrintManager.printReport(jPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "La cuenta no se pudo imprimir");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    public static int idCuentaCorriente(int idCliente) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProductos = null;
        int idCuentaCorriente = 0;
        String sSQL = "SELECT  * FROM vista_ctactes WHERE idClientes=" + idCliente;
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sSQL);
            rs.next();
            idCuentaCorriente = rs.getInt("idCuentaCorriente");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return idCuentaCorriente;
    }

    public void ImprimirPagoCtaCte(int IdPagoCC) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;
        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Pago Cuenta Corriente", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reporte_Pago_Cliente.jasper"));
            Map parametros = new HashMap();
            parametros.put("idPagoCC", String.valueOf(IdPagoCC));
            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

            //JasperPrintManager.printReport(jPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "La cuenta no se pudo imprimir");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public int ActualizacionCuentaCorriente(int idCuentaCorriente, double debe) {
        int estado = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement PagoCtaCte = null;
        fnCargarFecha f = new fnCargarFecha();
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        try {
            String sSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, debe, fecha,id_caja) VALUES(?,?,?,?,?)";
            PagoCtaCte = cn.prepareStatement(sSQL);
            PagoCtaCte.setInt(1, idCuentaCorriente);
            PagoCtaCte.setString(2, "");
            PagoCtaCte.setDouble(3, debe);
            PagoCtaCte.setString(4, f.cargarfecha());
            PagoCtaCte.setInt(5, idCaja);
            int n = PagoCtaCte.executeUpdate();
            if (n > 0) {
                estado = 1;
            }
        } catch (SQLException ex) {
            estado = 0;
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (PagoCtaCte != null) {
                    PagoCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return estado;
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    public void actualizacionDetalleCuentaCorriente(int idCuentaCorriente, int idPedido, double debe) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizacionCtaCte = null;

        String sSQL = "";
        try {
            cn.setAutoCommit(false); //transaction block start
            sSQL = "UPDATE detalledecuentacorriente SET"
                    + " detalle=?,debe=?"
                    + " WHERE idCuentaCorriente=" + idCuentaCorriente + " and idPedidos=" + idPedido;

            ActualizacionCtaCte = cn.prepareStatement(sSQL);
            ActualizacionCtaCte.setString(1, "");
            ActualizacionCtaCte.setDouble(2, debe);
            ActualizacionCtaCte.executeUpdate();

            cn.commit(); //transaction block end

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex + "/nclase cuenta corriente. Error en la base de datos.(e317)");
        } finally {
            try {
                if (ActualizacionCtaCte != null) {
                    ActualizacionCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex + "/nclase cuenta corriente. Error en la base de datos.(e326)");
            }
        }

    }

    public int anulacionCuentaCorriente(int idCuentaCorriente, int idPedido, double haber) {
        int estado = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement PagoCtaCte = null;
        Statement SelectPedido = null;
        fnCargarFecha f = new fnCargarFecha();
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        try {
            String SqlCuentaCorriente_detalle = "SELECT * FROM detalledecuentacorriente WHERE idPedidos=" + idPedido;
            SelectPedido = cn.createStatement();
            ResultSet rs = SelectPedido.executeQuery(SqlCuentaCorriente_detalle);
            if (rs.next()) {
                String sSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, debe, fecha,id_caja) VALUES(?,?,?,?,?)";
                PagoCtaCte = cn.prepareStatement(sSQL);
                PagoCtaCte.setInt(1, idCuentaCorriente);
                PagoCtaCte.setString(2, "ANULACION PEDIDO " + idPedido);
                PagoCtaCte.setDouble(3, -haber);
                PagoCtaCte.setString(4, f.cargarfecha());
                PagoCtaCte.setInt(5, idCaja);
                int n = PagoCtaCte.executeUpdate();
                if (n > 0) {
                    estado = 1;
                }
            }
        } catch (SQLException ex) {
            estado = 0;
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (PagoCtaCte != null) {
                    PagoCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return estado;
    }
}
