package Clases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;


public class ClaseAfipEstado {
    public ClaseAfipEstado() {
    }

    public String Estado() {
        //Consultar que tipo de SERVIDOR USAR
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement TipoAfip = null;
        String sSQL = "SELECT estado FROM afip_tipo_facturacion WHERE tipo=" + 1;
        String estado = null;
        try {
            TipoAfip = cn.createStatement();
            ResultSet rs = TipoAfip.executeQuery(sSQL);
            TipoAfip = cn.createStatement();
            rs.next();
            estado = rs.getString("estado");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TipoAfip != null) {
                    TipoAfip.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return estado;
    }
}
