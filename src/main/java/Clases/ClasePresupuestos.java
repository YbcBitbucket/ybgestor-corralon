package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.ImageIcon;
import static Formularios.Login.id_usuario;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ClasePresupuestos {

    public ClasePresupuestos() {
    }

    public int AgregarPresupuesto(String[][] datos, String Fecha, String Hora, Double Total, Double Descuento, int IdCliente, int IdDeposito, int FormadePago, Double DescuentoBase, Double DescuentoPorcentaje, Double Subtotal, Double Interes, Double InteresPorcentaje, String descripcion, int id_usuario,int idTipo) {
        int IdPresupuestos = 0;
        /////   Conexion Mysql
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertPresupuestos = null;
        Statement UltimoIdPresupuestos = null;
        PreparedStatement InsertDetallePresupuestos = null;
        PreparedStatement InsertformadepagoPresupuestos = null;
        try {
            String SQLInsertPedido = "INSERT INTO presupuestos (total, fecha, hora, idClientes, subtotal,observacion,id_usuario) "
                    + "VALUES (?,?,?,?,?,?,?)";

            cn.setAutoCommit(false); //transaction block start
            //Inserto Presupuesto
            InsertPresupuestos = cn.prepareStatement(SQLInsertPedido);
            InsertPresupuestos.setDouble(1, Total);
            InsertPresupuestos.setString(2, Fecha);
            InsertPresupuestos.setString(3, Hora);
            InsertPresupuestos.setInt(4, IdCliente);
            InsertPresupuestos.setDouble(5, Subtotal);
            InsertPresupuestos.setString(6, descripcion);
            InsertPresupuestos.setInt(7, id_usuario);
            InsertPresupuestos.executeUpdate();
            //Busco Ultimo ID Presupuesto
            UltimoIdPresupuestos = cn.createStatement();
            ResultSet rs = UltimoIdPresupuestos.executeQuery("SELECT MAX(idPresupuestos) AS idPresupuestos FROM presupuestos");
            rs.next();
            IdPresupuestos = rs.getInt("idPresupuestos");
            //Cargo Detalle Presupuesto

            String SQLInsertDetallePedido = "INSERT INTO detalledepresupuestos (idPresupuestos, idProductos, cantidad, precioVenta, idTipoiva, bonificacion) "
                    + "VALUES (?,?,?,?,?,?)";

            InsertDetallePresupuestos = cn.prepareStatement(SQLInsertDetallePedido);

            for (int i = 0; i < datos.length; i++) {
                //Insero Detalle Presupuestos
                InsertDetallePresupuestos.setInt(1, IdPresupuestos);
                InsertDetallePresupuestos.setInt(2, Integer.valueOf(datos[i][0]));
                InsertDetallePresupuestos.setDouble(3, Double.valueOf(datos[i][7]));
                InsertDetallePresupuestos.setDouble(4, Double.valueOf(datos[i][3]));
                InsertDetallePresupuestos.setInt(5, Integer.valueOf(datos[i][5]));
                InsertDetallePresupuestos.setDouble(6, Double.valueOf(datos[i][4]));
                InsertDetallePresupuestos.executeUpdate();
            }
            //FormadePago
            String SQLInsertFormadePago = "INSERT INTO presupuestos_tienen_formasdepago (idFormasDePago, idPresupuestos, descuento, descuentobase, descuentoporcentaje, interes,interesporcentaje,idTipo)"
                    + "VALUES (?,?,?,?,?,?,?,?)";
            InsertformadepagoPresupuestos = cn.prepareStatement(SQLInsertFormadePago);
            InsertformadepagoPresupuestos.setInt(1, FormadePago);
            InsertformadepagoPresupuestos.setInt(2, IdPresupuestos);
            InsertformadepagoPresupuestos.setDouble(3, Descuento);
            InsertformadepagoPresupuestos.setDouble(4, DescuentoBase);
            InsertformadepagoPresupuestos.setDouble(5, DescuentoPorcentaje);
            InsertformadepagoPresupuestos.setDouble(6, Interes);
            InsertformadepagoPresupuestos.setDouble(7, InteresPorcentaje);
            InsertformadepagoPresupuestos.setInt(8, idTipo);
            InsertformadepagoPresupuestos.executeUpdate();
            /////////
            cn.commit(); //transaction block end
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            try {
                cn.rollback();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        } finally {
            try {
                if (InsertPresupuestos != null) {
                    InsertPresupuestos.close();
                }
                if (UltimoIdPresupuestos != null) {
                    UltimoIdPresupuestos.close();
                }
                if (InsertDetallePresupuestos != null) {
                    InsertDetallePresupuestos.close();
                }
                if (InsertformadepagoPresupuestos != null) {
                    InsertformadepagoPresupuestos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        JOptionPane.showMessageDialog(null, "El presupuesto se realizo con exito...");
        return IdPresupuestos;
    }

    public void ImprimirPresupuesto(int IdPresupuestos) {

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        JasperReport reporte;

        try {
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Pedido", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
            viewer.setSize(1024, 720);
            viewer.setLocationRelativeTo(null);

            reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Reporte_Presupuesto.jasper"));
            Map parametros = new HashMap();
            parametros.put("id_presupuesto", String.valueOf(IdPresupuestos));
            JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, cn);
            JasperViewer visualizador = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(visualizador.getContentPane());
            viewer.setVisible(true);

            //JasperPrintManager.printReport(jPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "El pedido no se pudo imprimir");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    /*public void ImprimirPresupuesto(int IdPresupuestos) {
        LinkedList<camposbd> Resultados = new LinkedList<camposbd>();
        Resultados.clear();
        String cliente = null, fecha = null, subtotal = null, descuento = null, total = null, formadepago = null;
        String sqlPedidos = "SELECT * FROM vista_presupuestos_detalle WHERE idPresupuestos=" + IdPresupuestos;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectPedidos = null;
        try {
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidos = SelectPedidos.executeQuery(sqlPedidos);
            while (rsPedidos.next()) {
                String Total = String.valueOf(Redondear(rsPedidos.getDouble("precioVenta") * (1 - (rsPedidos.getDouble("bonificacion") / 100)) * (1 + (rsPedidos.getDouble("tipoiva") / 100))) * rsPedidos.getInt("cantidad"));
                camposbd tipo;
                tipo = new camposbd(rsPedidos.getString("codigo"), rsPedidos.getString("nombre"), rsPedidos.getString("precioVenta"), rsPedidos.getString("bonificacion"), rsPedidos.getString("tipoiva"), rsPedidos.getString("cantidad"), Total);
                Resultados.add(tipo);
                cliente = rsPedidos.getString("cliente");
                fecha = rsPedidos.getString("fecha");
                subtotal = rsPedidos.getString("subtotal");
                descuento = rsPedidos.getString("descuento");
                total = rsPedidos.getString("total");
                formadepago = rsPedidos.getString("formadepago");

            }
            JasperReport reporte;
            try {
                reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ventas.jasper"));
                Map parametros = new HashMap();
                parametros.put("tipo", "Presupuesto");
                parametros.put("cliente", cliente);
                parametros.put("numero", completarInt(String.valueOf(IdPresupuestos), 8));
                parametros.put("fecha", fecha);
                parametros.put("subtotal", subtotal);
                parametros.put("descuento", descuento);
                parametros.put("total", total);
                parametros.put("forma", formadepago);
                JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(Resultados));
                JasperViewer visualizador = new JasperViewer(jPrint, false);
                visualizador.setVisible(true);
                
            } catch (JRException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El presupuesto no se pudo imprimir");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPedidos != null) {
                    SelectPedidos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }*/
    public void ConvertirPresupuesto(int IdPresupuestos) {

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    ///// FUNCION COMPLETAR ENTERO /////
    String completarInt(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

}
