package Clases;

public class cboFormasdepago {
    
  
    private int idFormasdepago;
    private String nombreFormasdepago;
    private double descuentoFormasdepago;
    private String signoFormasdepago;
    
    public cboFormasdepago(){}
    
    public cboFormasdepago(int id, String nombre, double descuento,String signo){
        this.idFormasdepago = id;
        this.nombreFormasdepago = nombre;
        this.descuentoFormasdepago = descuento;
        this.signoFormasdepago=signo;
    }

    public void setSignoFormasdepago(String signoFormasdepago) {
        this.signoFormasdepago = signoFormasdepago;
    }

    public String getSignoFormasdepago() {
        return signoFormasdepago;
    }

    public int getidFormasdepago() {
        return idFormasdepago;
    }

    public void setidFormasdepago(int idFormasdepago) {
        this.idFormasdepago = idFormasdepago;
    }

    public String getnombreFormasdepago() {
        return nombreFormasdepago;
    }

    public void setnombreFormasdepago(String nombreFormasdepago) {
        this.nombreFormasdepago = nombreFormasdepago;
    }
    
    public Double getdescuentoFormasdepago() {
        return descuentoFormasdepago;
    }

    public void setnombreFormasdepago(Double descuento) {
        this.descuentoFormasdepago = descuento;
    }
    
    public String toString(){
        return this.nombreFormasdepago;
    }
    
}
